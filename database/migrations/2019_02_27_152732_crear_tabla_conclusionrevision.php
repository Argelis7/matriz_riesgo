<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaConclusionrevision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conclusionrevision', function (Blueprint $table) {
            $table->increments('cr_id');
            $table->integer('cr_revid');
            $table->text('cr_conclusion');
            $table->integer('cr_usrcreacion');
            $table->integer('cr_status');            
            $table->string('cr_nombre');            
            $table->string('cr_logro');            
            $table->string('cr_desviaciones');            
            $table->string('cr_puntosatencion');            
            $table->string('cr_unidadresponsable');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conclusionrevision');
    }
}
