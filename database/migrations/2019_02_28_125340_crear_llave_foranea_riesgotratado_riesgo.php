<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearLlaveForaneaRiesgotratadoRiesgo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('riesgotratado', function (Blueprint $table) {
             $table->foreign('rtra_rsgid')->references('rsg_id')->on('riesgos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('riesgotratado', function (Blueprint $table) {
            $table->dropForeign(['rtra_rsgid']);
        });
    }
}
