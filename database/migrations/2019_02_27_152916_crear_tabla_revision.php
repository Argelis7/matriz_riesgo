<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaRevision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revision', function (Blueprint $table) {
            $table->increments('rev_id');
            $table->date('rev_fechainicio');
            $table->date('rev_fechafinal');
            $table->date('rev_fecharealfinal');
            $table->string('rev_nombre',200);
            $table->string('rev_ggeneral');
            $table->string('rev_gerencia',60);
            $table->string('rev_coordinacion',60);            
            $table->string('rev_responsable',60);            
            $table->string('rev_objetivos',200);            
            $table->string('rev_metas',200);            
            $table->string('rev_alcances',200);            
            $table->string('rev_relacion',200);            
            $table->string('rev_fortalezas',200);            
            $table->string('rev_oportunidades',200);            
            $table->string('rev_debilidades',200);            
            $table->string('rev_amenazas',200);            
            $table->integer('rev_usrcreacion');            
            $table->integer('rev_status');            
            $table->integer('rev_trev');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revision');
    }
}
