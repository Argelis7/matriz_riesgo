<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearLlaveForaneaTipoControlTratamientoEnTablaRiesgoControlado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('riesgocontrolado', function (Blueprint $table) {
            $table->foreign('rcon_tipocontrol')->references('tcr_id')->on('tipocontrol_tratamiento');               
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('riesgocontrolado', function (Blueprint $table) {
            $table->dropForeign(['rcon_tipocontrol']);
        });
    }
}
