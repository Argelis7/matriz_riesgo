<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearLlaveForaneaImplementacionControlTratamientoEnTablaRiesgoControlado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('riesgocontrolado', function (Blueprint $table) {
            $table->foreign('rcon_implementacion')->references('icr_id')->on('implementacioncontrolr');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('riesgocontrolado', function (Blueprint $table) {
            $table->dropForeign(['rcon_implementacion']);            
        });
    }
}
