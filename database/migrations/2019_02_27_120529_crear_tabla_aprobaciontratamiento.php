<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAprobaciontratamiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aprobaciontratamiento', function (Blueprint $table) {
            $table->increments('aptra_id');
            $table->integer('aptra_rtraid');
            $table->integer('aptra_rsgid');
            $table->string('aptra_responsableapro');
            $table->date('aptra_fecharespuesta');
            $table->integer('aptra_ert');
            $table->string('aptra_observaciones');
            $table->string('aptra_nombretra');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aprobaciontratamiento');
    }
}
