<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaImplementaciontrata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('implementaciontrata', function (Blueprint $table) {
            $table->increments('imt_id');
            $table->integer('imt_aptraid');
            $table->string('imt_responsable',30);
            $table->date('imt_fechaseguimiento');
            $table->date('imt_fechaobje');
            $table->integer('imt_porcavance');
            $table->string('imt_observaciones');
            $table->integer('imt_usrcreacion');
            $table->integer('imt_status');
            $table->string('imt_culminacion');
            $table->integer('imt_rsgid');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('implementaciontrata');
    }
}
