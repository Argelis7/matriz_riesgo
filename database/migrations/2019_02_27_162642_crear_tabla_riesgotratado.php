<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaRiesgotratado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riesgotratado', function (Blueprint $table) {
            $table->increments('rtra_id');
            $table->integer('rtra_rsgid');            
            $table->string('rtra_nombre',100);            
            $table->string('rtra_responsable',50);            
            $table->string('rtra_unidad',200);            
            $table->integer('rtra_tipotra');            
            $table->integer('rtra_comptra');            
            $table->integer('rtra_costobentra');            
            $table->integer('rtra_tiempotra');           
            $table->integer('rtra_usrcreacion');           
            $table->integer('rtra_status');           
            $table->string('rtra_efectividad',15);           
            $table->integer('rtra_suma');           

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riesgotratado');
    }
}
