<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaRiesgocontrolado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riesgocontrolado', function (Blueprint $table) {
            $table->increments('rcon_id');
            $table->integer('rcon_rsgid');            
            $table->string('rcon_nombre',200);            
            $table->string('rcon_responsable',50);            
            $table->string('rcon_unidad',100);            
            $table->integer('rcon_tipocontrol');            
            $table->integer('rcon_implementacion');            
            $table->integer('rcon_documentacion');            
            $table->integer('rcon_ejecucion');            
            $table->integer('rcon_usrcreacion');            
            $table->integer('rcon_status');            
            $table->string('rcon_efectividad',15);            
            $table->integer('rcon_suma');            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riesgocontrolado');
    }
}
