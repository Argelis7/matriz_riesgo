<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasCaracteresANombreTablaRiesgoControlado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('riesgocontrolado', function (Blueprint $table) {
            $table->string('rcon_nombre',300)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('riesgocontrolado', function (Blueprint $table) {
            $table->string('rcon_nombre',100)->change();            
        });
    }
}
