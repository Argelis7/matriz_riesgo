<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BorrarAceptacionYAgregarEfectividadALaTablaRiesgotratado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('riesgotratado', function (Blueprint $table) {
            $table->string("rtra_efectividad",15)->nullable();
            $table->tinyInteger("rtra_suma");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('riesgotratado', function (Blueprint $table) {
            $table->dropColumn("rtra_efectividad");
            $table->dropColumn("rtra_suma");
        });
    }
}
