<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LlaveForaneaRsgRevidEnTablaRiesgos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('riesgos', function (Blueprint $table) {
            $table->foreign('rsg_revid')->references('rev_id')->on('revision')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('riesgos', function (Blueprint $table) {
            $table->dropForeign(['rsg_revid']);
        });
    }
}
