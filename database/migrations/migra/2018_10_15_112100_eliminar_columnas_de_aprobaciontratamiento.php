<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EliminarColumnasDeAprobaciontratamiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aprobaciontratamiento', function (Blueprint $table) {
            $table->dropColumn('aptra_usrcreacion');
            $table->dropColumn('aptra_fechacreacion');
            $table->dropColumn('aptra_horacreacion');
            $table->dropColumn('aptra_status');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aprobaciontratamiento', function (Blueprint $table) {
            $table->integer('aptra_usrcreacion');
            $table->date('aptra_fechacreacion');
            $table->string('aptra_horacreacion');
            $table->integer('aptra_status');            
        });
    }
}
