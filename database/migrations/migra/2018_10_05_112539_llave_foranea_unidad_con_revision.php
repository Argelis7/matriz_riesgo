<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LlaveForaneaUnidadConRevision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unidades', function (Blueprint $table) {           
            $table->dropColumn('uni_fechauni');
            $table->dropColumn('uni_status');            
            $table->foreign('uni_revid')->references('rev_id')->on('revision')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unidades', function (Blueprint $table) {        
            $table->date('uni_fechauni');
            $table->integer('uni_status');
            $table->dropForeign(['uni_revid']);            
        });
    }
}
