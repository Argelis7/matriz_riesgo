<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaUnidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidades', function (Blueprint $table) {
            $table->increments('uni_id');
            $table->integer('uni_revid');
            $table->string('uni_ggeneral');
            $table->string('uni_gcorporativa',70);
            $table->string('uni_coordinacion',70);
            $table->string('uni_email',60);
            $table->string('uni_tunidad_evaluacion',60);
            $table->string('uni_nomape',60);
            $table->string('uni_telf');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidades');
    }
}
