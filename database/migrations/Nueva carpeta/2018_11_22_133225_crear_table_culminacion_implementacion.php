<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTableCulminacionImplementacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('implementaciontrata', function (Blueprint $table) {
            $table->string('imt_culminacion');
            $table->string('imt_rsgid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('implementaciontrata', function (Blueprint $table) {
            $table->dropColumn('imt_culminacion');
            $table->dropColumn('imt_horacreacion');
            $table->dropColumn('created_at');            
            $table->dropColumn('updated_at'); 
        });
    }
}
