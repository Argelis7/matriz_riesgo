<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EliminarColumnaIdTraAprobaciontratamiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aprobaciontratamiento', function (Blueprint $table) {
            $table->dropColumn('aptra_rtraid');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aprobaciontratamiento', function (Blueprint $table) {
            $table->string('aptra_rtraid');            
        });
    }
}
