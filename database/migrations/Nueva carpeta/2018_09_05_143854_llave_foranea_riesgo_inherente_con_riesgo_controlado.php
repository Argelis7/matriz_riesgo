<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LlaveForaneaRiesgoInherenteConRiesgoControlado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('riesgocontrolado', function (Blueprint $table) {
             $table->foreign('rcon_rsgid')->references('rsg_id')->on('riesgos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('riesgocontrolado', function (Blueprint $table) {

            $table->dropForeign(['rcon_rsgid']);

        });
    }
}
