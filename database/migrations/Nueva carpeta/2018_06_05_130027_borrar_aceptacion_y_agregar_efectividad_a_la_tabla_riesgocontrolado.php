<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BorrarAceptacionYAgregarEfectividadALaTablaRiesgocontrolado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('riesgocontrolado', function (Blueprint $table) {
            $table->dropColumn("rcon_aceprc");
            $table->string("rcon_efectividad",15)->nullable();
            $table->tinyInteger("rcon_suma");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('riesgocontrolado', function (Blueprint $table) {
            $table->dropColumn("rcon_efectividad");
            $table->integer("rcon_aceprc");
            $table->dropColumn("rcon_suma");
        });
    }
}
