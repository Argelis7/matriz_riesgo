<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearLlaveForaneaAprobacionEstadoRiesgo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aprobaciontratamiento', function (Blueprint $table) {
            $table->foreign('aptra_ert')->references('estrsg_id')->on('estado_riesgo'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aprobaciontratamiento', function (Blueprint $table) {
             $table->dropForeign(['aptra_ert']);
        });
    }
}
