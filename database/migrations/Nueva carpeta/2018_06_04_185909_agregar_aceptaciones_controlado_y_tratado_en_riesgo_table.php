<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarAceptacionesControladoYTratadoEnRiesgoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('riesgos', function (Blueprint $table) {
            $table->integer("rsg_acepcon")->nullable();
            $table->integer("rsg_aceptra")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('riesgos', function (Blueprint $table) {
            $table->dropColumn("rsg_acepcon");
            $table->dropColumn("rsg_aceptra");
        });
    }
}
