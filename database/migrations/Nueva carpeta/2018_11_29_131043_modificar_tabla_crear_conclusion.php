<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModificarTablaCrearConclusion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('conclusionrevision', function (Blueprint $table) {
             $table->dropColumn('cr_fechacreacion');
             $table->dropColumn('cr_horacreacion');
             $table->string('cr_nombre');
             $table->string('cr_logro');
             $table->string('cr_desviaciones');
             $table->string('cr_puntosatencion');
             $table->string('cr_unidadresponsable');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('conclusionrevision', function (Blueprint $table) {
            $table->date('cr_fechacreacion'); 
            $table->string('cr_horacreacion'); 
            $table->dropColumn('cr_nombre');
            $table->dropColumn('cr_logro');
            $table->dropColumn('cr_desviaciones');
            $table->dropColumn('cr_puntosatencion');
            $table->dropColumn('cr_unidadresponsable');
            $table->dropColumn('created_at');            
            $table->dropColumn('updated_at');
        });
    }
}
