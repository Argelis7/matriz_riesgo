<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearLlaveForaneaAprobaciontratamientoRiesgotratado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aprobaciontratamiento', function (Blueprint $table) {
             $table->foreign('aptra_rtraid')->references('rtra_id')->on('riesgotratado')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aprobaciontratamiento', function (Blueprint $table) {
            $table->dropForeign(['aptra_rtraid']);
        });
    }
}
