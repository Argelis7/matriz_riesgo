<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaRiesgos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riesgos', function (Blueprint $table) {
            $table->increments('rsg_id');
            $table->integer('rsg_revid');            
            $table->string('rsg_nombre');            
            $table->integer('rsg_clasf');            
            $table->integer('rsg_estrsg');            
            $table->string('rsg_responsable',200);            
            $table->string('rsg_undresp',60);            
            $table->string('rsg_descripcion',200);            
            $table->integer('rsg_aimpact');            
            $table->integer('rsg_aimpactn1');            
            $table->integer('rsg_fuente_riesgo');            
            $table->integer('rsg_friesgon1');            
            $table->integer('rsg_friesgon2');            
            $table->integer('rsg_ricons');            
            $table->integer('rsg_riprob');            
            $table->integer('rsg_acepri')->nullable();            
            $table->integer('rsg_usrcreacion');            
            $table->integer('rsg_status');                 
            $table->integer('rsg_otroarea')->nullable();            
            $table->integer('rsg_otrofuente')->nullable();            
            $table->integer('rsg_acepcon')->nullable();            
            $table->string('rsg_severidad');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riesgos');
    }
}
