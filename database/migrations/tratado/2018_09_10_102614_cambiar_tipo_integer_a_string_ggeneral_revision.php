<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CambiarTipoIntegerAStringGgeneralRevision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('revision', function (Blueprint $table) {
            $table->string('rev_ggeneral')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('revision', function (Blueprint $table) {
             $table->integer('rev_ggeneral')->change();
        });
    }
}


////////////////////////////////////////////////////////////////////
// se debe colocar change para decirle que cambie el tipo de dato //
////////////////////////////////////////////////////////////////////




