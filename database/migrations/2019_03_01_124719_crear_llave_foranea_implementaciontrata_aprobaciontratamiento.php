<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearLlaveForaneaImplementaciontrataAprobaciontratamiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('implementaciontrata', function (Blueprint $table) {
            $table->foreign('imt_aptraid')->references('aptra_id')->on('aprobaciontratamiento')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('implementaciontrata', function (Blueprint $table) {
            $table->dropForeign(['imt_aptraid']);
        });
    }
}
