<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
<head>
	 <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     <!-- Font Awesome -->   
    <link rel="stylesheet" type="text/css" href="{{ asset('fontawesome/css/all.css') }}">    
     <!-- Bootstrap core CSS -->
    <link href="{{ asset('mdb/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{ asset('mdb/css/mdb.min.css') }} " rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{ asset('mdb/css/style.css') }}" rel="stylesheet">
</head>

<body>
	 <script src="{{ asset('js/app.js') }}"></script>

       <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="{{ asset('mdb/js/jquery-3.2.1.min.js') }} "></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('mdb/js/popper.min.js') }} "></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('mdb/js/bootstrap.min.js') }}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('mdb/js/mdb.min.js') }}"></script>
     <!-- Animations initialization (agrego animacion)-->
    
</body>
</html>

    