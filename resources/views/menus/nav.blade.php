<!DOCTYPE html>
<html>

  <head>
   
    @extends("extenciones.formato")
    @section('accesovista')
    @include("extenciones.libreriasJS")
  </head>
  


     {{--  <!-- Side Navbar -->

    <nav class="side-navbar">
     
        <div>
         <br>
          <center>
             <img src="imagenes/Cantv_logo.png" class="img-fluid" width="70%" height="0">
          </center>
         <br>
        </div>
        
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
          
          <ul id="side-main-menu" class="side-menu list-unstyled">                  
            <li><a  href="{{ url('/index') }}" > <i class="fa fa-home" style="font-size: 25px"></i>Inicio</a></li>
            <li><a href="#revisiones" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-clipboard" style="font-size: 20px"></i>Revisión </a>
              <ul id="revisiones" class="collapse list-unstyled ">
                 <li id="registrar_r"><a href="{{ url('/revision') }}" > <i class="fa fa-edit"></i>Registrar</a></li> 
                 <li><a href="{{ url('/consultarevision') }}" onclick="borrar();" > <i class="fa fa-desktop"></i>Consultar</a></li>            
              </ul>
            </li>
             <li><a href="#usuario" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-user" style="font-size: 20px"></i>Usuario </a>
              <ul id="usuario" class="collapse list-unstyled ">
                <li><a href="#" ><i class="icon-search"></i>Consultar Usuarios</a></li>
              </ul>
            </li>
            
           
          </ul>
        </div>
        
    </nav>

    <div class="page">
      <!-- navbar-->
      <header class="header">
        <nav class="navbar">
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <div class="navbar-header">
                <a id="toggle-btn" href="#" class="menu-btn">
                  <i class="fa fa-indent" id="jil" style="font-size: 20px;margin-top: 10px; "></i>
                </a>
                <a  class="navbar-brand">
                  <div class="brand-text d-none d-md-inline-block"><strong style="color:#fff"></strong></div>
                </a>
              </div>
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                
                <!-- Log out-->
                <li class="nav-item dropdown">
                
                            <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <span style= " text-transform: capitalize; "> 
                                 @auth
                                 {{ Auth::user()->name }}
                                 @endauth
                               </span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a href="http://localhost/riesgo/app/php/cerrar.php" class="dropdown-item">
                                  
                                   Cerrar Sesión
                                </a>

                            </div>
                             
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header> --}}

{{-- <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-8 col-sm-8 col-md-11  col-lg-11" style="color:#f8f9fa" align="center">
               <p>Sistema desarrollado por la Coordinación de Ingeniería de Seguridad, perteneciente a la Gerencia de Seguridad de la Operación y Servicios de Cantv 2018 </p>
            </div>
            
             <div class="col-4 col-sm-4 col-md-1  col-lg-1">
                <img src="imagenes/Cantv_logo.png" class="img-fluid" width="100%" height="0" align="right">
            </div>
          </div>
        </div>
</footer> --}}

<header>
<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-dark red darken-4">

    <!-- Navbar brand -->
    <div class="col">
       <a class="navbar-brand" href="#"><strong style="font-size:24px;">Matriz de Riesgo</strong></a> 
    </div>

    <!-- Collapse button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

    <!-- Collapsible content -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <!-- Links -->
        <ul class="navbar-nav mr-auto">
          
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('index') }}">
                  <i class="fa fa-home"></i>
                  Inicio
                </a>
            </li>
            <li class="nav-item" id="registrar_r">
                <a class="nav-link" href="{{ url('/revision') }}">
                  <i class="fa fa-edit"></i>
                  Registrar
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('consulta.revision') }}" onclick="borrar();">
                  <i class="fa fa-desktop"></i>
                  Consultar
                </a>
            </li>

                 
        </ul>
        <!-- Links -->

        <!-- Links -->
        
          <ul class="navbar-nav nav-flex-icons ml-auto">
              <!-- Dropdown -->
              <li class="nav-item dropdown" >
                  
                              <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
                                 aria-haspopup="true" aria-expanded="false">
                                  <span style= " text-transform: capitalize; "> 
                                   @auth
                                   {{ Auth::user()->name }}
                                   @endauth
                                 </span>
                              </a>

                              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                  <a class="dropdown-item">
                                    <form action="{{ route('logout') }}" method="post">
                                      {{ csrf_field() }}

                                      <input 
                                            type="submit" 
                                            name="cerrar_sesion" 
                                            value="Cerrar Sesión"
                                            class="dropdown-item">
                                    </form>
                                  </a>

                              </div>
                               
               </li>
                       
          </ul>
        <!-- Links -->

    </div>
    <!-- Collapsible content -->

</nav>
<!--/.Navbar-->     </header>
<body class="fondo">
    @yield("contenido")

      
      @include('menus.footer')

  @endsection
   

    
  </body>


</html>
