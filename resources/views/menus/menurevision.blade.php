<style type="text/css"> 
.oculto{
  visibility: hidden;
}
</style>
@extends('menus.nav')
@section('contenido')

  
<div class="container-fluid color-fondo">
            <div class="row justify-content-center">
                      <div class="col col-md-12 col-lg-12">

  		<div class="mt-3" id="menu" style="float:right!important; display: none;"> <label>Modificar</label>
		    <div class="btn-group ver" role="group" aria-label="Basic example">
		      <button type="button" class="btn btn-secondary btn-sm "  id="onr" onclick="activar();">Si</button>
		      <button type="button" class="btn btn-danger btn-sm " id="offr" onclick="desactivar();">No</button>  
		    </div>
  		</div>
      <br>
  
 <ul class="nav nav-tabs">
    
     <li class="nav-item"> 
      <a class="nav-link active{{-- letra --}}" href="#ficha" data-toggle="tab" >Ficha Introductoria</a> 
    </li>
    
     <li class="nav-item">
      <a class="nav-link oculto" href="#riesgos" data-toggle="tab">Riesgos</a> 
     </li>
    
     <li class="nav-item">
      <a class="nav-link oculto" href="#matrices" data-toggle="tab" id="matricesr">Matrices Revisión</a> 
     </li>
    
     <li class="nav-item">
      <a class="nav-link oculto" href="#unidades" data-toggle="tab">Unidades Involucradas</a> 
     </li>
    
     <li class="nav-item">
      <a class="nav-link oculto" href="#conclusion" data-toggle="tab">Conclusiones</a> 
     </li>    

 </ul>



@yield("dos")

</div>
</div>
</div>


<script type="text/javascript">

$(function(){
 // if (localStorage.getItem('si')) {
 //  $("#menu").attr("style","visibility:visible");
 //  $("#menu").attr("style","float:right!important;");
 //  $(".ver").attr("style","visibility:visible");
 //  // localStorage.clear()
 // }

 // if (localStorage.getItem('consulta') || localStorage.getItem('si')) {
 //  $(".oculto").attr("style","visibility:visible"); 
 //  // localStorage.clear()
 // }
 if (localStorage.getItem('si') || localStorage.getItem('id_revision_editar')) {
  $("#menu").attr("style","visibility:visible");
  $("#menu").attr("style","float:right!important;");
  $(".ver").attr("style","visibility:visible");
  $(".oculto").attr("style","visibility:visible");   
  $("#opcion").attr('disabled', true);
  // localStorage.clear()
 }

 // if (localStorage.getItem('id_revision_editar') || localStorage.getItem('si')) {
  // localStorage.clear()
 // }
})
        
</script>
@endsection





