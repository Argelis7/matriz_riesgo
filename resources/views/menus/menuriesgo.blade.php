<style type="text/css">	
.fondo{
	background: #cdd0d299;
}


</style>

@extends('menus.nav')
@section('contenido')
<div class="container-fluid">
   <div class="row justify-content-center">
     <div class="col col-md-12 col-lg-12 ">
  
		<div class="mt-3" id="menu_r" style="float:right!important;display:none;"> <label>Modificar</label>
		    <div class="btn-group ver" role="group" aria-label="Basic example">
		      <button type="button" class="btn btn-secondary btn-sm" id="on" onclick="activar();">Si</button>
		      <button type="button" class="btn btn-danger btn-sm" id="off" onclick="desactivar();">No</button>  
		    </div>
  		</div>
  		<br>
  	<ul class="nav nav-tabs">
    
     <li class="nav-item"> 
      <a class="nav-link active{{-- letra --}}" href="#home2" data-toggle="tab" id="inicio">Riesgo Inherente</a> 
    </li>
    
     <li class="nav-item">
      <a class="nav-link{{-- letra --}}" id="controlado" data-toggle="tab">Riesgo Controlado</a> 
     </li>
  
     <li class="nav-item">
      <a class="nav-link {{-- letra --}}" id="tratado" data-toggle="tab">Riesgo Tratado</a> 
     </li>
    
     <li class="nav-item">
      <a class="nav-link ver" href="#aprobacion" data-toggle="tab" style="display: none" id="m_aprobacion">Aprobacion</a> 
     </li>
    
     <li class="nav-item">
      <a class="nav-link ver" href="#implementacion" data-toggle="tab" style="display: none">Implementacion de los tratamientos</a> 
     </li>   

      <li class="nav-item">
      <a class="nav-link " id="matriz" data-toggle="tab">Matriz de Riesgo</a> 
     </li> 

 </ul>
    


@yield("uno")

<script type="text/javascript">

$(function(){
 if (localStorage.getItem('idInherenteEditar')) {
  $("#menu_r").attr("style","visibility:visible");
  $('#b_aprobacion').attr("style","visibility:visible");
  $("#menu_r").attr("style","float:right!important;");
  $(".ver").attr("style","visibility:visible");
  // localStorage.clear()
 }else{
   $('#b_aprobacion').attr("style","display:none");
 }

  // if (localStorage.getItem('agregar_riesgo') || localStorage.getItem('ver_gestion')) {
  if (localStorage.getItem('idInherenteEditar')) {
  $("#aproba,#imple").attr("style","visibility:visible");
  $("#controlado,#tratado,#matriz").removeClass('fondo');
  $("#controlado,#tratado,#matriz").attr("data-toggle","tab");
  $("#controlado").attr("href","#profile2");
  $("#tratado").attr("href","#messages2");
  $("#matriz").attr("href","#messages3");
 }
})
        
</script>
@endsection
	</div>
  </div>
</div>




