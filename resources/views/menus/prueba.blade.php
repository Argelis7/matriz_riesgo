@extends('layouts.apprueba')
@include('extenciones.libreriasMDB')
@section("content")

<div class="container">
    <div class="row  pt-lg-5 mt-lg-5 justify-content-center">
      <div class="col-md-6 col-xl-5 mb-4 form-group">
        <form method="POST" action="{{ route('login') }}">
        	
          <div 
          class="card wow fadeInRight" data-wow-delay="0.3s"> <!--son las que dan mvimiento-->
            <div class="card-body z-depth-2">
              <!--formulario-->
                <div class="text-center">
                    <h3 class="dark-grey-text">
                    <strong>Autenticación</strong>
                    </h3>
                    <hr>
                </div>

          <div class="form-group ">
                <div class="md-form">
                  <i class="fa fa-user prefix grey-text"></i>
                  <input type="text" name="usuario" id="form3" class="form-control">
                  <label for="form3">Usuario</label>
                </div>
          </div>

          <div class="form-group">
                <div class="md-form">
                   <i class="fa fa-lock prefix grey-text"></i>
                  <input type="password" name="contraseña" id="form2" class="form-control">
                  <label for="form2">Contraseña</label>
                </div>
          </div>
          
          <div class="form-group">
                  <div class="text-center mt-3">
                     <button  type="submit " class="btn btn-danger btn-rounded">Ingresar</button>                  
                     <hr>
                  </div>
          </div>
        </div>
      </div>
          {{csrf_field()}}
        </form>
      </div>
    </div>
         
      </div>
      <script>       
    new WOW().init();
    </script>
                      
@endsection