<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="UTF-8">
	<title>Document</title>	
  <link rel="stylesheet" type="text/css" href="{{ asset('css/estilos.css') }}">
  @include("extenciones.formato")
  @include("extenciones.libreriasJS")
</head>
<style type="text/css">
  .inherente{
  border: 1px #000 solid;
  border-radius: 30px;
  width: 30px;
  height: 30px;  
  background-color: grey;
  margin-top: 10px;
  }

  .controlado{
  border: 1px #000 solid;
  border-radius: 30px;
  width: 20px;
  height: 20px;  
  background-color: purple;
  float: left;
  
  /*position: absolute;*/
  }

  .tratado{
  border: 1px #000 solid;
  border-radius: 30px;
  width: 30px;
  height: 30px;  
  background-color: pink;
  margin-top: 10px;
  }
  
</style>
<body>
{{-- <div id="cajapadre" style="width: 200px; height: 100px;"></div> --}}


 <div class="container-fluid">
            <div class="row justify-content-center">
                      <div class="col col-md-12 col-lg-12 ">
    <div class="row">
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" align="right">5.- Casi sin Certeza</div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam naranja"><div id="cajapadre"></div></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam izquierda naranja"><div id="p25"></div></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam izquierda rojo"><div id="p35"></div></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam izquierda rojo"><div id="p45"></div></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam izquierda rojo"><div id="p55"></div></div>         
      </div>
      <div class="row">
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" align="right">4.- Probable</div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arriba amarillo"><div id="p14"></div></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq naranja"><div id="p24"></div></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq naranja"><div id="p34"></div></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq rojo"><div id="p44"></div></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq rojo"><div id="p54"></div></div>
      </div> 
      <div class="row" align="right">
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" >3.- Moderada</div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arriba verde"></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq amarillo"></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq naranja"></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq rojo"></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq rojo"></div>
      </div> 
      <div class="row" align="right">
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" >2.- Improbable</div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arriba verde"></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq verde"></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq amarillo"></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq naranja"></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq rojo"></div>
      </div> 
      <div class="row">
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" align="right">1.- Practicamente Imposible</div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arriba verde"></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq verde"></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq amarillo"></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq naranja"></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq naranja"></div>
      </div> 
      <div class="row" align="center">
        <div class="col palabra" ></div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" >1.- Insignificante</div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" >2.- Menor</div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" >3.- Moderada</div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" >4.- Mayor</div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" >5.- Catastrófica</div>
      </div> 
  </div>
   

<a href="#ventana1" class="btn btn-primary btn-lg" data-toggle="modal" id="boton">Boton</a>

{{-- <div class="modal fade" id="ventana1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        @include("formularios.controlado")
      </div>
    </div>
  </div>
</div>
 --}}

<div type="button" id="probando" class="btn btn-primary">Boton</div>

  

<script>


$(document).ready(function(){
    $("#boton").click(function(){
      $("#cajapadre").prepend("<div class='controlado'></div>");
       {}
  }); 
});

  // $(function(){
  //   $("#probando").click(function() {
  //     var num1 = p14;
  //     $(p14).addClass('inherente');  
  //     $(p24).addClass('controlado');  
  //     $(p34).addClass('tratado');
  //   });
  // });
  // (function(){
        // var saludo = function(){

          // if(riesgo==1){
          //   suma=suma+1;
          //   var elemento = document.getElementById(num1)
          //     elemento.style.width = "30px";
          //     elemento.style.height = "30px";
          //     elemento.style.background = "grey";
          //     elemento.style.borderRadius = "30px";
          
          // if(suma==1){           
          //     elemento.style.marginTop = "30px";    
          //     elemento.style.marginLeft = "75px";  
          //    }else{
          //     if(suma==2){           
          //     elemento.style.marginTop = "30px";    
          //     elemento.style.marginLeft = "30px";  
          //    }
          //    }}

      //      if(riesg==2){
     
      //     var elemento = document.getElementById(num2)
      //         elemento.style.width = "30px";
      //         elemento.style.height = "30px";
      //         elemento.style.background = "grey";
      //         elemento.style.borderRadius = "30px";    
      //         elemento.style.marginTop = "30px";    
      //         elemento.style.marginLeft = "75px";  
      //        }
      // };

// de esta manera accedemos al elemento mediante el boton
    // var boton = document.getElementById('boton');

    // var riesgo=1;
    // var riesg=2;
    // var suma=0;

    // var num1 = "p54";
    // var num2 = "p25";
// agregar evento
 //    boton.addEventListener("click", saludo);

 // }())

  
</script>




</body>
</html>