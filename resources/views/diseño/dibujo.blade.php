<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
    <script src="{{ asset('js/js/matriz.js') }}"></script>
    <link href="{{ asset('css/matriz.css') }}" rel="stylesheet">


	
	<title>Document</title>

</head>



<body>
	 

<input type="hidden" id="consecuencia" value="{{ $riesgo->rsg_ricons ?? '' }}">
<input type="hidden" id="probabilidad" value="{{ $riesgo->rsg_riprob ?? '' }}">
{{-- <p>suma control</p> --}}
<input type="hidden" id="sumaControlCorrectivo" value="{{ $sumaControlCorrectivo ?? ''}}">
<input type="hidden" id="sumaControlPreventivo" value="{{ $sumaControlPreventivo ?? ''}}">
{{-- <p>cantidad de controles</p> --}}
<input type="hidden" id="CantidadControlCorrectivo" value="{{ $CantidadControlCorrectivo ?? ''}}">
<input type="hidden" id="CantidadControlPreventivo" value="{{ $CantidadControlPreventivo ?? ''}}">
{{-- <p>suma tratamiento</p> --}}
<input type="hidden" id="sumaTratamientoCorrectivo" value="{{ $sumaTratamientoCorrectivo ?? ''}}">
<input type="hidden" id="sumaTratamientoPreventivo" value="{{ $sumaTratamientoPreventivo ?? ''}}">
{{-- <p>cantidad de tratamientos</p> --}}
<input type="hidden" id="CantidadTratamientoCorrectivo" value="{{ $CantidadTratamientoCorrectivo ?? ''}}">
<input type="hidden" id="CantidadTratamientoPreventivo" value="{{ $CantidadTratamientoPreventivo ?? ''}}">

 	
 	

 	<div class="container">
 		<div class="row">
 			<div class="col-md-2 mr-5">
				<h5>Grado del Riesgo:</h5>				
 			</div>
 			<div class="col-md-2 ml-p">
				<div class="verde-g" ></div><h5 class="ml-4">Baja</h5>				
 			</div>
 			<div class="col-md-2 ml-n">
				<div class="amarillo-g"></div><h5 class="ml-4">Moderada</h5>				
 			</div>
 			<div class="col-md-2 ">
				<div class="naranja-g"></div><h5 class="ml-4">Alta</h5>				
 			</div>
 			<div class="col-md-2 ml-n" >
				<div class="rojo-g"></div><h5 class="ml-4">Extrema</h5>				
 			</div>
 		</div>
 	</div>
 	<hr>

 	<div class="container">
 		<div class="row">
 			<div class="col-md-3 offset-md-1">
				<div class="inherente"></div><h5 class="ml-4">Riesgo Inherente</h5>				
 			</div>
 			<div class="col-md-3 offset-md-1">
				<div class="controlado"></div><h5 class="ml-4">Riesgo Controlado</h5>				
 			</div>
 			<div class="col-md-3 offset-md-1">
				<div class="tratado"></div><h5 class="ml-4">Riesgo Tratado</h5>				
 			</div>
 		</div>
 	</div>

 	<hr>

	<h2 align="center">Matriz Global</h2><br>

	 <div class="container-fluid">
            <div class="row justify-content-center">
                      <div class="col col-md-12 col-lg-12">
		<div class="row">
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" align="right">5.- Casi sin Certeza</div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam naranja" ><div id="p15"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam izquierda naranja"><div id="p25"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam izquierda rojo"><div id="p35"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam izquierda rojo"><div id="p45"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam izquierda rojo"><div id="p55"></div></div>   			
   		</div>
   		<div class="row">
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" align="right">4.- Probable</div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arriba amarillo"><div id="p14"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq naranja"><div id="p24"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq naranja"><div id="p34"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq rojo"><div id="p44"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq rojo"><div id="p54"></div></div>
   		</div> 
   		<div class="row" align="right">
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" >3.- Moderada</div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arriba verde"><div id="p13"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq amarillo"><div id="p23"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq naranja"><div id="p33"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq rojo"><div id="p43"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq rojo"><div id="p53"></div></div>
   		</div> 
   		<div class="row" align="right">
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" >2.- Improbable</div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arriba verde"><div id="p12"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq verde"><div id="p22"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq amarillo"><div id="p32"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq naranja"><div id="p42"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq rojo"><div id="p52"></div></div>
   		</div> 
   		<div class="row">
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" align="right">1.- Practicamente Imposible</div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arriba verde"><div id="p11"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq verde"><div id="p21"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq amarillo"><div id="p31"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq naranja"><div id="p41"></div></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 tam arrizq naranja"><div id="p51"></div></div>
   		</div> 
   		<div class="row" align="center">
   			<div class="col palabra" ></div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" >1.- Insignificante</div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" >2.- Menor</div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" >3.- Moderada</div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" >4.- Mayor</div>
   			<div class="col-2 col-sm-2 col-md-2 col-lg-2 palabra" >5.- Catastrófica</div>
   		</div> 
	</div>

	<div class="container">

			{{-- riesgo inherente --}} 
			<div class="row ">

				<div class="col-12 col-sm-6 col-md-4 col-lg-4"><br><br><br><br>
					{{-- quinta fila --}}
					<div class="row" align="center">
						<div class="col quit ml-5" style="font-size: 19px"> Riesgos Inherentes</div>
					</div>
					{{-- quinta fila --}}
					<div class="row">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna naranja"><div id="pi15"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna izquierda naranja"><div id="pi25"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna izquierda rojo"><div id="pi35"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna izquierda rojo"><div id="pi45"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna izquierda rojo"><div id="pi55"></div></div>	
					</div>
					{{-- cuarta fila --}}
					<div class="row">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arriba amarillo"><div id="pi14"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pi24"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pi34"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pi44"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pi54"></div></div>		
					</div>
					{{-- tercera fila --}}
					<div class="row">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arriba verde"><div id="pi13"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq amarillo"><div id="pi23"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pi33"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pi43"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pi53"></div></div>		
					</div>
					{{-- segunda fila --}}
					<div class="row">						
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arriba verde"><div id="pi12"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq verde"><div id="pi22"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq amarillo"><div id="pi32"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pi42"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pi52"></div></div>	
					</div>
					{{-- primera fila --}}
					<div class="row">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arriba verde"><div id="pi11"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq verde"><div id="pi21"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq amarillo"><div id="pi31"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pi41"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pi51"></div></div>		
					</div>
					{{-- sexta fila --}}
					<div class="row">						
						<div class="col-6 col-sm-6 col-md-6 col-lg-6 caja_interna quit texto-vertical-2 text-primary" align="center">Probabilidad</div>

						<div class="col-6 col-sm-6 col-md-6 col-lg-6 caja_interna quit mt-1 pink-text" align="center" ><b>Consecuencia</b></div>						
					</div>

			</div>
			
			{{-- Riesgos Controlados --}}
			<div class="col-12 col-sm-6 col-md-4 col-lg-4"><br><br><br><br>
					{{-- quinta fila --}}
					<div class="row" align="center">
						<div class="col quit ml-5" style="font-size: 19px"> Riesgos Controlados</div>
					</div>
					{{-- quinta fila --}}
					<div class="row">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna naranja"><div id="pc15"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna izquierda naranja"><div id="pc25"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna izquierda rojo"><div id="pc35"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna izquierda rojo"><div id="pc45"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna izquierda rojo"><div id="pc55"></div></div>	
					</div>
					{{-- cuarta fila --}}
					<div class="row">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arriba amarillo"><div id="pc14"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pc24"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pc34"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pc44"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pc54"></div></div>		
					</div>
					{{-- tercera fila --}}
					<div class="row">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arriba verde"><div id="pc13"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq amarillo"><div id="pc23"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pc33"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pc43"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pc53"></div></div>		
					</div>
					{{-- segunda fila --}}
					<div class="row">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arriba verde"><div id="pc12"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq verde"><div id="pc22"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq amarillo"><div id="pc32"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pc42"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pc52"></div></div>	
					</div>
					{{-- primera fila --}}
					<div class="row">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arriba verde"><div id="pc11"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq verde"><div id="pc21"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq amarillo"><div id="pc31"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pc41"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pc51"></div></div>		
					</div>
					{{-- sexta fila --}}
					<div class="row">					
						<div class="col-6 col-sm-6 col-md-6 col-lg-6 caja_interna quit texto-vertical-2 text-primary" align="center">Probabilidad</div>

						<div class="col-6 col-sm-6 col-md-6 col-lg-6 caja_interna quit mt-1 pink-text" align="center"><b>Consecuencia</b></div>						
					</div>


			</div>

			{{-- Riesgos Tratados --}}
			<div class="col-12 col-sm-6 col-md-4 col-lg-4"><br><br><br><br>
					{{-- quinta fila --}}
					<div class="row" align="center">
						<div class="col quit ml-5" style="font-size: 19px"> Riesgos Tratados</div>
					</div>
					{{-- quinta fila --}}
					<div class="row">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna naranja"><div id="pt15"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna izquierda naranja"><div id="pt15"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna izquierda rojo"><div id="pt15"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna izquierda rojo"><div id="pt15"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna izquierda rojo"><div id="pt15"></div></div>	
					</div>
					{{-- cuarta fila --}}
					<div class="row">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arriba amarillo"><div id="pt14"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pt24"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pt34"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pt44"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pt54"></div></div>		
					</div>
					{{-- tercera fila --}}
					<div class="row">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arriba verde"><div id="pt13"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq amarillo"><div id="pt23"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pt33"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pt43"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pt53"></div></div>		
					</div>
					{{-- segunda fila --}}
					<div class="row">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arriba verde"><div id="pt12"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq verde"><div id="pt22"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq amarillo"><div id="pt32"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pt42"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq rojo"><div id="pt52"></div></div>	
					</div>
					{{-- primera fila --}}
					<div class="row">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna quit"></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arriba verde"><div id="pt11"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq verde"><div id="pt21"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq amarillo"><div id="pt31"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pt41"></div></div>
						<div class="col-2 col-sm-2 col-md-2 col-lg-2 caja_interna arrizq naranja"><div id="pt51"></div></div>		
					</div>
					{{-- sexta fila --}}
					<div class="row">						
						<div class="col-6 col-sm-6 col-md-6 col-lg-6 caja_interna quit texto-vertical-2 text-primary" align="center">Probabilidad</div>						
						<div class="col-6 col-sm-6 col-md-6 col-lg-6 caja_interna quit mt-1 pink-text" align="center"><b>Consecuencia</b></div>						
					</div>


			</div>


		</div>

	</div>
			
</div>
</div>

</body>






</html>

