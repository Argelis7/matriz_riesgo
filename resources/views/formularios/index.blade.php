<head>
  <title>Document</title>
</head>
<body>

  @extends('menus.nav')

  @section('contenido')
 




  <div class="page-wrapper"><br> 
    <div class="container-fluid mt-4">
      <div class="row">

        <div class="col-md-4">
          <div class="card">
            <div class="card-header  red lighten-1 text-white">
              <h3>
                Bienvenido(a) 
                <span style= " text-transform: capitalize; "> 
                   @auth
                   {{ Auth::user()->name }}
                   @endauth
                !</span>
              </h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-xs-3 ml-3">
                    <i class="fa fa-laptop fa-5x"></i>
                </div>
                <div class="col-xs-4 ml-5 text-right">                   
                    <div><h4>Última conexión:</h4></div>
                    <div> Fecha: {{ $fecha}}</div>
                    <div>Hora: {{ $hora }} </div>                                
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="card">
            <div class="card-header  red lighten-1 text-white">
              <h3>
                Cantidad de Revisiones
              </h3>
            </div>
            <div class="card-body">
              <div class="row justify-content-md-center">
                <div class="col-xs-2">
                  <i class="fas fa-clipboard-list fa-5x"></i>
                </div>
                <div class="col-xs-2 text-right ml-5">                   
                     <div> 
                      <h1> <strong>{{ $cantidadRevision}}</strong></h1> 
                     </div>
                     <div># de revisiones registradas</div>                         
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
           <div class="card-header  red lighten-1 text-white">
             <h3>
                Cantidad de Riesgos
              </h3>
           </div>
           <div class="card-body">
              <div class="row justify-content-md-center">
                <div class="col-xs-2">
                  <i class="fas fa-exclamation-triangle fa-5x"></i>
                </div>
                <div class="col-xs-5  ml-3 text-right">                   
                     <div> 
                      <h1><strong>{{$cantidadRiesgo}}</strong></h1> 
                     </div>
                     <div># de Riesgos Inherentes registrados</div>                         
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br><br>

      <div class="row">

        <div class="col-md-4">
          <div class="card">
            <div class="card-header  red lighten-1 text-white">
              <h3>
                Cantidad de Controles                
              </h3>
            </div>
            <div class="card-body">
              <div class="row justify-content-md-center">
                <div class="col-xs-2">
                  {{-- <i class="fas fa-clipboard-list fa-5x"></i> --}}
                  <i class="fas fa-cogs fa-5x"></i>
                </div>
                <div class="col-xs-2 text-right ml-5">                   
                     <div> 
                      <h1> <strong>{{ $cantidadControlado}}</strong></h1> 
                     </div>
                     <div># de Riesgos Controlados registrados</div>                       
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="card">
            <div class="card-header  red lighten-1 text-white">
              <h3>
                Cantidad de Tratamientos
              </h3>
            </div>
            <div class="card-body">
              <div class="row justify-content-md-center">
                <div class="col-xs-2">
                  <i class="fas fa-capsules fa-5x"></i>
                </div>
                <div class="col-xs-2 text-right">                   
                     <div> 
                      <h1> <strong>{{$cantidadTratado}}</strong></h1> 
                     </div>
                     <div># de Riesgos Tratados registrados <strong style="color:white;">sddfffdffd</strong></div>                         
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
           <div class="card-header  red lighten-1 text-white">
             <h3>
                Cantidad T I
              </h3>
           </div>
           <div class="card-body">
              <div class="row justify-content-md-center">
                <div class="col-xs-2">
                  <i class="fas fa-tasks fa-5x"></i>
                </div>
                <div class="col-xs-5  ml-3 text-right">                   
                     <div> 
                      <h1><strong>{{$cantidadImplementaTra}}</strong></h1> 
                     </div>
                     <div># de Tratamientos Implementados registrados</div>                         
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>




                        {{-- <div class="col-lg-12 col-lg-offset-1 mt-4 ml-4">
                            <h3 class="page-header"> Bienvenido(a) <span style= " text-transform: capitalize; "> 
                                 @auth
                                 {{ Auth::user()->name }}
                                 @endauth
                               !</span> </h3>

                        </div> --}}
                        <!-- /.col-lg-12 -->
                    {{-- </div> --}}
                    {{-- <div class="container-fluid">
               <div class="col-lg-12 col-md-12 col-lg-offset-1 mb-4">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-4 ">
                                    <i class="fa fa-laptop fa-5x"></i>
                                </div>
                                <div class="col-xs-8 text-right">
                                   
                                    <div><h4>Última conexión:</h4></div>
                                    <div> Fecha: {{ $dia }} </div>
                                    <div>Hora: {{ $hora }}</div>                                  

                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                   </div> --}}


    </div>


  </div>

  @endsection
</body>
</html>


