
@include('extenciones.libreriasMDB')

 @guest
 @php
   
echo '<h1>ERROR</h1>';
echo $redirecto=redirect()->route('login') ;
 @endphp

@else
<link rel="stylesheet" href="{{ asset('css/estilos.css') }}">

<body>

    <div class="container-fluid">
      <img src="{{ asset('imagenes/logo_cantvnew.png') }}" class="img-fluid" width="100%" height="60" >
    </div>     
    <br>

<div class="container">
    <div class="row  pt-lg-5 mt-lg-5 justify-content-center">
      <div class="col-md-6 col-xl-5 mb-4 form-group">
          <div class="card wow fadeInRight" data-wow-delay="0.3s"> 
            <!--son las que dan movimiento-->
            <div class="card-body z-depth-2">

              <form  action="http://161.196.125.18/tokenweb/prueba_radius.php" method="POST">

              <!--formulario-->
                <div class="text-center">
                    <h3 class="dark-grey-text">
                    <strong>Autenticación</strong>
                    </h3>
                    <hr>
                </div>

                <div class="form-group">
                      <div class="md-form">
                        <i class="fa fa-user prefix grey-text"></i>
                        <input type="text" name="nombre" id="form3" class="form-control" value="@auth {{ Auth::user()->user_red }} @endauth" readonly >
                        {{ $errors->first('nombre') }}
                        <label for="form3">Usuario</label>
                      </div>
                </div>

                <div class="form-group">
                      <div class="md-form">
                         <i class="fa fa-lock prefix grey-text"></i>
                        <input type="password" name="contrasena" id="form2" class="form-control">{{ $errors->first('contrasena') }}
                        <label for="form2">Contraseña</label>
                      </div>
                </div>
          
                <div class="form-group">
                        <div class="text-center mt-3">
                           <button  type="submit " class="btn btn-danger btn-block">Ingresar</button>                  
                           <hr>
                        </div>
                </div>

                {{csrf_field()}}
                
                <input type="text" name="acept" size="20" value ='http://localhost/matriz_riesgo/app/php/procesar.php' >
                <input type="text" name="recha" size="20" value ='http://localhost/matriz_riesgo/app/php/procesar.php' >

              </form>

              {{-- Para cambiar de Usuario --}}

              <div class="form-group">
                <center><a href="{{ route('logout') }}" class="dropdown-item" style="color:#007bff" 
                  onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                  Cambiar Usuario de red
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST"
                        style="display: none;">
                       {{ csrf_field() }}
                  </form>
                </center>
              </div>
        </div>
      </div>
      </div>
      </div>
          </div>

        

<!--Footer-->

<!--Footer-->
<footer class="font-small pt-0 footer-login">
      <div class="posicion-f">


 
    
      
     
    <!--Copyright-->
    <div class="footer-copyright py-3 text-center">
{{-- <hr style="border: 3px solid #d32f2f; margin-top: -15px;">  --}}
        <div class="container-fluid">
          <div class="row">
            <div class="col-8 col-sm-8 col-md-11 col-lg-11 color-f">

                 <p>Sistema desarrollado por la Coordinación de Ingeniería de Seguridad, perteneciente a la Gerencia de Seguridad de la Operación y Servicios de Cantv 2018 </p>
            </div>
            
             <div class="col-4 col-sm-4 col-md-1  col-lg-1">
                <img src="imagenes/logo-blanco.png" class="img-fluid" width="100%" height="0" align="right">
            </div>
          </div>
        </div>
       
    </div>
    <!--/Copyright-->
  </div>

</footer>
<!--/Footer-->



 <!-- Animations initialization (agrego animacion)-->
    <script>       
    new WOW().init();

    $(document).ready(function(){
      console.log('ready');
    })
    </script>

</body>

                
                      
 @endguest
