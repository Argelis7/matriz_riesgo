{{--ventana emergente --}}

<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
     <br>

     <div class="container-fluid">
       <div class="card">
         <div class="card-header blue darken-4 text-white" align="center"> Nueva Conclusión </div>
           <div class="card-body">

            <form action="{{ route('registrarRevision') }}" method="post" id="form_cont" class="form_cont">
            {{ csrf_field() }}
            <fieldset id="modal_con">
               <div class="row justify-content-center">
                               
                      <div class="col-md-6">

                        <div class="form-group">

                          <label>Nombre de la Revision</label>
                          <input required name="cr_nombre" class="form-control">
                        </div>
                      </div>

                      <div class="col-md-6">        
                        <div class="form-group">
                          <label>Logros</label>
                          <input required name="cr_logro" class="form-control">              
                        </div>
                      </div>

                      <div class="col-md-6">

                        <div class="form-group">

                          <label>Desviaciones</label>
                          <input required name="cr_desviaciones" class="form-control">  
                                            
                        </div>
                      </div>

                      <div class="col-md-6">

                        <div class="form-group">

                          <label>Puntos de atención</label>
                          <input required name="cr_puntosatencion" class="form-control">
                                            
                        </div>
                      </div>

                      <div class="col-md-6" >

                        <div class="form-group">
                          <label>Unidad Responsable</label>
                          <input required name="cr_unidadresponsable" class="form-control">
                        
                        </div>
                      </div>

                       <div class="col-md-12" >

                        <div class="form-group">
                          <label>Conclusiones Generales</label>
                         <textarea class="form-control" rows="5" id="comment" required name="cr_conclusion">
                      
                         </textarea>            
                        </div>
                      </div>

                      <input type="hidden" name="cr_revid">

                    <div class="row  ">
                      <div class="fcol-md-3 center-block">

                       <button type="button" class="btn btn-danger btn-sm pull-left "  aria-expanded="false" data-dismiss="modal" >Cancelar</button>&nbsp

                       <input type="submit" class="btn btn-success btn-sm pull-right " align="" aria-expanded="false" id="bt_add" value="Guardar">

                       <input type="hidden" name="formulario_revision" value="Conclusion">

                      </div>
                    </div>                                                        
                      
                   </div>  
                </fieldset>
              </form>
        </div>
       </div>
      </div>
{{-- contenido de la ventana --}}
     </div>
    </div>
   </div>
