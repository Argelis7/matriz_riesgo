<style type="text/css">
  .seleccionada{
    background: #3da6f375;
    color:white;
  }

  .tamaño{
    font-size: 12px !important;
  }
</style>
<!-- Editable table -->                         
      <div id="content"> 
       
            <div class="row justify-content-center mt-2">
       
              <!-- Boton para llamar modal de unidades -->
               <button 
                  href="#ventana7" 
                  type="button" 
                  class="btn blue darken-4 btn-sm d-control" 
                  id="agregar_u" 
                  aria-expanded="false" 
                  data-toggle="modal">

                    Agregar Nueva Conclusión &nbsp;

                  <i class="fa  fa-plus-square icono-plus"></i>

               </button>
               <!-- ! Boton para llamar modal de unidades -->
             </div>

           <table id="table-conclusiones" class="table table-striped table-bordered table-responsive-lg" width="100%">
                      <thead>
                        <tr>
 
                          <th class="th-sm">Nombre y Apellido
                          </th>
                          <th class="th-sm">Gerencia General
                          </th>
                          <th class="th-sm">Gerencia Corportiva
                          </th>
                          <th class="th-sm">Coordinacion</th>
                          <th>Tipo de Unidad de Evaluacion</th>
                          <th>Acciones</th>

                        </tr>
                      </thead>                   
                     <tbody>
                       @foreach ($conclusion as $conclu)
                        <tr>                       
                          <td>{{ $conclu->cr_nombre }}</td>
                          <td>{{ $conclu->cr_logro }}</td>
                          <td>{{ $conclu->cr_desviaciones }}</td>
                          <td>{{ $conclu->cr_puntosatencion }}</td>
                          <td>{{ $conclu->cr_unidadresponsable }}</td>
                         
                          <td class="justify-content-center">
                            <form action="{{ route('conclusion.eliminar', $conclu->cr_id) }}" method="post" >
                              {{ csrf_field() }}
                              {{ method_field('DELETE') }}
                              
                              <button 
                                      type="submit"  
                                      class="btn btn-danger btn-sm px-2 bord-rad borrar_fila" 
                                       
                                      name="eliminarConclusion">
                                <i class="fa fa-trash icon-bas" ></i>
                              </button>                              

                            </form>
                          </td>
                        </tr>
                      @endforeach                 
                    </tbody>
                   
                     </table>  

          <p class="oculto-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>   
       
              
              {{-- tabla emergente --}}
              <div class="modal fade" id="ventana7" role="dialog" data-backdrop="static">

                @include("formularios.revision.conclusiones.modalconclusiones")

              </div>
        </div>
   
{{-- <script type="text/javascript">
      


$(function(){
 if (localStorage.getItem('si')) {
  $("#agregar_u,#eliminar_u").prop('disabled', 'true');
  $('#agregar_u').parent().removeAttr('data-toggle');
 }
})        
</script> --}}

{{-- <div id="mensaje_conclusion"> </div> --}}

<!-- Editable table -->

<div class="row justify-content-center con" style="display:none;"> 
<strong><label>Eliminar Conclusion</label></strong>
</div>

<div class="row justify-content-center"> 

@foreach ($conclusion as $element)

    <td> 
      <form action="{{ route('conclusion.eliminar', $element->cr_id ) }}" method="post" >
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
                                    
              <button type="submit"  class="btn btn-danger btn-sm px-2 bord-rad" onclick="conclusion();">
                                      <i class="fa fa-trash icon-bas" ></i>
              </button>                              

            </form>
              </td>
@endforeach 
</div>


<script type="text/javascript">
function conclusion(){
  localStorage.removeItem('editar_conclusion');
}
$(function(){
 if (localStorage.getItem('id_revision_editar')) {
  $("input[name='cr_revid']").val(localStorage.getItem('id_revision_editar'));
 }

 if (localStorage.getItem('conclusion')) {
     var mensaje = ("<div class='alert alert-success'>"+'Conclusion registrada Exitosamente..!!'+"</div>");
              $('#prueba').html(mensaje);
              $('#prueba').fadeOut(3000);
              $('#prueba').removeAttr('style', 'display:none');
      localStorage.removeItem('conclusion');
      localStorage.setItem('editar_conclusion',true);
 }

 if (localStorage.getItem('editar_conclusion')) {
  
  $('#modal_con').attr('disabled',true);
  $('.con').attr('style','display:visibility')
 }


})
        
</script>
