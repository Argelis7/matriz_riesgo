<head>
    <title>Registrar Nueva Revision</title>
</head>

@extends("menus.menurevision") 

  @section("dos")

    @if (session('mensaje'))
      <div class="alert alert-success" id="mensaje1">{{ session('mensaje') }}</div>
    @endif

    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="card ">
          <div class="card-header  red darken-4 text-white"> 
            <h5>Revisión - <i>(Registrar)</i></h5> 
          </div>
            <div class="card-body">
              <div class="tab-content">            

               @include('formularios.revision.FichaRevision')           
                                            
              </div>
                                                                    
              <div class="tab-pane fade" id="riesgos">
                <div id="mensaje_inhe"></div>         
                  <div class="card borde">
                     <div class="card-header barra grey darken-3"> Consultar riesgos de la revisión </div>
                          <div class="card-body">
                            <div class="row justify-content-center mt-4">                              

                                <span id="agregar_revision">
                                {{--  <a>
                                  <button type="button" class="btn btn-default btn-sm  "  aria-expanded="false" id="agregar">
                                    Agregar nuevo riesgo &nbsp 
                                    <i class="fa  fa-plus-square"></i>
                                  </button></a> --}}
                               
                                   <button 
                                href="#ventana1" 
                                type="button" 
                                class="btn blue darken-4 btn-sm d-control" 
                                id="agregar"  
                                aria-expanded="false" 
                                data-toggle="modal" >
                                
                                Agregar nuevo riesgo &nbsp;

                                <i class="fa  fa-plus-square icono-plus"></i>

                              </button>
                                </span>
                                
                                
                           </div>

                                
                                <br>


    {{--           <div class="container-fluid"> 
                <div class="row"> --}}
                  
                 <table id="inherente_table" class="table table-striped table-bordered table-responsive-lg" width="100%">
                      <thead>
                        <tr>

                          <th>Fecha
                          </th>      
                          <th class="th-sm">Nombre
                          </th>
                          <th class="th-sm">Clasificacion
                          </th>
                          <th class="th-sm">Responsable
                          </th>                  
                          <th class="th-sm">Acciones</th>

                        </tr>
                      </thead>                   
                     {{-- <tbody> --}}
                       @foreach ($riesgosInherentes as $riesgoInherente)
                        <tr>
                          <td >{{ $riesgoInherente->created_at }}</td>
                          <td>{{ $riesgoInherente->rsg_nombre }}</td>
                          <td>{{ $riesgoInherente->rsg_clasf }}</td>
                          <td>{{ $riesgoInherente->rsg_responsable }}</td>
                         
                          <td>
                            <div class="conatiner">
                                <div class="row justify-content-center">

                                   <a href="{{  route('editar.riesgos', $riesgoInherente->rsg_id) }}">
                                     <button class="btn btn-primary btn-sm px-2 bord-rad" id="gestion_riesgo" onclick="IdRiesgoInherente({{ $riesgoInherente->rsg_id }});">
                                      <i class="fa fa-edit icon-bas" ></i>
                                      </button>                                     
                                      
                                   </a>

                                    <form action="{{ route('riesgoInherente.eliminar',$riesgoInherente->rsg_id) }}" method="post" >
                                      {{ csrf_field() }}
                                      {{ method_field('DELETE') }}
                                      
                                      <button type="submit"  class="btn btn-danger btn-sm px-2 bord-rad borrar_fila" value='{{ $riesgoInherente->rsg_id }}' name="eliminarRiesgo" id="eliminar_Riesgo">
                                        <i class="fa fa-trash icon-bas" ></i>
                                      </button>                              

                                    </form>
                                  
                                </div>
                              </div>


                            
                            

                          </td>
                        </tr>
                      @endforeach                 
                    {{-- </tbody> --}}
                   
                     </table>  

                      <p class="oculto-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              {{-- </div>
            </div> --}}
          </div>
        </div>
      </div>
 
    
   
{{-- <script type="text/javascript">
  $(function(){
    $("#gestion_riesgo").click(function(){
      alert("hola");
    });
  });
</script> --}}
               

                                 
                        <div class="tab-pane fade" id="matrices">
                          <div class="card borde">
                               <div class="card-header grey darken-3 barra"> Matrices </div>
                                 <div class="card-body">                         
                                    @include("diseño.dibujo")
                                 </div>
                             </div>
                         </div> 

                        <div class="tab-pane fade" id="unidades">
                          <div class="card borde">
                             <div class="card-headergrey darken-3 barra"> Unidades involucradas </div>
                                 <div class="card-body">
                                   @include("formularios.revision.unidades.unidades")
                                  </div>
                          </div>
                        </div>
                                 
                                
                               
                        <div class="tab-pane fade" id="conclusion">
                          <div class="card borde">
                             <div class="card-header grey darken-3 barra"> Conclusiones de la revisión </div>
                                 <div class="card-body">
                                    @include("formularios.revision.conclusiones.conclusiones")
                                  </div>
                          </div>
                        </div>
                                

                    
                                       
  
         </div>
        </div>
       </div>
      </div>


      

@endsection




