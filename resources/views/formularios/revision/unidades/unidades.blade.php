<style type="text/css">
  .seleccionada{
    background: #3da6f375;
    color:white;
  }

  .tamaño{
    font-size: 12px !important;
  }
</style>
<!-- Editable table -->                         
      <div id="content"> 
       
            <div class="row justify-content-center mt-2">
       
              <!-- Boton para llamar modal de unidades -->
               <button 
                  href="#ventana3" 
                  type="button" 
                  class="btn blue darken-4 btn-sm d-control" 
                  id="agregar_u" 
                  aria-expanded="false" 
                  data-toggle="modal">

                    Agregar Nueva Unidad &nbsp;

                  <i class="fa  fa-plus-square icono-plus"></i>

               </button>
               <!-- ! Boton para llamar modal de unidades -->
             </div>

           <table id="table-unidades" class="table table-striped table-bordered table-responsive-lg" width="100%">
                      <thead>
                        <tr>
 
                          <th class="th-sm">Nombre y Apellido
                          </th>
                          <th class="th-sm">Gerencia General
                          </th>
                          <th class="th-sm">Gerencia Corportiva
                          </th>
                          <th class="th-sm">Coordinacion</th>
                          <th>Tipo de Unidad de Evaluacion</th>
                          <th>Acciones</th>

                        </tr>
                      </thead>                   
                     <tbody>
                       @foreach ($unidad as $uni)
                        <tr>                       
                          <td>{{ $uni->uni_nomape }}</td>
                          <td>{{ $uni->uni_ggeneral }}</td>
                          <td>{{ $uni->uni_gcorporativa }}</td>
                          <td>{{ $uni->uni_coordinacion }}</td>
                          <td>{{ $uni->uni_tunidad_evaluacion }}</td>
                         
                          <td class="justify-content-center">
                            <form action="{{ route('unidad.eliminar', $uni->uni_id) }}" method="post" >
                              {{ csrf_field() }}
                              {{ method_field('DELETE') }}
                              
                              <button 
                                      type="submit"  
                                      class="btn btn-danger btn-sm px-2 bord-rad borrar_fila" 
                                      id="eliminar_unidad" 
                                      name="eliminarUnidad">
                                <i class="fa fa-trash icon-bas" ></i>
                              </button>                              

                            </form>
                          </td>
                        </tr>
                      @endforeach                 
                    </tbody>
                   
                     </table>  

          <p class="oculto-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>   
       
              
              {{-- tabla emergente --}}
              <div class="modal fade" id="ventana3" role="dialog" data-backdrop="static">

                @include("formularios.revision.unidades.modalunidades")

              </div>
        </div>
   
<script type="text/javascript">
      


$(function(){
 if (localStorage.getItem('si')) {
  $("#agregar_u,#eliminar_u").prop('disabled', 'true');
  $('#agregar_u').parent().removeAttr('data-toggle');
 }
})        



                     

//   $(document).ready(function(){
//     $('#bt_add').click(function(){
//       agregar();
//     });
//       $('#bt_del').click(function(){
//         eliminar(id_fila_selected);
//       })
//   });

// var cont=0;
//     function agregar(){
//       cont++;
//       var fila='<tr id="fila'+cont+'" onclick="seleccionar(this.id);"><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>'
//       $('#tabla').append(fila);
//     }

//     function seleccionar(id_fila){
//     if($('#'+id_fila).hasClass('seleccionada'))
//       {
//       $('#'+id_fila).removeClass('seleccionada');
//     }
//     else{
//       $('#'+id_fila).addClass('seleccionada');
//     }
//     id_fila_selected=id_fila;    
//   }

//   function eliminar(id_fila){  
//     $('#'+id_fila).remove();
//   }
</script>