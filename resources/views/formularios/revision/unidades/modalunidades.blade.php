{{--ventana emergente --}}

<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
     <br>
                    
{{-- consultas controles del riesgo  --}}

<div class="container-fluid">
 <div class="card">
   <div class="card-header blue darken-4 text-white" align="center"> Nueva Unidad </div>
     <div class="card-body">
      <form action="{{ route('registrarRevision') }}" method="post" id="form_unidad">
        
        <div class="row">
                   
          <div class="col-md-4">
            <div class="form-group">
              <label>Nombre y Apellido</label>
              <input type="text" required name="uni_nomape" id="nom_ape" class="form-control">
            </div>
          </div>

          <div class="col-md-4">        
            <div class="form-group">
              <label>Gerencia General</label>
              <input type="text" required name="uni_ggeneral" class="form-control">              
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Gerencia Corporativa</label>
              <input type="text" required name="uni_gcorporativa" class="form-control">                                
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Coordinacion</label>
              <input type="text" required name="uni_coordinacion" class="form-control">                                
            </div>
          </div>

          <div class="col-md-8">
            <div class="form-group">
              <label>Correo Electronico</label>
              <input type="email" required name="uni_email" class="form-control">            
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Telefono</label>
                <input type="text" required name="uni_telf" class="form-control">            
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
               <label>Tipo de unidad dentro de la evaluacion</label>
             <input type="text" required name="uni_tunidad_evaluacion" class="form-control">            
            </div>
          </div>
                      
          {{-- input para asociar unidad con revision id de revision --}}
            <input type="hidden" name="uni_revid">                                                       
          
        </div>

        {{-- boton de guardar y cancelar --}}
         <div class="row justify-content-center">
                <div class="fcol-md-12">

                  <button type="button" class="btn btn-danger btn-sm pull-left "  aria-expanded="false" data-dismiss="modal" id="cancelar_unidad">Cancelar</button>&nbsp

                  <input type="submit" class="btn btn-success btn-sm pull-right " aria-expanded="false" value="Guardar">

                  <input type="hidden" name="formulario_revision" value="unidades">

                </div>
         </div>



         </form>
        </div>
       </div>
      </div>
{{-- contenido de la ventana --}}
     </div>
    </div>
   </div>

