<head>
    <title>Editar Revision</title>
</head>

@extends("menus.menurevision") 

  @section("dos")

    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="card ">
          <div class="card-header  red darken-4 text-white"> 
            <h5>Revisión - <i>(Editar)</i></h5> 
          </div>
            <div class="card-body">
              <div id="prueba"></div>
               @if (session('conclusion'))
                <div class="alert alert-success" id="mensaje1">{{ session('conclusion') }}</div>
              @endif
              <div class="tab-content">            

               @include('formularios.revision.FichaRevision')           
                                            
              </div>

             
                                                                    
              <div class="tab-pane fade" id="riesgos">
                <div id="mensaje_inhe"></div>         
                  <div class="card borde">
                     <div class="card-header barra grey darken-3"> Consultar riesgos de la revisión </div>
                          <div class="card-body">
                            <div class="row justify-content-center mt-4">                              

                                <span id="agregar_revision">
                                {{--  <a>
                                  <button type="button" class="btn btn-default btn-sm  "  aria-expanded="false" id="agregar">
                                    Agregar nuevo riesgo &nbsp 
                                    <i class="fa  fa-plus-square"></i>
                                  </button></a> --}}
                               
                                   <button 
                                href="#ventana1" 
                                type="button" 
                                class="btn blue darken-4 btn-sm d-control" 
                                id="agregar"  
                                aria-expanded="false" 
                                data-toggle="modal" >
                                
                                Agregar nuevo riesgo &nbsp;

                                <i class="fa  fa-plus-square icono-plus"></i>

                              </button>
                                </span>
                                
                                
                           </div>

                                
                                <br>


    {{--           <div class="container-fluid"> 
                <div class="row"> --}}
                  
                 <table id="inherente_table" class="table table-striped table-bordered table-responsive-lg" width="100%">
                      <thead>
                        <tr>

                          <th>Fecha
                          </th>      
                          <th class="th-sm">Nombre
                          </th>
                          <th class="th-sm">Clasificacion
                          </th>
                          <th class="th-sm">Responsable
                          </th>                  
                          <th class="th-sm">Acciones</th>

                        </tr>
                      </thead>                   
                     {{-- <tbody> --}}
                       @foreach ($riesgosInherentes as $riesgoInherente)
                        <tr>
                          <td>{{ $riesgoInherente->created_at }}</td>
                          <td>{{ $riesgoInherente->rsg_nombre }}</td>
                          <td>{{ $riesgoInherente->clasificacionRiesgo->clasfrsg_descripcion }}</td>
                          <td>{{ $riesgoInherente->rsg_responsable }}</td>
                         
                          <td>
                            <div class="conatiner">
                                <div class="row justify-content-center">

                                   <a href="{{  route('editar.riesgos', $riesgoInherente->rsg_id) }}">
                                     <button class="btn btn-primary btn-sm px-2 bord-rad" id="gestion_riesgo" onclick="IdRiesgoInherente({{ $riesgoInherente->rsg_id }});">
                                      <i class="fa fa-edit icon-bas" ></i>
                                      </button>                                     
                                      
                                   </a>

                                    <form action="{{ route('riesgoInherente.eliminar',$riesgoInherente->rsg_id) }}" method="post" >
                                      {{ csrf_field() }}
                                      {{ method_field('DELETE') }}
                                      
                                      <button type="submit"  class="btn btn-danger btn-sm px-2 bord-rad borrar_fila mr-3" value='{{ $riesgoInherente->rsg_id }}' name="eliminarRiesgo" id="eliminar_Riesgo">
                                        <i class="fa fa-trash icon-bas" ></i>
                                      </button>                              

                                    </form>
                                  
                                </div>
                              </div>


                            
                            

                          </td>
                        </tr>
                      @endforeach                 
                    {{-- </tbody> --}}
                   
                     </table>  

                      <p class="oculto-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              {{-- </div>
            </div> --}}
          </div>
        </div>
      </div>
 
    
   
{{-- <script type="text/javascript">
  $(function(){
    $("#gestion_riesgo").click(function(){
      alert("hola");
    });
  });
</script> --}}
               

                                 
                        <div class="tab-pane fade" id="matrices">
                          <div class="card borde">
                               <div class="card-header grey darken-3 barra"> Matrices </div>
                                 <div class="card-body">                         
                                    @include("diseño.dibujo")
                                 </div>
                             </div>
                         </div> 


                        <div class="tab-pane fade" id="unidades">
                          <div id="mensaje_uni"></div>
                           <div class="card borde">
                             <div class="card-header grey darken-3 barra"> Unidades involucradas </div>
                                 <div class="card-body">
                                   @include("formularios.revision.unidades.unidades")
                                  </div>
                          </div>
                        </div>
                                 
          
                               
                        <div class="tab-pane fade" id="conclusion">

                          <div class="card borde">
                             <div class="card-header grey darken-3 barra"> Conclusiones de la revisión </div>
                                 <div class="card-body">
                                    @include("formularios.revision.conclusiones.conclusiones")
                                  </div>
                          </div>
                        </div>
                                

                    
                                       
  
         </div>
        </div>
       </div>
      </div>

<script type="text/javascript">
// $(function(){

//   $("#agregar").click(function() {
    
//   let id_RevisionEditar = localStorage.getItem("id_revision_editar");
// location.href ="http://127.0.0.1:8000/registrariesgo/" + id_RevisionEditar;
// location.href ="http://127.0.0.1:8000/registrariesgo/1";
//   console.log(window.location.href);
//   let counter = 0;
//   let cadena = '';

//   for (const value of Array.from(window.location.href)) {
//     if (value === '/') {
//       counter++;
//     }
//     if (counter < 3) {
//       cadena = cadena.concat(value);
//       continue;
//     } else {
//       break;
//     }
//   }

//   console.log(cadena+ '/registrariesgo/1');

// location.href = cadena + '/registrariesgo/' + id_RevisionEditar;


  // var url = ' url('/registrariesgo/') }}';
  // let direccion = url + '/' + id_RevisionEditar;
  // var ruta = window.location.href;
  // console.log(ruta)
  // window.location = "registrariesgo/" + 1;
  // location.href = 'http://127.0.0.1:8000/registrariesgo/1';
  // location.href = cadena+ '/registrariesgo/1';

//   });
// })

$(function(){
 if (localStorage.getItem('si')) {
  $("#agregar").prop('disabled', 'true');
  $('#agregar').parent().attr("href", "#");
 }
})


  $(function(){
var contador=0;
    $('#matricesr').click(function() {

      if (localStorage.getItem("matriz")) {
      ///////////////////////////////////////////////////////////
     // variables guardadas para dibujar el riesgo controlado //
    ///////////////////////////////////////////////////////////
    localStorage.removeItem('matriz');
    
        var conse_x;
        var proba_y;
        var cant;
        var cantC;
        var indiceRI;
        var indiceCCP;
        var i=0;
        var promedio_TraPrevent;

        var sumaCC= [];
        var SCPA = [];
        var CCC = [];
        var CTC = [];
        var PCP = [];
        var PTP = [];
        var promediosCC = [];
        var porcTI = [];
        var promediosCP = [];   
        var porcTIP = [];
        var ConsProbC = [];
        var ConsProbCP = [];
        var CCPA = [];
        var C= [];
        var promediosTC = [];
        var promediosTP = [];
        var ConsProbT = [];
        var ConsProbTP = [];


        /////////////////////////////////////////////////////////////
        // se obtienen los datos para dibujar el RIESGO INHERENTE  //
        /////////////////////////////////////////////////////////////


        @foreach ($riesgosInherentes as $indiceRI => $riesgoInherente)

          indiceRI = "{{ $indiceRI }}"
          conse_x = "{{ $riesgoInherente->rsg_ricons }}"               
          proba_y = "{{ $riesgoInherente->rsg_riprob }}"       

          //////////////////////////////
          // concatenamos los valores //
          //////////////////////////////         
          
          var ConsProb = 'p' + conse_x + proba_y;
          var ConsProbRi = 'pi' + conse_x + proba_y; 


          //////////////////////////////////////////////////////////
          // aqui dibujo el riesgo inherente en la matriz grande  //
          //////////////////////////////////////////////////////////
          
          $("#" + ConsProb).prepend('<div class="inherente"></div>');

          //////////////////////////////////////////////////////////////
          // aqui se dibuja el riesgo inherente en la matriz pequeña  //
          //////////////////////////////////////////////////////////////

          if ($( "#" + ConsProbRi ).hasClass( "inherente_pe" )) {

              cant = $( "#" + ConsProbRi ).text(); 
              cant =  Number(cant) + 1;
              $("#" + ConsProbRi).addClass('inherente_pe').text(cant);         
          }
          else{
              $("#" + ConsProbRi).addClass('inherente_pe').text(1);
          }   

          
                            ///////////////////////////////////////
                            // para dibujar el riesgo CONTROLADO //
                            ///////////////////////////////////////
                            
                    /////////////////////////////////////////////////////////
                    // para calcular la CONSECUENCIA del riesgo controlado //
                    /////////////////////////////////////////////////////////

           @foreach ($sumaControlCorrectivo_r as $indiceSCC =>  $sumaControlCorrectivo )
             var indiceSCC = "{{ $indiceSCC }}";

            // pregunto si es el mismo indice para que guarde el valor en un array 
            
             if (indiceSCC === indiceRI) {
              sumaControlCorrectivo = "{{ $sumaControlCorrectivo }}";
             }            
            @endforeach    

            @foreach ($CantidadControlCorrectivo_r as $indiceCCC => $CantidadControlCorrectivo)
             var indiceCCC = "{{ $indiceCCC }}";

             if (indiceCCC === indiceRI) {
               CantidadControlCorrectivo = "{{ $CantidadControlCorrectivo }}";
             }                
            @endforeach 

            /////////////////////////////////////////////////////////
            // calcular promedio de riesgo controlado (CORRECTIVO) //
            /////////////////////////////////////////////////////////

            if (CantidadControlCorrectivo >0) {

              for (var i = 0; i < CantidadControlCorrectivo; i++) {                

               promedio_ConCorrec = Math.ceil (sumaControlCorrectivo / CantidadControlCorrectivo);
               PorcPromedio_ConCorrec = promedio_ConCorrec / 100 ;
               PorcTrata_implemen = 0; 

               promediosCC.push(PorcPromedio_ConCorrec);
               porcTI.push(PorcTrata_implemen);
                    
              }  

            }else{

              PorcPromedio_ConCorrec = 0 ;
              PorcTrata_implemen = 0; 

              promediosCC.push(PorcPromedio_ConCorrec);   
              porcTI.push(PorcTrata_implemen);

            }      
              
          // console.log(promediosCC[indiceRI]);         

                     /////////////////////////////////////////////////////////
                    // para calcular la PROBABILIDAD del riesgo controlado //
                    /////////////////////////////////////////////////////////
          @foreach ($sumaControlPreventivo_r as $indiceSCP =>  $sumaControlPreventivo )
            var indiceSCP = "{{ $indiceSCP }}";      

            if (indiceSCP === indiceRI) {
            var sumaControlPreventivo = "{{ $sumaControlPreventivo }}";
            }             

          @endforeach  

          @foreach ($CantidadControlPreventivo_r as $indiceCCP => $CantidadControlPreventivo)
            var indiceCCP = "{{ $indiceCCP }}";

            if (indiceCCP === indiceRI) {
            var CantidadControlPreventivo = "{{ $CantidadControlPreventivo }}"; 
            }               

          @endforeach 

               /////////////////////////////////////////////////////////
              // calcular promedio de riesgo controlado (CORRECTIVO) //
              /////////////////////////////////////////////////////////

            if (CantidadControlPreventivo > 0) {

              for (var i = 0; i < CantidadControlPreventivo; i++) {                

               promedio_ConPrevent = Math.ceil (sumaControlPreventivo / CantidadControlPreventivo);
               PorcPromedio_ConPrevent = promedio_ConPrevent / 100;                 
               PorcTrata_implemenP = 0; 

               promediosCP.push(PorcPromedio_ConPrevent);
               porcTIP.push(PorcTrata_implemenP);
                    
              }  

            }else{

              PorcPromedio_ConPrevent = 0;
              PorcTrata_implemenP = 0;

              promediosCP.push(PorcPromedio_ConPrevent);
              porcTIP.push(PorcTrata_implemenP);

            } 



                                    ////////////////////////////////////
                                    // para dibujar el riesgo TRATADO //
                                    ////////////////////////////////////
                                  
                          //////////////////////////////////////////////////////
                          // para calcular la CONSECUENCIA del riesgo tratado //
                          //////////////////////////////////////////////////////
                          
            @foreach ($sumaTratamientoCorrectivo_r as $indiceSTC =>  $sumaTratamientoCorrectivo )
             var indiceSTC = "{{ $indiceSTC }}";     

              if (indiceSTC === indiceRI) {
              var sumaTratamientoCorrectivo = "{{ $sumaTratamientoCorrectivo }}";
              }              

            @endforeach 

            @foreach ($CantidadTratamientoCorrectivo_r as $indiceCTC => $CantidadTratamientoCorrectivo)
             var indiceCTC = "{{ $indiceCTC }}";

             if (indiceCTC === indiceRI) {
             var CantidadTratamientoCorrectivo = "{{ $CantidadTratamientoCorrectivo }}";                  
             }                

            @endforeach 

                          //////////////////////////////////////////////////////
                          // calcular promedio de riesgo tratado (CORRECTIVO) //
                          //////////////////////////////////////////////////////
                          

            if (CantidadTratamientoCorrectivo > 0) {

              for (var i = 0; i < CantidadTratamientoCorrectivo; i++) {                

               promedio_TraCorrec = Math.ceil (sumaTratamientoCorrectivo / CantidadTratamientoCorrectivo);
               PorcPromedio_TraCorrec = promedio_TraCorrec / 100 ;
               PorcTrata_implemen = 0; 

               promediosTC.push(PorcPromedio_TraCorrec);
               porcTI.push(PorcTrata_implemen);
                    
              }  

            }else{

               PorcPromedio_TraCorrec = 0;
               PorcTrata_implemen = 0; 

               promediosTC.push(PorcPromedio_TraCorrec);
               porcTI.push(PorcTrata_implemen);

            }


             // console.log(promediosCC[indiceRI]);
             // console.log(promediosCP[indiceRI]);
             // console.log(promediosTC[indiceRI]);

                          /////////////////////////////////////////////////
                          // calcular la PROBABILIDAD del riesgo tratado //
                          /////////////////////////////////////////////////

            @foreach ($sumaTratamientoPreventivo_r as $indiceSTP =>  $sumaTratamientoPreventivo )
              var indiceSTP = "{{ $indiceSTP }}";  
                
              if (indiceSTP === indiceRI) {
              var sumaTratamientoPreventivo = "{{ $sumaTratamientoPreventivo }}";                
              }               
            @endforeach 

            @foreach ($CantidadTratamientoPreventivo_r as $indiceCTP => $CantidadTratamientoPreventivo)

              var indiceCTP = "{{ $indiceCTP }}";

              if (indiceCTP === indiceRI) {                
              var CantidadTratamientoPreventivo = "{{ $CantidadTratamientoPreventivo }}";
              }                

             @endforeach

                          //////////////////////////////////////////////////////
                          // calcular promedio de riesgo tratado (PREVENTIVO) //
                          //////////////////////////////////////////////////////
                          

            if (CantidadTratamientoPreventivo > 0) {

              for (var i = 0; i < CantidadTratamientoPreventivo; i++) {                

               promedio_TraPrevent = Math.ceil (sumaTratamientoPreventivo / CantidadTratamientoPreventivo);
               PorcPromedio_TraPrevent = promedio_TraPrevent / 100 ;
               PorcTrata_implemenP = 0; 

               promediosTP.push(PorcPromedio_TraPrevent);
               porcTIP.push(PorcTrata_implemenP);
                    
              }  

            }else{

               PorcPromedio_TraPrevent = 0;
               PorcTrata_implemenP = 0; 

               promediosTP.push(PorcPromedio_TraPrevent);
               porcTIP.push(PorcTrata_implemenP);

            }

       
            // console.log(promediosTP[indiceRI]);

               

          
          //////////////////////////////////////////////////////////
          // obtengo el valor del promedio del CONTROL CORRECTIVO //
          //////////////////////////////////////////////////////////
          // console.log(SCP[indiceRI]);          
          // console.log(conse_x + proba_y);

          // console.log(promediosCP[indiceRI]);          
          // console.log(porcTIP[indiceRI]);  
          // console.log(CCPA[indiceRI]);   
          // console.log(SCP[indiceRI]);          
               


                              ////////////////////////////////////////////////////
                              // calcular CONSECUENCIA del control (CORRECTIVO) //
                              ////////////////////////////////////////////////////
                              
          var ConsCorrectivo = [conse_x - (conse_x * promediosCC[indiceRI]) - (conse_x * porcTI[indiceRI])];    

          if (ConsCorrectivo < 1) {
             var ConseContCorrectivo = 1;
             CCC.push(ConseContCorrectivo);

          }else{
             var ConseContCorrectivo = Math.ceil(ConsCorrectivo);
             CCC.push(ConseContCorrectivo);             
          }  


                            /////////////////////////////////////////////////////////
                            // calcular la PROBABILIDAD del control (PROBABILIDAD) //
                            /////////////////////////////////////////////////////////
        
          var ProbPreventivo = [proba_y - (proba_y * promediosCP[indiceRI]) - (proba_y * porcTIP[indiceRI])]; 

          if (ProbPreventivo < 1) {
             var ProbContPreventivo = 1;
             PCP.push(ProbContPreventivo);
          }else{
             var ProbContPreventivo = Math.ceil(ProbPreventivo);
             PCP.push(ProbContPreventivo);
          }

                          //////////////////////////////////////////////////////////////////
                          // concatenamos ambos valores para dibujar el RIESGO CONTROLADO //
                          //////////////////////////////////////////////////////////////////
                        
            var ConsProbControl = 'p' +  CCC[indiceRI] + PCP[indiceRI];
            ConsProbC.push(ConsProbControl);
            var ConsProbControl_c = 'pc' +  CCC[indiceRI] + PCP[indiceRI];
            ConsProbCP.push(ConsProbControl_c);
            
             
          if (CantidadControlCorrectivo > 0 || CantidadControlPreventivo > 0) {     
          
            $("#" + ConsProbC[indiceRI]).prepend('<div class="controlado"></div>');

             

            //////////////////////////////////////////////////////////////
            // aqui se dibuja el riesgo CONTROLADO en la matriz pequeña //
            //////////////////////////////////////////////////////////////

            if ($( "#" + ConsProbControl_c ).hasClass( "controlado_pe" )) {

                cantC = $( "#" + ConsProbControl_c ).text(); 
                cantC =  Number(cantC) + 1;
                $("#" + ConsProbControl_c).addClass('controlado_pe').text(cantC);         
            }
            else{
                $("#" + ConsProbControl_c).addClass('controlado_pe').text(1);
            } 

          } 

                        ///////////////////////////////////////////////////////////
                        // calcular la CONSECUENCIA del tratamiento (CORRECTIVO) //
                        ///////////////////////////////////////////////////////////
                        
           var ConsTraCorrectivo = [conse_x - (conse_x * promediosCC[indiceRI]) - (conse_x * promediosTC[indiceRI]) - (conse_x * porcTI[indiceRI])];     

              if (ConsTraCorrectivo < 1) {
                 var ConseTraCorrectivo = 1;
                 CTC.push(ConseTraCorrectivo);
              }else{
                 var ConseTraCorrectivo = Math.ceil(ConsTraCorrectivo);
                 CTC.push(ConseTraCorrectivo);
              }

                        ///////////////////////////////////////////////////////////
                        // calcular la PROBABILIDAD del tratamiento (PREVENTIVO) //
                        ///////////////////////////////////////////////////////////
            
                              
          var ProbTraPreventivo = [proba_y - (proba_y * promediosCP[indiceRI]) - (proba_y * promediosTP[indiceRI]) - (proba_y * porcTIP[indiceRI])];   

            if (ProbTraPreventivo < 1) {
               var ProbaTraPreventivo = 1;
               PTP.push(ProbaTraPreventivo);
            }else{
               var ProbaTraPreventivo = Math.ceil(ProbTraPreventivo);
               PTP.push(ProbaTraPreventivo);               
            }

                      ///////////////////////////////////////////////////////////////
                      // concatenamos ambos valores para dibujar el RIESGO TRATADO //
                      ///////////////////////////////////////////////////////////////

            var ConsProbTrata = 'p' +  CTC[indiceRI] + PTP[indiceRI];
            ConsProbT.push(ConsProbTrata);
            var ConsProbTrata_t = 'pt' +  CTC[indiceRI] + PTP[indiceRI];
            ConsProbTP.push(ConsProbTrata_t);
            
             
          if (CantidadTratamientoCorrectivo > 0 || CantidadTratamientoPreventivo > 0) {     
          
            $("#" + ConsProbT[indiceRI]).prepend('<div class="tratado"></div>');

                      ///////////////////////////////////////////////////////////
                      // aqui se dibuja el riesgo TRATADO en la matriz pequeña //
                      ///////////////////////////////////////////////////////////
                      

            if ($( "#" + ConsProbTrata_t ).hasClass( "tratado_pe" )) {

                cantT = $( "#" + ConsProbTrata_t ).text(); 
                cantT =  Number(cantT) + 1;
                $("#" + ConsProbTrata_t).addClass('tratado_pe').text(cantT);         
            }
            else{
                $("#" + ConsProbTrata_t).addClass('tratado_pe').text(1);
            } 

          }         
            
            
                              
          // console.log(CCC[indiceRI]);
          // console.log(PCP[indiceRI]);
          // console.log(ConsProbC[indiceRI]);
          // console.log(ConsProbControl_c);


        @endforeach







  }
                
     });
     });

var promediosCC = [];
var porcTI = [];
function promedio(sumaControlCorrectivo,CantidadControlCorrectivo){
console.log(sumaControlCorrectivo + 'suma');
          for (var i = 0; i < CantidadControlCorrectivo; i++) {
           

             if (sumaControlCorrectivo && CantidadControlCorrectivo != 0) {

                 promedio_ConCorrec = Math.ceil (sumaControlCorrectivo / CantidadControlCorrectivo);
                 PorcPromedio_ConCorrec = promedio_ConCorrec / 100 ;
                 PorcTrata_implemen = 0; 

                 promediosCC.push(PorcPromedio_ConCorrec);
                 porcTI.push(PorcTrata_implemen);

                }else{
                  PorcPromedio_ConCorrec = 0 ;
                  PorcTrata_implemen = 0; 

                  promediosCC.push(PorcPromedio_ConCorrec);   
                  porcTI.push(PorcTrata_implemen);

                } 
          }

}

//    var miArray = [ 2, 4, 6, 8, 10 ];
// miArray.forEach( function(valor, indice, array) {
//     console.log("En el índice " + indice + " hay este valor: " + valor);
// });

        
</script>
      

@endsection




