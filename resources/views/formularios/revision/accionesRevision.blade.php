 {{-- <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script> --}}

<div class="continer">
	<div class="row justify-content-center">

		<a href="{{ route('revision.editar', $rev_id) }}">
			<button id="consulta" class="btn btn-sm btn-primary mr-2" onclick="IdRevisionEditar({{ $rev_id }});">	
				Ver/Modificar
			</button>			
		</a>

		<form action="{{ route('revision.eliminar', $rev_id) }}" method="post">

			{{ csrf_field() }}
			{{ method_field('DELETE') }}

			<button type="submit" class="btn btn-sm btn-danger borrar_fila" value="{{ $rev_id }}">Eliminar</button>
			
		</form>

		
	</div>
</div>

 {{-- <script src="{{ asset('js/js/variables localStorage.js') }}"></script> --}}


