{{-- @extends('formularios.revision') --}}

{{-- @section('contenido') --}}


<div class="tab-pane active" id="ficha">

@if (session('mensaje'))
  <div class="alert alert-success" id="mensaje1">{{ session('mensaje') }}</div>
@endif


  @if ( !isset($estado) )             
      <form action="{{ route('registrarRevision') }}" method="post" role="form" id="form">
  @else
      <form action="{{ route('actualizarRevision', $revision->rev_id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
  @endif
  {{-- <form action="{{ route('registrarRevision') }}" method="post" role="form" id="form"> --}}

<fieldset id="opcion" >

  <div class="card borde">
          
    <div class="card-header grey darken-3 barra " > Detalles de la revisión </div>
          
      <div class="card-body">
        
        <div class="row">
                   
          <div class="col-md-3">

            <div class="form-group">

             <label>Tipo de revision</label>
              <select name="rev_trev" class="form-control" id="rev_trev">
                <option value="" disabled selected>{{ old('rev_trev', $revision->rev_trev ?? 'Seleccione un tipo') }}</option>
                  @foreach($tipoRevision as $tipo)
                  <option value="{{ $tipo->trev_id }}">{{ $tipo->trev_descripcion }}</option>
                  @endforeach
              </select>
              {{-- {!! Form::select('rev_trev', [' ' => 'Seleccione'] + $tipoRevision, null, ['class' => 'form-control'] ) !!} --}}
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Fecha de inicio</label>
              <input {{-- required --}} 
                name="rev_fechainicio" 
                type="date" 
                class="form-control" 
                value="{{ old('rev_fechainicio', $revision->rev_fechainicio ?? date("Y-m-d") ) }}" min="{{ date("Y-m-d") }}" >              
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Fecha final estimada</label>
              <input {{-- required --}} 
                name="rev_fechafinal" 
                type="date" 
                class="form-control" 
                value="{{ old('rev_fechafinal', $revision->rev_fechafinal ?? date("Y-m-d") ) }}" min="{{ date("Y-m-d") }}">                                
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Fecha final real</label>
              <input {{-- required --}} 
                name="rev_fecharealfinal" 
                type="date" 
                class="form-control" 
                value="{{ old('rev_fecharealfinal', $revision->rev_fecharealfinal ?? date("Y-m-d") ) }}" min="{{ date("Y-m-d") }}">                                
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">       
              <label>Nombre de la revision </label>
              <input {{-- required --}} 
                name="rev_nombre" 
                class="form-control" 
                placeholder="Plataforma Internet Banda Ancha Residencial ABA" 
                value="{{ old('rev_nombre', $revision->rev_nombre ?? '') }}">            
            </div>
          </div>
                                
          <div class="col-md-6">
            <div class="form-group">
              <label>Gerencia General / Vicepresidencia</label>
              <input {{-- required --}} 
                name="rev_ggeneral" 
                class="form-control" 
                value="{{ old('rev_ggeneral', $revision->rev_ggeneral ?? '') }}">
            </div>

            <div class="form-group">
              <label>Coordinacion</label>
              <input {{-- required --}} 
                name="rev_coordinacion" 
                class="form-control" 
                placeholder="Ingenieria de Seguridad" 
                value="{{ old('rev_coordinacion', $revision->rev_coordinacion ?? '') }}">
            </div>
          </div>
                                                        
          <div class="col-md-6">
            <div class="form-group">
              <label>Gerencia</label>
              <input {{-- required --}} 
                name="rev_gerencia" 
                class="form-control" 
                placeholder="Seguridad de la Operación y Servicios" 
                value="{{ old('rev_gerencia', $revision->rev_gerencia ?? '') }}">                                   
            </div>
            <div class="form-group">
              <label>Responsable</label>
              <input {{-- required --}} 
                name="rev_responsable" 
                class="form-control" 
                placeholder="Ej Angel Key Especialista" 
                value="{{ old('rev_responsable', $revision->rev_responsable ?? '') }}">            
            </div>
          </div>
        </div>                       
      </div>
    </div>
    <br>

<!-- /.row atributos-->
 <div class="card borde">          
    <div class="card-header grey darken-3 barra " > Atributos </div>          
      <div class="card-body">  
           <div class="row">

             <div class="col-md-6">

               <div class="form-group">
                  <label>Objetivos</label>
                  <textarea {{-- required --}} 
                    name="rev_objetivos" 
                    class="form-control" 
                    rows="3" 
                    placeholder="Proveer servicio de ABA a los usuarios (as) a nivel nacional">
                    {{ old('rev_objetivos', $revision->rev_objetivos ?? '') }}
                  </textarea>
               </div>

               <div class="form-group">
                  <label>Alcance</label>
                  <textarea {{-- required --}} 
                    name="rev_alcances" 
                    class="form-control" 
                    rows="3" 
                    placeholder="Prestar servicio a los usuarios (as) a nivel nacional a las velocidades contratada.">
                    {{ old('rev_alcances', $revision->rev_alcances ?? '') }}</textarea>
               </div>

              </div>
                                                        
            <div class="col-md-6">
               
               <div class="form-group">
                <label>Metas</label>
                <textarea {{-- required --}} 
                  name="rev_metas" 
                  class="form-control" 
                  rows="3" 
                  placeholder="Mantener la calidad de servicio ABA a los usuarios (as) a nivel nacional">
                  {{ old('rev_metas', $revision->rev_metas ?? '') }}
                </textarea>
               </div>

               <div class="form-group">
                 <label>Relacion con otros (interdependencia)</label>
                 <textarea {{-- required --}} 
                   name="rev_relacion" 
                   class="form-control" 
                   rows="3" 
                   placeholder="Elementos de Red de Backbone IP">
                   {{ old('rev_relacion', $revision->rev_relacion ?? '') }}
                 </textarea>
               </div>

            </div>

           </div>
       </div>
  </div>
  <br>

 <!-- /.row ANALISIS FODA -->
 <div class="card borde">          
    <div class="card-header grey darken-3 barra " > Analisis FODA </div>          
      <div class="card-body">  
         <div class="row">

          <div class="col-md-6">

            <div class="form-group">
              <label>Fortalezas</label>
              <textarea {{-- required --}} 
                name="rev_fortalezas" 
                class="form-control" 
                rows="3" >
                {{ old('rev_fortalezas', $revision->rev_fortalezas ?? '') }}
              </textarea>
            </div>

            <div class="form-group">
              <label>Debilidades</label>
              <textarea {{-- required --}} 
                name="rev_debilidades" 
                class="form-control" 
                rows="3" >
                {{ old('rev_debilidades', $revision->rev_debilidades ?? '') }}
              </textarea>
            </div>

          </div>
                                                       
          <div class="col-md-6">
                     
            <div class="form-group">
              <label>Oportunidades</label>
              <textarea {{-- required --}} 
                name="rev_oportunidades" 
                class="form-control" 
                rows="3" >
                {{ old('rev_oportunidades', $revision->rev_oportunidades ?? '') }}
              </textarea>
            </div>

            <div class="form-group">
              <label>Amenazas</label>
              <textarea {{-- required --}} 
                name="rev_amenazas" 
                class="form-control" 
                rows="3" >
                {{ old('rev_amenazas', $revision->rev_amenazas ?? '') }}
              </textarea>
            </div>
          </div>
        </div>
      </div>
             
      
        <div class="btn-group justify-content-center" role="group" aria-label="Basic example">
          
          <a href=" {{ url('index') }}">
            <button 
              type="button" 
              class="btn btn-danger btn-sm"  
              aria-expanded="false">
              Cancelar
            </button>&nbsp;
          </a>
            
          <input 
            type="submit" 
            class="btn btn-success btn-sm" 
            id="subir" 
            aria-expanded="false" 
            value="Guardar">

            {{-- input para saber que formulario se envia --}}

          <input type="hidden" name="formulario_revision" value="Revision">

        
        </div>

      <br>
   

{{-- {{ method_field('GET') }}  --}} {{-- no funciona para mostrar en la barra de navegacion los input(averiguar) --}}
{{ csrf_field() }}

    </form>
  </div>
</fieldset>


<script type="text/javascript">

$(function(){
 if (localStorage.getItem('si')) {
  $("#opcion").prop('disabled', 'true');
  // localStorage.clear()
 }

 // if (localStorage.getItem('id_revision')) {
 //   location.href ="registrariesgo";
 // }

  // $('#subir').click(function()
  // {
  //   location.href ="registrariesgo";
  // })
})
        
</script>
