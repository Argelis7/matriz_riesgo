<head>
  <title>Registrar Riesgo</title>
</head>
<body>


@extends("menus.menuriesgo")

 @section("uno")


   <div class="container-fluid">
     <div class="row justify-content-center">
      <div class="card">
       <div class="card-header  red darken-4 text-white"> 
        <h5>Gestión del Riesgo - <i>( {{ isset($estado) ? 'Registrar Riesgo' : 'Editar Riesgo' }} )</i> </h5> 
       </div>
         <div class="card-body">
          @if (session()->has("mensaje"))

            <div id="ir" value="{{ old('elegir') }}"> </div>

          @endif
          @if ( session("mensaje1") )          
           <div class="alert alert-success" id="mensaje_apro">
              {{ session("mensaje1") }}
            </div> 
          @endif

            <div id="mensaje_inhe"></div> 


            {{-- input para obtener el id del riesgo inherente  --}}
            <input type="hidden" id="id_inherente" name="rsg_id">            
         

            {{-- ficha riesgo  --}}
            <div class="tab-content">
            @include('formularios.riesgo.fichaRiesgo')
            </div>
            {{-- ficha riesgo  --}}

            {{-- Pestaña de riesgo controlado  --}}
            
            <div class="tab-pane" id="profile2">
              <div id="mensaje_con"></div>
              <div class="card borde">          
                
                <div class="card-header grey darken-3 barra"> Consultar controles del riesgo </div>
                  <div class="card-body">                  

                    <div class="container">
                      <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 offset-md-1">
                          <div class="container">
                            <div class="row justify-content-center mt-4">
                              
                           
                             <!-- Boton para llamar modal de controlado -->    
                              <button 
                                href="#ventana1" 
                                type="button" 
                                class="btn blue darken-4 btn-sm d-control" 
                                id="b_controlado"  
                                aria-expanded="false" 
                                data-toggle="modal" >
                                
                                Agregar nuevo control &nbsp;

                                <i class="fa  fa-plus-square icono-plus"></i>

                              </button>
                             <!-- ! Boton para llamar modal de controlado -->
                            </div>  
                          </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 offset-md-1">
                          <div class="container">
                              <div class="row justify-content-center">
                                <h4>Aceptación</h4>  
                              </div>

                              <div class="row justify-content-center">
                          
                                <div class="btn-group" role="group" aria-label="Basic example">
                                  <form action="{{ route('actualizar', 'id') }}" method="post">
                                    {{-- el id que le estamos pasando por aqui no esta haciendo nada esto solo se hizo para que se lea el metodo PUT  --}}
                                                
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <input type="hidden" name="rsg_id" value="{{ session('id_inherente') }}">
                                    <input type="hidden" name="aceptadoControlado" value="1" >
                                    <input type="submit" data-si="1" class="btn blue darken-4 btn-sm redondeo-izq px-3 ml-1" value="Si" id="acp_s" name="aceptaControlado">                      
                                    <input type="hidden" name="rechazadoControlado" value="0">
                                    <input type="submit" data-si="0" class="btn danger-color-dark btn-sm redondeo-der px-3" value="No" id="acp_n" name="rechazaControlado">

                                  </form>
                                </div>
                              </div>                   
                          </div>          
                        </div>  
                      
                      </div>                       
                    </div> 

                    <hr class="mr-5 ml-5 hr">   <br>                

   
                   
                    <table id="controlado_table" class="table table-striped table-bordered table-responsive-sm " width="100%">
                      <thead>
                        <tr>

                          <th>Fecha
                          </th>      
                          <th class="th-sm">Control
                          </th>
                          <th class="th-sm">Responsable
                          </th>
                          <th class="th-sm">Unidad
                          </th>
                          <th>Tipo
                          </th>
                          <th class="th-sm">Implementación
                          </th>
                          <th class="th-sm">Documentacion
                          </th>
                          <th>Ejecución
                          </th>
                          <th>Efectividad
                          </th>
                          <th>Acciones</th>

                        </tr>
                      </thead>                   
                     {{-- <tbody> --}}
                      @foreach ($riesgosControlados as $riesgoControlado)
                        <tr>
                          <td>{{ $riesgoControlado->created_at }}</td>
                          <td>{{ $riesgoControlado->rcon_nombre }}</td>
                          <td>{{ $riesgoControlado->rcon_responsable }}</td>
                          <td>{{ $riesgoControlado->rcon_unidad }}</td>
                          <td>{{ $riesgoControlado->tipoControlTratamiento->tcr_descripcion }}</td> {{-- se debe anotar el metodo y la columna a la que referimos --}}
                          <td>{{ $riesgoControlado->implementacion->icr_descripcion }}</td>
                          <td>{{ $riesgoControlado->documentacion->dcr_descripcion }}</td>
                          <td>{{ $riesgoControlado->ejecucion->ecr_descripcion }}</td>
                          <td>{{ $riesgoControlado->rcon_efectividad }}</td>
                          
                          <td class="justify-content-center">
                            <form action="{{ route('riesgo.eliminar', $riesgoControlado->rcon_id) }}" method="post" >
                              {{ csrf_field() }}
                              {{ method_field('DELETE') }}
                              
                              <button type="submit"  class="btn btn-danger btn-sm px-2 bord-rad borrar_fila" value='{{ $riesgoControlado->rcon_id }}' name="eliminarControl">
                                <i class="fa fa-trash icon-bas" ></i>
                              </button>                              

                            </form>
                          </td>
                        </tr>
                      @endforeach                    
                    {{-- </tbody> --}}
                   
                     </table>  

                    

                     {{-- ventana emergente Agregar control MODAL--}}
                <div class="modal fade" id="ventana1" role="dialog" data-backdrop="static">
                  @include("formularios.riesgo.controlado")
                </div>
                {{-- ! ventana emergente Agregar control MODAL--}}

                 <p class="oculto-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                     </div>
                     </div>
                   </div>
                       
                 
                   
          {{-- Pestaña de riesgo tratado --}}
           <div class="tab-pane fade" id="messages2">
           <div id="mensaje_tra"></div>          
           <div id="mensaje_apro"></div>          
            <div class="card borde">
              <div class="card-header grey darken-3 barra"> Consultar tratamientos del riesgo </div>
                <div class="card-body">
                  
                 <div class="row justify-content-center mt-4">
       
                  <!-- Boton para llamar modal de tratado -->
                   <button 
                      href="#ventana2" 
                      type="button" 
                      class="btn blue darken-4 btn-sm d-control" 
                      id="b_tratado" 
                      aria-expanded="false" 
                      data-toggle="modal">

                        Agregar nuevo tratamiento &nbsp;

                      <i class="fa  fa-plus-square icono-plus"></i>


                   </button>
                   <!-- ! Boton para llamar modal de tratado -->
                 </div>

                 <hr class="mr-5 ml-5 hr">   

                    <br>
                 
                  

                  <table id="tratado_table" class="table table-striped table-bordered table-responsive-sm" width="100%">
                      <thead>
                        <tr>

                          <th>Fecha
                          </th>      
                          <th class="th-sm">Tratamientos Propuestos
                          </th>
                          <th class="th-sm">Responsable
                          </th>
                          <th class="th-sm">Unidad
                          </th>
                          <th>Tipo
                          </th>
                          <th class="th-sm">Complejidad
                          </th>
                          <th class="th-sm">Costo/Beneficio
                          </th>
                          <th>Tiempo
                          </th>  
                          <th>Efectividad
                          </th>                       
                          <th class="th-sm">Acciones</th>

                        </tr>
                      </thead>                   
                     <tbody>
                      @foreach ($riesgosTratados as $riesgoTratado)
                        <tr>
                          <td>{{ $riesgoTratado->created_at }}</td>                          
                          <td class="tra_propuestos">{{ $riesgoTratado->rtra_nombre }}</td>
                          <td>{{ $riesgoTratado->rtra_responsable }}</td>
                          <td>{{ $riesgoTratado->rtra_unidad }}</td>
                          <td>{{ $riesgoTratado->tipoControlTratado->tcr_descripcion }}</td> {{-- se debe anotar el metodo y la columna a la que referimos --}}
                          <td>{{ $riesgoTratado->complejidad->ctr_descripcion }}</td>
                          <td>{{ $riesgoTratado->costo_beneficio->cbtr_descripcion }}</td>
                          <td>{{ $riesgoTratado->tiempo->ttr_descripcion }}</td>                          
                          <td>{{ $riesgoTratado->rtra_efectividad }}</td>                          
                          
                          <td class="justify-content-center">
                            <form action="{{ route('riesgo.eliminar', $riesgoTratado->rtra_id) }}" method="post">
                              {{ csrf_field() }}
                              {{ method_field('DELETE') }}
                              
                              <div class="row">
                                <button 
                                    type="submit"  
                                    class="btn btn-danger btn-sm px-2 bord-rad borrar_fila" 
                                    value="{{ $riesgoTratado->rtra_id }}" 
                                    name="eliminarTratamiento"
                                    title="Eliminar tratamientos">

                                  <i class="fa fa-trash icon-bas" ></i>
                                </button>

                                 <button  
                                    href="#ventana4" 
                                    title="Agregar Aprobación" 
                                    type="button" 
                                    id="b_aprobacion"
                                    class="btn blue darken-4 btn-sm bord-rad px-2 aprobacion" 
                                    aria-expanded="false" 
                                    data-toggle="modal"
                                    value="{{ $riesgoTratado->rtra_id }}"
                                    >

                                    <i class="fas fa-clipboard-check icono-plus"></i>
                                </button>
                              </div>

                                                          

                            </form>
                          </td>
                        </tr>
                      @endforeach                    
                    </tbody>
                   
                     </table>  

                      {{-- ventana emergente Agregar tratamiento MODAL--}}
                      <div class="modal fade" id="ventana2" role="dialog" data-backdrop="static">
                        @include("formularios.riesgo.tratados")              
                      </div>
                      {{-- !ventana emergente Agregar control MODAL--}}

                        {{-- ventana emergente Agregar Aprobacion --}}
                       <div class="modal fade" id="ventana4" role="dialog" data-backdrop="static">
                        @include('formularios.riesgo.tratamientos.aprobacion.modal_aprobacion')
                      </div>

                       <p class="oculto-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                  </div>               

               </div>
            </div>
  

          {{-- ! Pestaña de riesgo tratado --}}

          {{-- Pestaña de matriz de riesgo --}}

          <div class="tab-pane fade" id="messages3">
            <div class="card borde">
              <div class="card-header grey darken-3 barra" >Consultar Matrices </div>
                <div class="card-body">                   
                    @include("diseño.dibujo")
                </div>
            </div>
              
              {{-- Botones para guardar --}}

                <div class="container">                 
                  <div class="row justify-content-center">
                    <div class="form-group">
                       <a href="{{ route('consulta.revision') }}">
                       <button type="button" class="btn btn-danger btn-sm "  aria-expanded="false" id="seguir" onclick="borrar();">Siguiente</button>
                       {{-- <label>Siguiente</label> --}}
                       {{--  <img src="imagenes/pasar.png" class="img-fluid" width="100px" height="0" onclick="borrar();"> --}}
                        {{-- <button class="btn btn-success">Siguiente</button> --}}
                     </a>
                       
                    </div>
                  </div>
                </div>

           {{-- Botones para guardar --}}

          </div>

          {{-- ! Pestaña de matriz de riesgo --}}

            {{-- pestaña de aprobacion  --}}
            <div class="tab-pane fade" id="aprobacion">
              <div class="card borde">
                <div class="card-header grey darken-3 barra"> Consultar aprobación </div>
                  <div class="card-body">
                     @include('formularios.riesgo.tratamientos.aprobacion.aprobacion')
                  </div>
              </div>
            </div>            
            {{-- fin de pestaña  --}}

            {{-- pestaña de aprobacion  --}}
            <div class="tab-pane fade" id="implementacion">
              <div class="card borde">
                <div class="card-header grey darken-3 barra">Consultar Implementacion de tratamientos </div>
                  <div class="card-body">
                     @include('formularios.riesgo.tratamientos.implementacion.implementacion')
                  </div>
              </div>
            </div>            
            {{-- fin de pestaña  --}}

        </div>
      </div>
    </div>
  </div>
    

   
@endsection

   </body>
</html>



