<style type="text/css">
  .seleccionada{
    background: #3da6f375;
    color:white;
  }

 .tamaño{
    font-size: 12px !important;
  }
</style>
<!-- Editable table -->
      <div id="content" >     

            {{-- <div class="row justify-content-center mt-2"> --}}
       
              <!-- Boton para llamar modal de unidades -->
               {{-- <button 
                  href="#ventana4" 
                  type="button" 
                  class="btn blue darken-4 btn-sm d-control" 
                  id="b_aprobacion" 
                  aria-expanded="false" 
                  data-toggle="modal">

                    Agregar Nueva Aprobación &nbsp;

                  <i class="fa  fa-plus-square icono-plus"></i>

               </button> --}}
               <!-- ! Boton para llamar modal de unidades -->
               {{-- <h5>Historial de Aprobaciones de Tratamientos</h5> --}}
             {{-- </div> --}}
    {{-- <hr class="mr-5 ml-5 hr">  --}}
          
          <table id="aprobacion_table" class="table table-striped table-bordered table-responsive-lg" width="100%">
                      <thead>
                        <tr>

                          <th>Nombre del Tratamiento
                          </th>      
                          <th class="th-sm">Responsable de la aprobación
                          </th>
                          <th class="th-sm">Fecha de respuesta
                          </th>
                          <th class="th-sm">Estado de respuesta
                          </th>
                          <th class="th-sm">Observaciones
                          </th>                  
                          <th>Acciones</th>

                        </tr>
                      </thead>                   
                     <tbody>

                      @foreach ($aprobacion as $apro)
                        <tr>
                          <td class="nombretra">{{ $apro->aptra_nombretra }}</td>
                          <td class="responsableapro">{{ $apro->aptra_responsableapro }}</td>
                          <td class="fecharespuesta">{{ $apro->aptra_fecharespuesta }}</td>
                          <td class="estadoR">{{ $apro->estadoRiesgo->estrsg_descripcion }}</td>
                          <td class="observaciones">{{ $apro->aptra_observaciones }}</td>

                          <td class="justify-content-center apro">
                           <form action="{{ route('riesgo.eliminar', $apro->aptra_id) }}" method="post">
                             {{ csrf_field() }}
                             {{ method_field('DELETE') }}
                            <input type="hidden" name="tipo_riesgo" value="eliminarAprobacion">
                            <div class="row"> 
                               <button 
                                type="submit"
                                class="btn btn-danger btn-sm px-2 bord-rad borrar_fila"
                                value='{{-- {{ $riesgoControlado->rcon_id }} --}}' 
                                name="eliminarAprobacion">

                                 <i class="fa fa-trash icon-bas" ></i>

                               </button>                           
                                {{-- data-target lo usamos como el href y a su vez enviamos el id del usuario--}}
                               <button  
                                  data-target="#ventana5{{ $apro->aptra_id }}"
                                  title="Agregar Implementación" 
                                  type="button"                 
                                  class="btn blue darken-4 btn-sm bord-rad px-2 implementacion" 
                                  aria-expanded="false" 
                                  data-toggle="modal"
                                  value="{{ $apro->aptra_id }}"
                                  style='display:none'
                                  >
                                      
                                  <i class="fas fa-cogs icono-plus"></i>
                               </button> 

                               <button  
                                  data-target="#ventana6{{ $apro->aptra_id }}"
                                  title="Editar Aprobacion" 
                                  type="button"                        
                                  class="btn blue darken-4 btn-sm bord-rad px-2 edit_aprobacion" 
                                  aria-expanded="false" 
                                  data-toggle="modal"
                                  value="{{ $apro->aptra_id }}"
                                  style='display:none'
                                  >
                                      
                                  <i class="fa fa-edit icon-bas icono-plus"></i>
                               </button>
                             </div>                           

                           </form>
                         </td>
                        </tr>
                      @endforeach                       
                    </tbody>                   
                     </table>  

                    @foreach ($aprobacion as $apro) 
                      @if ( $apro->aptra_ert == 2 )                        
                         {{--Modal Implementacion --}}
                        <div class="modal fade" id="ventana5{{ $apro->aptra_id }}" role="dialog" data-backdrop="static">
                            @include('formularios.riesgo.tratamientos.implementacion.modal_implementacion')
                        </div>
                      @endif                     
                                  
                      @if ( $apro->aptra_ert == 1 )
                        {{-- ventana emergente Agregar Aprobacion --}}
                        <div class="modal fade" id="ventana6{{ $apro->aptra_id }}" role="dialog" data-backdrop="static">
                          @include('formularios.riesgo.tratamientos.aprobacion.modal_aprobacion_editar')
                        </div>
                      @endif

                    @endforeach

                      <p class="oculto-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>       


       
              
        </div>


        
   
<script type="text/javascript">
  

      

  $(document).ready(function(){
    if (localStorage.getItem("consulta")) {
     $("#agregar_aprob").attr("disabled", "true");
    }

  });

// var cont=0;
//     function agregar(){
//       cont++;
//       var fila='<tr id="fila'+cont+'" onclick="seleccionar(this.id);"><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>'
//       $('#tabla').append(fila);
//     }

//     function seleccionar(id_fila){
//     if($('#'+id_fila).hasClass('seleccionada'))
//       {
//       $('#'+id_fila).removeClass('seleccionada');
//     }
//     else{
//       $('#'+id_fila).addClass('seleccionada');
//     }
//     id_fila_selected=id_fila;    
//   }

//   function eliminar(id_fila){  
//     $('#'+id_fila).remove();
//   }
</script>