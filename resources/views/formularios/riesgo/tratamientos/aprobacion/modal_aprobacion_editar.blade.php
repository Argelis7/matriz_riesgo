{{--ventana emergente --}}

<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
     <br>
                    
{{-- consultas controles del riesgo  --}}

<div class="container-fluid">
     <div class="card">
       <div class="card-header blue darken-4 text-white" align="center"> Editar Aprobación </div>
         <div class="card-body">
          <form action="{{ route('actualizar.aprobacion', $apro->aptra_id) }}" method="post" role="form" >            
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            
           
            @include('formularios.riesgo.tratamientos.aprobacion.formulario')

           
           </form>

       


           </div>
          </div>
        </div>
       </div>
      </div>
     </div>



