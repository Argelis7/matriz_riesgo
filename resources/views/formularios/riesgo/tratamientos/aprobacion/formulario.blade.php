              <div class="row">                     
              <div class="col-md-12">
                <div class="form-group">
                  <label>Nombre del tratamiento</label>
                  <input required name="aptra_nombretra" class="form-control" readonly value="{{ old('aptra_nombretra', $apro->aptra_nombretra ?? '') }}">
                </div>
              </div>

              <div class="col-md-5">        
                <div class="form-group">
                  <label>Responsable de la aprobación</label>
                  <input required name="aptra_responsableapro" class="form-control" value="{{ old('aptra_responsableapro', $apro->aptra_responsableapro ?? '') }}">              
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Fecha de respuesta</label>
                  <input required type="date" name="aptra_fecharespuesta" class="form-control" value="{{ old('aptra_fecharespuesta', $apro->aptra_fecharespuesta ?? '') }}">                                     
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>Estado de respuesta</label>
                  <select required name="aptra_ert" class="form-control" id="est_res">
                  <option  value="0" disabled selected >{{ old('aptra_ert', $apro->estadoRiesgo->estrsg_descripcion ?? 'Seleccionar') }}</option>
                  <option  value="1">Abierto</option>
                  <option  value="2">Cerrado</option> 
                  </select>                               
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label>Observaciones</label>
                   <textarea name="aptra_observaciones" class="form-control" rows="5" id="comment">{{ old('aptra_observaciones', $apro->aptra_observaciones ?? '') }}</textarea>            
                </div>
              </div>                                                                                                   
            
             </div>


              {{-- input del tratamiento para asociarlo a la aprobacion --}}
              <input type="hidden" name="aptra_rtraid" value="{{ old('aptra_rtraid', $apro->aptra_rtraid ?? '') }}">
              {{-- input del riesgo --}}
              <input type="hidden" name="aptra_rsgid" value="{{ old('aptra_rsgid', $apro->aptra_rsgid ?? '') }}">

             {{-- boton de guardar y cancelar --}}
             <div class="row justify-content-center">

              <button type="button" class="btn btn-danger btn-sm "  aria-expanded="false" data-dismiss="modal" id="cancelar_apro">
                Cancelar
              </button> &nbsp;

              <input type="submit" class="btn btn-success btn-sm" id="g_aprobacion" aria-expanded="false" value="Guardar">

              <input  type="hidden" name="tipo_riesgo" value="Aprobacion">

             </div>
