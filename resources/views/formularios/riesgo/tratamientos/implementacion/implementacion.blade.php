<style type="text/css">
  .seleccionada{
    background: #3da6f375;
    color:white;
  }
</style>
<!-- Editable table -->
      <div id="content" > 

         

  

          <table id="implementacion_table" class="table table-striped table-bordered table-responsive-lg" width="100%">
            <thead>
              <tr>

                <th>Responsable de la implementación
                </th>      
                <th class="th-sm">Fecha de seguimiento
                </th>
                <th class="th-sm">Fecha objetivo
                </th>
                <th class="th-sm">Fecha de culminación
                </th>
                <th class="th-sm avance">% Avance
                </th>
                <th class="th-sm">Observaciones
                </th>                  
                <th class="th-sm">Acciones</th>

              </tr>
            </thead>                   
                     {{-- <tbody> --}}
                       @foreach ($implementacion as $imple)
                        <tr>
                          <td>{{ $imple->imt_responsable }}</td>
                          <td>{{ $imple->imt_fechaseguimiento }}</td>
                          <td>{{ $imple->imt_fechaobje }}</td>
                          <td>{{ $imple->imt_culminacion }}</td>
                          <td class="avance">{{ $imple->imt_porcavance }}</td>
                          <td>{{ $imple->imt_observaciones }}</td>                    
                         
                          <td class="justify-content-center imple">
                           <form action="{{ route('riesgo.eliminar', $imple->imt_id) }}" method="post">
                             {{ csrf_field() }}
                             {{ method_field('DELETE') }}
                             <input type="hidden" name="tipo_riesgo" value="eliminarImplementacion">
                            <div class="row"> 
                               <button 
                                type="submit"
                                class="btn btn-danger btn-sm px-2 bord-rad borrar_fila"
                                value='{{-- {{ $riesgoControlado->rcon_id }} --}}' 
                                name="eliminarAprobacion">

                                 <i class="fa fa-trash icon-bas" ></i>

                               </button>                           
                               

                               <button  
                                  data-target="#ventana7"
                                  title="Editar Implementación" 
                                  type="button"                        
                                  class="btn blue darken-4 btn-sm bord-rad px-2 implementacion_editar" 
                                  aria-expanded="false" 
                                  data-toggle="modal"
                                  value="{{ $imple->imt_id }}"                                  
                                  >
                                      
                                  <i class="fa fa-edit icon-bas icono-plus"></i>
                               </button>
                             </div>                           

                           </form>
                         </td>
                        </tr>

                          <div class="modal fade" id="ventana7" role="dialog" data-backdrop="static">

                            @include('formularios.riesgo.tratamientos.implementacion.modal_implementacion_editar')

                          </div>
                      @endforeach                 
                    {{-- </tbody> --}}

                   
                </table>      

                <p class="oculto-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
       
              
              {{-- tabla emergente --}}
              <div class="modal fade" id="ventana5" role="dialog" data-backdrop="static">

                  @include('formularios.riesgo.tratamientos.implementacion.modal_implementacion')

              </div>
      
               
              
        </div>

        <script type="text/javascript">
          $(document).ready(function(){

             

            $('td.avance').each(function(index){
            let avance = $(this).text();   
            let padre =  $(this).parent().children('td.imple').children('form').children('div.row').children('button.implementacion_editar');

              if (avance == 100) {
               padre.css('display','none');                
              }
           
          })

})
        </script>


   