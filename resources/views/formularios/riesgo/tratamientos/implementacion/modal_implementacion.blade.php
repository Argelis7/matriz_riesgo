{{--ventana emergente --}}

<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
     <br>
                    
{{-- consultas controles del riesgo  --}}

<div class="container-fluid">
     <div class="card">
       <div class="card-header blue darken-4 text-white" align="center"> Nueva Implementacion </div>
         <div class="card-body">
          <form action="{{ route('registrarRiesgo') }}" method="post">            
            {{ csrf_field() }}
                      
            @include('formularios.riesgo.tratamientos.implementacion.formulario')   
            

           </form>

      


           </div>
          </div>
        </div>
       </div>
      </div>
     </div>


     <script type="text/javascript">
       $(document).ready(function(){
        $('.implementacion').click(function(){
          let id_aptra = $(this).val();
          $('input[name="imt_aptraid"]').val(id_aptra);
        })

        if (localStorage.getItem("idInherenteEditar")) {
          $('input[name="imt_rsgid"]').val(localStorage.getItem("idInherenteEditar"));
        }
       })
     </script>


