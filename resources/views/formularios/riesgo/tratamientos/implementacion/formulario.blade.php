            <div class="row">
                     
              <div class="col-md-8">
                <div class="form-group">
                  <label>Responsable de la implementacion</label>
                  <input name="imt_responsable" required class="form-control" value="{{ old('imt_responsable', $imple->imt_responsable ?? '') }}">
                </div>
              </div>

              <div class="col-md-4">        
                <div class="form-group">
                  <label>Fecha de seguimiento</label>
                  <input name="imt_fechaseguimiento" required type="date" class="form-control" value="{{ old('imt_fechaseguimiento', $imple->imt_fechaseguimiento ?? '') }}">              
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Fecha objetivo</label>
                  <input name="imt_fechaobje" required type="date" class="form-control" value="{{ old('imt_fechaobje', $imple->imt_fechaobje ?? '') }}"> 
                                    
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Fecha de culminación</label>
                  <input name="imt_culminacion" required type="date" class="form-control" value="{{ old('imt_culminacion', $imple->imt_culminacion ?? '') }}">                                
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>% Avance</label>
                  <input name="imt_porcavance" required type="number"  class="form-control" value="{{ old('imt_porcavance', $imple->imt_porcavance ?? '') }}">                                         
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label>Observaciones</label>
                   <textarea name="imt_observaciones" class="form-control" rows="5" id="comment">
                     {{ old('imt_observaciones', $imple->imt_observaciones ?? '') }}
                   </textarea>            
                </div>
              </div>                                                                                                 
            
             </div>

             <input type="hidden" name="imt_aptraid" value="{{ old('imt_aptraid', $imple->imt_aptraid ?? '') }}">
             <input type="hidden" name="imt_rsgid" value="{{ old('imt_rsgid', $imple->imt_rsgid ?? '') }}">
             <input type="hidden" name="tipo_riesgo" value="Implementacion">

              {{-- boton de guardar y cancelar --}}
       <div class="row justify-content-center">

        <button type="button" class="btn btn-danger btn-sm pull-left "  aria-expanded="false" data-dismiss="modal" >
          Cancelar
        </button> &nbsp;

        <input type="submit" class="btn btn-success btn-sm pull-right " aria-expanded="false" value="Guardar">

       </div>