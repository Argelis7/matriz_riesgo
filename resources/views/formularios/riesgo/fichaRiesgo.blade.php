
<div class="tab-pane active" id="home2">
  <fieldset id="opcion_riesgo">

        @if ( isset($estado) )
          
     {{--      <p>registra riesgo</p> --}}
          <form action="{{ route('registrarRiesgo') }}" method="post" role="form1" id="form_riesgo">
          <script src="{{ asset('js/js/select.js') }}"></script>
          {{ csrf_field() }}

        @else
{{--  --}}
          {{-- <p>edita riesgo</p> --}}
          <form action="{{ route('actualizarRiesgo', $riesgo->rsg_id) }}" method="post">
          {{ csrf_field() }}
          {{ method_field('PUT') }} 
          <script src="{{ asset('js/js/selectEdita.js') }}"></script>
        @endif
  
      
    <div id="mensaje"></div>
   
           <div class="card borde">
              <div class="card-header grey darken-3 barra">  Identificación del riesgo </div>
                <div class="card-body">
                  <div class="row">

                    <div class="col-md-12">
                      <div class="form-group">
                          <label>Nombre del riesgo</label>
                          <input {{-- required --}} name="rsg_nombre" id="rsg_nombre" class="form-control" placeholder="Introduzca nombre del riesgo" value="{{ old('rsg_nombre', $riesgo->rsg_nombre ?? '') }}">
                      </div>                               
                    </div>
                          
                   <div class="col-md-6">
                    <div class="form-group">
                      <label>Clasificación del riesgo</label>
                      <select {{-- required --}} name="rsg_clasf" class="form-control" >
                          <option disabled selected value="0">
                            {{ old('rsg_clasf', $riesgo->clasificacionRiesgo->clasfrsg_descripcion ?? 'Seleccionar') }}
                          </option>
                          @foreach ($clasificacionRiesgo as $clasificacion)
                            <option value="{{ $clasificacion->clasfrsg_id }}">
                              {{ $clasificacion->clasfrsg_descripcion }}
                            </option>
                          @endforeach
                       </select>
                     </div>            
                   </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Estado del riesgo</label>
                        <select {{-- required --}} name="rsg_estrsg" class="form-control" >
                          <option disabled selected value="0">
                            {{ old('rsg_estrsg', $riesgo->estadoRiesgo->estrsg_descripcion ?? 'Seleccionar') }}
                          </option>
                          @foreach ($estadoRiesgo as $estado)
                            <option value="{{ $estado->estrsg_id }}">{{ $estado->estrsg_descripcion }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>

                   <div class="col-md-6">
                      <div class="form-group">
                        <label>Responsable</label>
                        <input {{-- required --}} name="rsg_responsable" class="form-control" placeholder="GOTIC" value="{{ old('rsg_responsable', $riesgo->rsg_responsable ?? '') }}">
                      </div>
                   </div>

                   <div class="col-md-6">
                      <div class="form-group">
                        <label>Unidad responsable</label>
                        <input {{-- required --}} name="rsg_undresp" class="form-control" placeholder="Energia" value="{{ old('rsg_undresp', $riesgo->rsg_undresp ?? '') }}">
                      </div>
                   </div>

                   <div class="col-md-12">
                      <div class="form-group">
                      <label>Descripción del riesgo</label>
                      <textarea {{-- required --}} name="rsg_descripcion" class="form-control" rows="3" >
                        {{ old('rsg_descripcion', $riesgo->rsg_descripcion ?? '') }}
                      </textarea>
                      </div>
                   </div>

                   <div class="col-md-4">
                     <div class="form-group">
                       <label>Area de impacto</label>
                       <select {{-- required --}} name="rsg_aimpact" class="form-control" id="rsg_aimpact">
                          <option disabled selected>
                          {{ old('rsg_aimpact', $riesgo->areaImpacto->aimpac_descripcion ?? 'Seleccionar') }}
                          </option>
                            @foreach ($areaImpacto as $area)
                              <option value="{{ $area->aimpac_id }}">{{ $area->aimpac_descripcion }}</option>
                            @endforeach
                       </select>
                      </div>
                                                                                      
                     <div class="form-group">
                       <label>Fuente de impacto</label>
                       <select {{-- required --}} name="rsg_fuente_riesgo" class="form-control" id="rsg_fuente_riesgo">
                          <option disabled selected value="0">
                          {{ old('rsg_fuente_riesgo', $riesgo->fuenteImpacto->frsg_descripcion ?? 'Seleccionar') }}
                          </option>
                            @foreach ($fuenteImpacto as $fuente)
                              <option value="{{ $fuente->frsg_id }}">{{ $fuente->frsg_descripcion }}</option>
                            @endforeach
                       </select>
                     </div>


                     <div class="form-group">
                        <label>Otra fuente de impacto</label>
                        <input name="rsg_otrofuente" id="rsg_otrofuente" type="text" name="" class="form-control" placeholder="Ingrese otra fuente de impacto" disabled value="{{ old('rsg_otrofuente', $riesgo->rsg_otrofuente ?? '') }}">
                     </div>

                   </div>
                           
                   <div class="col-md-4">

                      <div class="form-group">
                        <label>Area de impacto nivel 1</label>
                        <select {{-- required --}} name="rsg_aimpactn1" class="form-control" id="rsg_aimpactn1">
                          <option disabled selected value="0">
                            {{ old('rsg_aimpact', $riesgo->areaImpactoN1->aimpac_descripcion ?? 'Seleccionar') }}
                          </option>                            
                        </select>
                      </div>
                                                                                      
                       <div class="form-group">
                          <label>Fuente de impacto nivel 1</label>
                          <select {{-- required --}} name="rsg_friesgon1" class="form-control" id="rsg_friesgon1">
                            <option disabled selected value="0">
                            {{ old('rsg_friesgon1', $riesgo->fuenteImpactoN1->frn1_descripcion ?? 'Seleccionar') }}
                            </option>                              
                          </select>
                       </div>

                   </div>

                   <div class="col-md-4">

                      <div class="form-group">
                         <label>Otra área de impacto</label>
                         <input name="rsg_otroarea" id="rsg_otroarea" type="text" class="form-control" placeholder="Ingrese otra área de impacto" disabled value="{{ old('rsg_otroarea', $riesgo->rsg_otroarea ?? '') }}">
                      </div>

                      <div class="form-group">
                         <label>Fuente de impacto nivel 2</label>
                         <select {{-- required --}} name="rsg_friesgon2" class="form-control" id="rsg_friesgon2">
                            <option disabled selected value="0">
                              {{ old('rsg_friesgon2', $riesgo->fuenteImpactoN2->frn2_descripcion ?? 'Seleccionar') }}
                            </option>                           
                         </select>
                      </div>

                   </div>
                  </div>
                </div>
              </div><br>

        <!-- Riesgo inherente -->


       <div class="card borde">
        <div class="card-header grey darken-3 barra"> Riesgo inherente </div>
           <div class="card-body">
              <div class="row">

                <div class="col-md-3">
                   <div class="form-group">
                     <label>Consecuencia</label>
                     <select {{-- required --}} name="rsg_ricons" class="form-control" id="rsg_ricons" onchange="severidad();">
                        <option disabled selected value="0">
                          {{ old('rsg_ricons', $riesgo->consecuenciaRiesgo->cons_descripcion ?? 'Seleccionar') }}
                        </option>
                        @foreach ($consecuencia as $consec)
                          <option value="{{ $consec->cons_id }}">{{ $consec->cons_descripcion }}</option>
                        @endforeach
                     </select>
                   </div>
                </div>
                  
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Probabilidad</label>
                    <select {{-- required --}} name="rsg_riprob" class="form-control" id="rsg_riprob" onchange="severidad();">
                      <option disabled selected value="0">
                      {{ old('rsg_riprob', $riesgo->probabilidadRiesgo->proba_descripcion ?? 'Seleccionar') }}
                      </option>
                      @foreach ($probabilidad as $proba)
                        <option value="{{ $proba->proba_id }}">{{ $proba->proba_descripcion }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Severidad</label>
                    <input {{-- required --}} name="rsg_severidad" class="form-control" id="rsg_severidad" value="{{ old('rsg_severidad', $riesgo->rsg_severidad ?? '') }}" readonly>
                  </div>
                </div>
                      
                <div class="col-md-3">
                            
                  <div class="form-group">
                    <label>Aceptación</label>
                      <select required class="form-control" onchange="aceptacion();" name="elegir" id= "elegir">
                        <option disabled selected>{{ old('elegir', $riesgo->elegir ?? 'Elegir') }}</option>
                        <option value="1">Si</option>
                        <option value="0">No</option>
                      </select> 
                  </div>
                </div>

              </div>
            </div>

            <div class="btn-group justify-content-center" role="group" aria-label="Basic example">
                
               <a href=" {{ url('revision') }}">
                <button 
                  type="button" 
                  class="btn btn-danger btn-sm"  
                  aria-expanded="false" 
                  id="cancelarRiesgo">
                  Cancelar
                </button>&nbsp;
               </a>

               <a id="ir">
                <input 
                  type="submit" 
                  disabled 
                  class="btn btn-success btn-sm" 
                  aria-expanded="false" 
                  id="aceptar" 
                  value="Guardar" 
                  name="submitInherente">
               </a>

               {{-- input que indica el tipo de riesgo a guardar ya que esto se envia con AJAX --}}
               <input type="hidden" name="tipo_riesgo" value="RiesgoInherente">

            </div>
        <br>
     </div>

  
      {{-- input para observar el id de la revision --}}
      <input type="hidden" name="rsg_revid">
 

    </form>
</fieldset>



<script type="text/javascript">

$(function(){
 // if (localStorage.getItem('si') || localStorage.getItem('ver_gestion')) {
 if (localStorage.getItem('idInherenteEditar')) { 
  $("#opcion_riesgo,#b_controlado,#b_tratado,#acp_s,#acp_n,#b_aprobacion").prop('disabled', 'true');
 }

 if (localStorage.getItem('id_revision_editar')) {

  $('input[name="rsg_revid"]').val(localStorage.getItem('id_revision_editar'));  
 }else{
  $('input[name="rsg_revid"]').val(localStorage.getItem('id_revision'));

 } 
 
})
        
</script>





