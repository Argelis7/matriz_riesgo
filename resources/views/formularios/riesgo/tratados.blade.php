{{--ventana emergente --}}

<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
     <br>
                    
{{-- consultas controles del riesgo  --}}

<div class="container-fluid">
    <div class="card">
       <div class="card-header blue darken-4 text-white" align="center"> Nuevo Tratamiento</div>
         <div class="card-body">
            <form action="{{ route('registrarRiesgo') }}" role="form" method="post" id="form_tra">
            {{ csrf_field() }}
                                               
                         <div class="row">
                       
                            <div class="col-md-6"> 

                                 <div class="form-group"  >
                                     <label>Tratamientos Propuestos</label>
                                     <input required name="rtra_nombre" class="form-control" placeholder="Introducir texto" >
                                 </div>
                               
                            </div>

                            <div class="col-md-3"> 

                                 <div class="form-group"  >
                                     <label>Responsable</label>
                                     <input required name="rtra_responsable" class="form-control" placeholder="Introducir texto" >
                                 </div>
                               
                            </div>

                             <div class="col-md-3"> 

                                 <div class="form-group"  >
                                     <label>Unidad</label>
                                     <input required name="rtra_unidad" class="form-control" placeholder="Introducir texto" >
                                 </div>
                               
                            </div>

                         </div>

                   <div class="row">

                         <div class="col-md-3"> 

                                 <div class="form-group"  >
                                     <label>Tipo</label>
                                     <select required id="rtra_tipotra" name="rtra_tipotra" class="form-control">
                                        <option value="" disabled selected>Seleccionar</option>
                                        @foreach ($tipoTratamiento as $tipo)
                                            <option value="{{ $tipo->tcr_id }}">{{ $tipo->tcr_descripcion }}</option>
                                        @endforeach
                                     </select>
                                 </div>
                               
                            </div>

                             <div class="col-md-3"> 

                                 <div class="form-group"  >
                                     <label>Complejidad</label>
                                     <select required id="rtra_comptra" onchange="complejidad();" name="rtra_comptra" class="form-control">
                                        <option value="" disabled selected>Seleccionar</option>
                                        @foreach ($complejidadTratado as $complejidad)
                                            <option value="{{ $complejidad->ctr_id }}">{{ $complejidad->ctr_descripcion }}</option>
                                        @endforeach
                                     </select>

 {{-- este select oculto muestra el valor de la complejidad para calcular la funcion efectividad --}}
                            <select style="visibility:hidden" name="ocCom" id="ocCom" 
                                    " class="form-control">
                              <option value="" disabled selected >Seleccionar</option>
                                     @foreach ($complejidadTratado as $complejidad)
                                            <option value="{{ $complejidad->ctr_id }}">{{ $complejidad->ctr_valor }}</option>
                                        @endforeach
                            </select>
{{-- /////////////////////////////////////////////////////////////////////////////////////////////////////// --}}



                                 </div>
                               
                            </div>

                            <div class="col-md-3"> 

                                 <div class="form-group"  >
                                     <label>Costo/Beneficio</label>
                                     <select required id="rtra_costobentra" onchange="costoBeneficio();" name="rtra_costobentra" class="form-control">
                                        <option value="" disabled selected>Seleccionar</option>
                                        @foreach ($costobeneficioTratado as $costobeneficio)
                                            <option value="{{ $costobeneficio->cbtr_id }}">{{ $costobeneficio->cbtr_descripcion }}</option>
                                        @endforeach
                                     </select>

 {{-- este select oculto muestra el valor de la costoBeneficio para calcular la funcion efectividad --}}
                            <select style="visibility:hidden" name="ocCB" id="ocCB" 
                                    " class="form-control">
                              <option value="" disabled selected >Seleccionar</option>
                                     @foreach ($costobeneficioTratado as $costobeneficio)
                                            <option value="{{ $costobeneficio->cbtr_id }}">{{ $costobeneficio->cbtr_valor }}</option>
                                        @endforeach
                            </select>
{{-- /////////////////////////////////////////////////////////////////////////////////////////////////////// --}}                                     
                                 </div>
                               
                            </div>

                             <div class="col-md-3"> 

                                 <div class="form-group"  >
                                     <label>Tiempo</label>
                                     <select required id="rtra_tiempotra" onchange="tiempo();" name="rtra_tiempotra" class="form-control">
                                        <option value="" disabled selected>Seleccionar</option>
                                        @foreach ($tiempoTratado as $tiempo)
                                            <option value="{{ $tiempo->ttr_id }}">{{ $tiempo->ttr_descripcion }}</option>
                                        @endforeach
                                     </select>

 {{-- este select oculto muestra el valor de la tiempo para calcular la funcion efectividad --}}
                            <select style="visibility:hidden" name="ocTiempo" id="ocTiempo" 
                                    " class="form-control">
                              <option value="" disabled selected >Seleccionar</option>
                                    @foreach ($tiempoTratado as $tiempo)
                                            <option value="{{ $tiempo->ttr_id }}">{{ $tiempo->ttr_valor }}</option>
                                        @endforeach
                            </select>
{{-- /////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                                 </div>
                               
                            </div>
                        
                  </div>

                    <div class="row justify-content-center">
                       <div class="col-md-3"> 
                            <div class="form-group "  >

                                <center><label>Efectividad</label></center>
                                <input class="form-control" id="efecTratado" name="rtra_efectividad" readonly>
                            </div>
                        </div>
                    </div>

{{-- input contenedor de la suma total oculto --}}
 <input type="hidden" id="sumaTratado" name="sumaTratado">
 {{-- input para asociar la id de inherente con el controlado --}}
 <input type="hidden" name="rtra_rsgid">
 {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
 
                    <div class="row justify-content-center">
                        <div class="col-md-9 col-lg-5 ml-5">

                            <a href=" {{ url('registrariesgo') }}">
                            <button type="button" class="btn btn-danger btn-sm pull-left "  aria-expanded="false"data-dismiss="modal" id="cancelar_tra">Cancelar</button></a>
                            
                            <input type="submit" name="submitTratado" class="btn btn-success btn-sm pull-right " align="" aria-expanded="false" value="Guardar">

                            <input  type="hidden" name="tipo_riesgo" value="RiesgoTratado">

                        </div>
                    </div>

 </form>                     
                      </div>
            
</div>

</div>



{{-- contenido de la ventana --}}
     </div>
    </div>
   </div>

