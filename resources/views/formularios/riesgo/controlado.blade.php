{{--ventana emergente --}}

<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
     <br>
                    
{{-- consultas controles del riesgo  --}}




{{-- consultas controles del reiesgo  --}}

<div class="container-fluid">
     <div class="card" >
       <div class="card-header blue darken-4 text-white" align="center" > Nuevo Control </div>
         <div class="card-body">
           <form action="{{ route('registrarRiesgo') }}" method="post" role="form" id="form_con">
            {{ csrf_field() }}
           {{-- <div class="card borde"> --}}


                {{-- <div class="card-header blue darken-4 barra"> Nuevo Control </div> --}}
                     {{-- <div class="card-body"> --}}
                         
                         <div class="row">
                      
                            <div class="col-sm-12 col-md-12 col-lg-6"> 

                                 <div class="form-group"  >
                                     <label>Control</label>
                                     <input required name="rcon_nombre" class="form-control" placeholder="Introducir texto" >
                                     {{ $errors->first('rcon_nombre') }}
                                 </div>
                               
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-3"> 

                                 <div class="form-group"  >
                                     <label>Responsable</label>
                                     <input required name="rcon_responsable" class="form-control" placeholder="Introducir texto" >
                                 </div>
                               
                            </div>

                             <div class="col-sm-12 col-md-12 col-lg-3"> 

                                 <div class="form-group"  >
                                     <label>Unidad</label>
                                     <input required name="rcon_unidad" class="form-control" placeholder="Introducir texto" >
                                 </div>
                               
                            </div>

                         </div>

                    <div class="row">

                            <div class="col-sm-12 col-md-12 col-lg-3"> 

                                 <div class="form-group"  >
                                     <label>Tipo</label>
                                     <select required name="rcon_tipocontrol" class="form-control">
                                        <option value="" disabled selected >Seleccionar</option>
                                        @foreach ($tipoTratamiento as $tipo)
                                            <option value="{{ $tipo->tcr_id }}">{{ $tipo->tcr_descripcion }}</option>
                                        @endforeach
                                     </select>
                                 </div>
                               
                            </div>

                             <div class="col-sm-12 col-md-12 col-lg-3"> 

                                 <div class="form-group"  >
                                     <label>Implementación</label>
                                     <select required name="rcon_implementacion" id="rcon_implementacion" onchange="implementacion();" class="form-control">
                                        <option value="" disabled selected >Seleccionar</option>
                                        @foreach ($implementacionControlado as $implementacion)
                                            <option value="{{ $implementacion->icr_id }}">{{ $implementacion->icr_descripcion }} </option>
                                        @endforeach

                                     </select>

                                            {{-- este select oculto muestra el valor de la implementacion para calcular la funcion efectividad --}}
                                            <select style="visibility:hidden" name="ocImp" id="ocImp" 
                                                    " class="form-control">
                                              <option value="" disabled selected >Seleccionar</option>
                                                      @foreach ($implementacionControlado as $implementacion)
                                                      <option  value="{{ $implementacion->icr_id }}">{{ $implementacion->icr_valor }} </option>
                                                      @endforeach
                                            </select>
                                           {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////// --}}

                                 </div>                               
                             </div>

                            <div class="col-sm-12 col-md-12 col-lg-3"> 

                                 <div class="form-group"  >
                                     <label>Documentación</label>
                                     <select required id="rcon_documentacion" name="rcon_documentacion" onchange="documentacion();"  class="form-control">
                                        <option value="" disabled selected >Seleccionar</option>
                                        @foreach ($documentacionControlado as $documentacion)
                                            <option value="{{ $documentacion->dcr_id }}">{{ $documentacion->dcr_descripcion }}</option>
                                        @endforeach
                                     </select>

                                    {{-- este select oculto muestra el valor de la Documentación para calcular la funcion efectividad --}}
                                    <select style="visibility:hidden" name="ocDocu" id="ocDocu" 
                                            " class="form-control">
                                      <option value="" disabled selected >Seleccionar</option>
                                            @foreach ($documentacionControlado as $documentacion)
                                                    <option value="{{ $documentacion->dcr_id }}">{{ $documentacion->dcr_valor }}</option>
                                                @endforeach
                                    </select>
                                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////// --}}

                                 </div>
                               
                            </div>

                             <div class="col-sm-12 col-md-12 col-lg-3"> 

                                 <div class="form-group"  >
                                     <label>Ejecución</label>
                                     <select required name="rcon_ejecucion" id="rcon_ejecucion" onchange="ejecucion();" class="form-control">
                                        <option value="" disabled selected >Seleccionar</option>
                                        @foreach ($ejecucionControlado as $ejecucion)
                                            <option value="{{ $ejecucion->ecr_id }}">{{ $ejecucion->ecr_descripcion }}</option>
                                        @endforeach
                                     </select>

                                        {{-- este select oculto muestra el valor de la Documentación para calcular la funcion efectividad --}}
                                        <select style="visibility:hidden" name="ocEjec" id="ocEjec" 
                                                " class="form-control">
                                          <option value="" disabled selected >Seleccionar</option>
                                                  @foreach ($ejecucionControlado as $ejecucion)
                                                        <option value="{{ $ejecucion->ecr_id }}">{{ $ejecucion->ecr_valor }}</option>
                                                    @endforeach
                                        </select>
                                        {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                                 </div>
                               
                             </div>
                        
                    </div>
                       

                          <div class="row justify-content-center">
                            
                            <div class="col-sm-12 col-md-12 col-lg-3"> 

                                <div class="form-group"  >
                                     <center><label>Efectividad</label></center>
                                     <input class="form-control" id="efectividad"  name="rcon_efectividad" readonly>
                                </div>
                               {{-- input contenedor de la suma total oculto --}}
                                 <input type="hidden" id="sumaControl" name="rcon_suma">
                                 {{-- input para asociar la id de inherente con el controlado --}}
                                 <input type="hidden" name="rcon_rsgid">

                                {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                            </div>
                          </div>

                    <div class="row justify-content-center">

                        <div class="col-md-9 col-lg-5 ml-5 ">

                            
                            <button type="button" class="btn btn-danger btn-sm pull-left "  aria-expanded="false" data-dismiss="modal" id="cancelar_con">Cancelar</button>

                            <input type="submit" name="submitControlado" class="btn btn-success btn-sm pull-right " align="" aria-expanded="false" id="guardar_con" value="Guardar">

                                     <input  type="hidden" name="tipo_riesgo" value="RiesgoControlado">
                                     
                         
                        </div>
                    </div>

            </form>
        </div>
    </div>
</div>




       
{{-- contenido de la ventana --}}
     </div>
    </div>
   </div>

