@extends('layouts.app')
 @include('extenciones.libreriasMDB')
@section('content')


<link rel="stylesheet" href="{{ asset('css/estilos.css') }}">
      

<body>

  
    
<div class="container">
    <div class="row  pt-lg-5 mt-lg-5 justify-content-center">
      <div class="col-md-6 col-xl-5 mb-4 form-group">       
          <div class="card wow fadeInRight" data-wow-delay="0.3s"> <!--son las que dan mvimiento-->
            <div class="card-body z-depth-2">
              <!--formulario-->
              
                <div class="text-center">
                    <h3 class="dark-grey-text">
                    <strong>Matriz de Riesgo</strong>
                    </h3>
                    <hr>
                </div>

                

                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="md-form">
                              <i class="fa fa-user prefix grey-text"></i>
                             <input
                                        id="user_red"
                                        type="text"
                                        class="form-control{{ $errors->has('user_red') ? ' is-invalid' : '' }}"
                                        name="user_red"
                                        value="{{ old('user_red') }}"
                                        required
                                        {{-- autofocus --}}
                                >
                                @if ($errors->has('user_red'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('user_red') }}</strong>
                                    </div>
                                @endif

                              <label for="form3">Usuario</label>
                            </div>

                            <div class="md-form">
                              <i class="fa fa-lock prefix grey-text"></i>
                             <input
                                        id="password"
                                        type="password"
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                        name="password"
                                        required
                                        {{-- autofocus --}}
                                >
                                @if ($errors->has('user_red'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('user_red') }}</strong>
                                    </div>
                                @endif

                              <label for="form3">Contraseña</label>
                            </div>
                        </div>


                                    {{-- password oculto             --}}
                                                                                   
                          
                        <div class="form-group">
                              <div class="text-center mt-3">
                                 <button  type="submit " class="btn red darken-4 btn-sm" style="border-radius: 20px !important;">Ingresar</button>                  
                                 <hr>
                              </div>
                        </div>

                         
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>       
    new WOW().init();
    </script>
</body>
<!--Footer-->
<footer class="font-small pt-0 footer-login">
    <div class="posicion-f">

    <div class="footer-copyright py-3 text-center">
        <div class="container-fluid">
          <div class="row">
            <div class="col-8 col-sm-8 col-md-11 col-lg-11 color-f">

                 <p>Sistema desarrollado por la Coordinación de Ingeniería de Seguridad, perteneciente a la Gerencia de Seguridad de la Operación y Servicios de Cantv 2018 </p>
            </div>
            
             <div class="col-4 col-sm-4 col-md-1  col-lg-1">
                <img src="{{ asset('imagenes/logo-blanco.png') }}" class="img-fluid" width="100%" height="0" align="right">
            </div>
          </div>
        </div>
       
    </div>
    <!--/Copyright-->
  </div>

</footer>
<!--/Footer-->

@endsection


