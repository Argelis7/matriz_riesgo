@extends('consultas.menurevision')
@section('consulta')

<div class="container-fluid">
        <div class="row justify-content-center">
          <div class="panel panel-danger col col-md-12 col-lg-12 ">
            <div class="panel-heading"> 
              Gestión del Riesgo 
            </div>

            <div class="panel-body">
          
          {{-- Notifica cuando un riesgo se ha registrado exitosamente --}}
         {{--  @if (session()->has('mensaje'))
              <div class="alert alert-success">
                {{ session('mensaje') }}
              </div>
          @endif --}}
          

      <div class="tab-content">
        <div class="tab-pane fade in active" id="home2">
          <form action="{{-- {{ route('registrarRiesgo') }} --}}" method="post" role="form">{{ csrf_field() }}
            <div class="panel borde">
              <div class="panel-heading barra">  Identificacón del riesgo </div>
                <div class="panel-body">
                  <div class="row">

                    <div class="col-md-12">
                      <div class="form-group">
                          <label>Nombre del riesgo</label>
                          <input name="rsg_nombre" class="form-control" placeholder="Introduzca nombre del riesgo" >
                      </div>                               
                    </div>
                          
                   <div class="col-md-6">
                    <div class="form-group">
                      <label>Clasificación del riesgo</label>
                      <select name="rsg_clasf" class="form-control" >
                          <option disabled selected>Pick one</option>
                         {{--  @foreach ($clasificacionRiesgo as $clasificacion)
                            <option value="{{ $clasificacion->clasfrsg_id }}">{{ $clasificacion->clasfrsg_descripcion }}</option>
                          @endforeach --}}
                       </select>
                    </div>            
                   </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Estado del riesgo</label>
                        <select name="rsg_estrsg" class="form-control" >
                          <option disabled selected>Pick one</option>
                          {{-- @foreach ($estadoRiesgo as $estado)
                            <option value="{{ $estado->estrsg_id }}">{{ $estado->estrsg_descripcion }}</option>
                          @endforeach --}}
                        </select>
                      </div>
                    </div>

                   <div class="col-md-6">
                      <div class="form-group">
                        <label>Responsable</label>
                        <input name="rsg_responsable" class="form-control" placeholder="GOTIC" >
                      </div>
                   </div>

                   <div class="col-md-6">
                      <div class="form-group">
                        <label>Unidad responsable</label>
                        <input name="rsg_undresp" class="form-control" placeholder="Energia" >
                      </div>
                   </div>

                   <div class="col-md-12">
                      <div class="form-group">
                      <label>Descripción del riesgo</label>
                      <textarea name="rsg_descripcion" class="form-control" rows="3" ></textarea>
                      </div>
                   </div>

                   <div class="col-md-4">

                     <div class="form-group">
                       <label>Area de impacto</label>
                       <select name="rsg_aimpact" class="form-control" >
                          <option disabled selected>Pick one</option>
                            {{-- @foreach ($areaImpacto as $area)
                              <option value="{{ $area->aimpac_id }}">{{ $area->aimpac_descripcion }}</option>
                            @endforeach --}}
                       </select>
                     </div>
                                                                                      
                     <div class="form-group">
                       <label>Fuente de impacto</label>
                       <select name="rsg_fuente_riesgo" class="form-control" >
                          <option disabled selected>Pick one</option>
                            {{-- @foreach ($fuenteImpacto as $fuente)
                              <option value="{{ $fuente->frsg_id }}">{{ $fuente->frsg_descripcion }}</option>
                            @endforeach --}}
                       </select>
                     </div>

                     <div class="form-group">
                        <label>Otra fuente de impacto</label>
                        <input name="rsg_otrofuente" type="text" name="" class="form-control" placeholder="Ingrese otra fuente de impacto" disabled>
                     </div>

                   </div>
                           
                   <div class="col-md-4">

                      <div class="form-group">
                        <label>Area de impacto nivel 1</label>
                        <select name="rsg_aimpactn1" class="form-control" >
                          <option disabled selected>Pick one</option>
                           {{--  @foreach ($areaImpactoN1 as $areaN1)
                              <option value="{{ $areaN1->aimpac_id }}">{{ $areaN1->aimpac_descripcion }}</option>
                            @endforeach --}}
                        </select>
                      </div>
                                                                                      
                       <div class="form-group">
                          <label>Fuente de impacto nivel 1</label>
                          <select name="rsg_friesgon1" class="form-control" >
                            <option disabled selected>Pick one</option>
                             {{--  @foreach ($fuenteImpactoN1 as $fuenteN1)
                                <option value="{{ $fuenteN1->frn1_id }}">{{ $fuenteN1->frn1_descripcion }}</option>
                              @endforeach --}}
                          </select>
                       </div>

                   </div>

                   <div class="col-md-4">

                      <div class="form-group">
                         <label>Otra área de impacto</label>
                         <input name="rsg_otroarea" type="text" class="form-control" placeholder="Ingrese otra área de impacto" disabled>
                      </div>

                      <div class="form-group">
                         <label>Fuente de impacto nivel 2</label>
                         <select name="rsg_friesgon2" class="form-control" >
                            <option disabled selected>Pick one</option>
                            {{-- @foreach ($fuenteImpactoN2 as $fuenteN2)
                              <option value="{{ $fuenteN2->frn2_id }}">{{ $fuenteN2->frn2_descripcion }}</option>
                            @endforeach --}}
                         </select>
                      </div>

                   </div>
                  </div>
                </div>
              </div>

        <!-- Riesgo inherente -->


       <div class="panel borde">
            <div class="panel-heading barra"> Riesgo inherente </div>
               <div class="panel-body">
                  <div class="row">
                    <div class="col-md-3">
                       <div class="form-group">
                         <label>Consecuencia</label>
                         <select name="rsg_ricons" class="form-control">
                            <option disabled selected>Pick one</option>
                           {{--  @foreach ($consecuencia as $consec)
                              <option value="{{ $consec->cons_id }}">{{ $consec->cons_descripcion }}</option>
                            @endforeach --}}
                         </select>
                       </div>
                    </div>
                       
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Probabilidad</label>
                          <select name="rsg_riprob" class="form-control">
                            <option disabled selected>Pick one</option>
                            {{-- @foreach ($probabilidad as $proba)
                              <option value="{{ $proba->proba_id }}">{{ $proba->proba_descripcion }}</option>
                            @endforeach --}}
                          </select>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Severidad</label>
                          <input name="rsg_severidad" class="form-control" placeholder="Enter text" readonly value="FUNCION">
                        </div>
                      </div>
                            
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Aceptación</label>
                          <select name="rsg_acepri" class="form-control" onchange="ShowSelected();" name="select" id= "select" >
                              <option disabled selected>Elegir</option>
                              <option value="1">Si</option>
                              <option value="0">No</option>
                           </select> 
                          </div>
                      </div>
                  </div>
                </div>
        </div>

          
   </form>        
  </div>


        {{-- Riesgo Controlado  --}}
      <div class="tab-pane fade" id="profile2">
       <div class="panel borde">
         <div class="panel-heading barra">Consultar controles del riesgo </div>
           <div class="panel-body">
              <div class="dataTable_wrapper">

                                      
                  <div class="container-fluid"> 
                    <div class="row">
                  
                      <table class="table table-striped table-bordered table-hover " id="dataTables-example2" style="border: 1px #fff solid;">
                        <thead class="col-12 col-sm-12 col-md-12 col-lg-12" >
                          
                                  <th style="border: 1px #fff solid;"></th>
                                  <th style="border: 1px #fff solid;"></th>
                                  <th style="border: 1px #fff solid;"></th>
                                  <th style="border: 1px #fff solid;"></th>
                           
                        </thead>
                            
                        <tbody>
                                  <th>Control</th>
                                  <th>Tipo</th>
                                  <th> Responsable</th>
                                  <th></th>
                                  

                             <tr class="even gradeC">

                                <td>Falla en los servidores DHCP a nivel de código del aplicativo ya que no existe contrato de soporte y mantenimiento con el proveedor.</td>
                                <td>Regulatorio / Legal</td>
                                <td>MIDDLEWARE</td>
                                <td class="center">

                                  <span class="dropdown">

                                    <button type="button" class="btn btn-default btn-sm " data-toggle="dropdown" aria-expanded="false" >Gestión &nbsp<i class="fa  fa-caret-down" style="font-size:20px; color: #20847a"></i></button>

                                       <ul class="dropdown-menu dropdown-user" style="right: 0px; left: auto">
                                           <li><a href="#"><i class="fa fa-eye fa-fw"></i> Ver</a> </li>
                                           <li><a href="#"><i class="fa fa-pencil fa-fw"></i> Modificar</a> </li>
                                           <li><a href="#"><i class="fa fa-trash fa-fw"></i> Eliminar</a>  </li>
                                       </ul>

                                  </span>

                                </td>

                               </tr>


                         </tbody>
                      </table>
                    </div>
                  </div>   
              </div>
            </div>
          </div>
        </div>


        {{-- Riesgo Tratado --}}

      <div class="tab-pane fade" id="messages2">
       <div class="panel borde">
         <div class="panel-heading barra">Consultar tratamientos del riesgo</div>
           <div class="panel-body">
              <div class="dataTable_wrapper">

                                      
                  <div class="container-fluid"> 
                    <div class="row">
                  
                      <table class="table table-striped table-bordered table-hover " id="dataTables-example1" style="border: 1px #fff solid;">
                        <thead class="col-12 col-sm-12 col-md-12 col-lg-12" >
                          
                                  <th style="border: 1px #fff solid;"></th>
                                  <th style="border: 1px #fff solid;"></th>
                                  <th style="border: 1px #fff solid;"></th>
                                  <th style="border: 1px #fff solid;"></th>
                           
                        </thead>
                            
                        <tbody>
                                  <th>&nbsp Tratamiento</th>
                                  <th>Tipo</th>
                                  <th> Responsable</th>
                                  <th></th> 
                                  

                             <tr class="even gradeC">

                                <td>Falla en los servidores DHCP a nivel de código del aplicativo ya que no existe contrato de soporte y mantenimiento con el proveedor.</td>
                                <td>Regulatorio / Legal</td>
                                <td>MIDDLEWARE</td>
                                <td class="center">

                                  <span class="dropdown">

                                    <button type="button" class="btn btn-default btn-sm " data-toggle="dropdown" aria-expanded="false" >Gestión &nbsp<i class="fa  fa-caret-down" style="font-size:20px; color: #20847a"></i></button>

                                       <ul class="dropdown-menu dropdown-user" style="right: 0px; left: auto">
                                           <li><a href="#"><i class="fa fa-eye fa-fw"></i> Ver</a> </li>
                                           <li><a href="#"><i class="fa fa-pencil fa-fw"></i> Modificar</a> </li>
                                           <li><a href="#"><i class="fa fa-trash fa-fw"></i> Eliminar</a>  </li>
                                       </ul>

                                  </span>

                                </td>

                               </tr>


                         </tbody>
                      </table>
                    </div>
                  </div>   
              </div>
            </div>
          </div>
        </div>

       

      
        {{-- Matriz de riesgo --}}
        <div class="tab-pane fade" id="messages3">
          <div class="panel borde">
           <div class="panel-heading barra">Consultar Matrices </div>
             <div class="panel-body">
                <div class="dataTable_wrapper">
                  @include("diseño.dibujo")
                </div>    
              </div>
          </div>
        </div>  



   </body>
</html>
@endsection