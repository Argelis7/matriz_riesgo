@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{ asset('css/estilos.css') }}">

<div class="container">
    <div class="row justify-content-md-center mt-5">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">
                    <strong>
                        <h4>Registro de Usuarios</h4>
                    </strong>
                </div>
                <div class="card-body">
                    <form role="form" method="POST" action="{{ url('/register') }}">
                        {!! csrf_field() !!}

                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label text-lg-right">Nombre y Apellido</label>

                            <div class="col-lg-6">
                                <input
                                        type="text"
                                        class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                        name="name"
                                        value="{{ old('name') }}"
                                        required
                                >
                                @if ($errors->has('name'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label text-lg-right">Correo Electrónico</label>

                            <div class="col-lg-6">
                                <input
                                        type="email"
                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                        name="email"
                                        value="{{ old('email') }}"
                                        required
                                >

                                @if ($errors->has('email'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label text-lg-right">Contraseña</label>

                            <div class="col-lg-6">
                               <input
                            type="password"
                             class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                             name="password"
                            {{-- required --}}
                        >

                                @if ($errors->has('password'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                         <div class="form-group row">
                            <label class="col-lg-4 col-form-label text-lg-right">Confirmar Contraseña</label>

                            <div class="col-lg-6">
                               <input
                            type="password"
                            class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                            name="password_confirmation"
                            {{-- required --}}
                        >

                                
                            </div>
                        </div>

                       
                        {{-- Password oculta + confimacion --}}
                        
                       
                              
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label text-lg-right">Usuario de red</label>

                            <div class="col-lg-6">
                                <input
                                        type="text"
                                        class="form-control{{ $errors->has('user_red') ? ' is-invalid' : '' }}"
                                        name="user_red"
                                        value="{{ old('user_red') }}"
                                        required
                                >

                                @if ($errors->has('user_red'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('user_red') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-lg-8 offset-lg-1">
                                <button type="submit" class="btn btn-block" style="background: #d32f2f;color:white; ">
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Footer-->
<footer class="font-small pt-0 footer-login">
    <div class="posicion-f">

    <div class="footer-copyright py-3 text-center">
        <div class="container-fluid">
          <div class="row">
            <div class="col-8 col-sm-8 col-md-11 col-lg-11 color-f">

                 <p>Sistema desarrollado por la Coordinación de Ingeniería de Seguridad, perteneciente a la Gerencia de Seguridad de la Operación y Servicios de Cantv 2018 </p>
            </div>
            
             <div class="col-4 col-sm-4 col-md-1  col-lg-1">
                <img src="{{ asset('imagenes/logo-blanco.png') }}" class="img-fluid" width="100%" height="0" align="right">
            </div>
          </div>
        </div>
       
    </div>
    <!--/Copyright-->
  </div>

</footer>
<!--/Footer-->
@endsection
