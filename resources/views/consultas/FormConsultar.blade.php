@extends("menus.nav")
@section("contenido")
{{-- <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script> --}}
<br>

<div class="container-fluid">
 <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="card ">
          <div id="mensaje_rev"></div>
          <div class="card-header  red darken-4 text-white"> <h5>Revisiones</h5> </div>
            <div class="card-body">
              <div class="tab-content">            
      
                                            
            

<div class="container-fluid">
            <div class="row justify-content-center">
                     
    



 <div class="page-wrapper col col-md-12 col-lg-12">
    <div class="card borde ">
       <div class="card-header grey darken-3 barra"> Consultar Revisión</div>
            <div class="card-body">
                <div class="dataTable_wrapper form-group">


                    <table id="revisiones_table" class="table table-striped table-bordered table-responsive-lg" width="100%">
                      <thead>
                        <tr>

                        {{--   <th>Fecha</th>      --}} 
                          <th class="th-lg">Control
                          </th>
                          <th class="th-sm">Tipo
                          </th>
                          <th class="th-sm">Responsable
                          </th>                          
                          <th>Acciones
                          </th>

                        </tr>
                      </thead>                   
                    
                     </table>  

                 

              <p class="oculto-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur.</p>
            
              </div>

            </div>
    </div>
</div>
</div>

  </div></div> <br></div></div></div></div></div>

  <script type="text/javascript">     
   
      DataTableConsulta("{{ auth()->id() }}");
        

  </script>

  {{-- <script type="text/javascript">
    $(function(){

      $('#revisiones_table').DataTable({

          "serverSide": true,
          "ajax": "{{ url('api/revisiones') }}",
          "columns": [
                {data: 'rev_nombre'},
                {data: 'rev_responsable'},
                {data: 'rev_trev'},
          ],

          "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }


        });

    })
  </script> --}}
@endsection

