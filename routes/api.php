<?php

use Illuminate\Http\Request;

/*===========================================================================
::::::::::::::::::::::::::::: selects dinamicos ::::::::::::::::::::::::::::
===========================================================================*/
Route::get('/select/a_impacto/{id}', 'RiesgoController@selectAreaImpactoN1');
Route::get('/select/f_impacton1/{id}', 'RiesgoController@selectFuenteImpactoN1');
Route::get('/select/f_impacton2/{id}', 'RiesgoController@selectFuenteImpactoN2');


Route::get('revisiones', 'RevisionController@consultaRevision');
// Route:post('/formulario/ajax', 'RiesgoController@registrarAjax');

// Route::get('revisiones', function(){

// 	return datatables()->eloquent(App\Models\Revision::query())
// 	->toJson();

// });
