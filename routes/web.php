<?php
Route::get('argelis', function () {
    return view('argelis2');
});

Route::get('deslogueo', function () {
    return view('cerrar.php');
});

Route::get('luis', function () {
    return view('/luis');
});

Route::get('matriz', function () {
    return view('diseño.dibujo');
});

// Route::get('index', function () {
//     return view('formularios.index');
// })->name('index');

Route::get('/index', 'HomeController@vistaInicio')->name('index');

Route::get('/', function () {
    // return view('welcome');
    return redirect()->route('login');
});

Route::get('/ver', function () {
    return view('luis');
});
// Route::get('crevision', function () {
//     return view('consultas.revision.revision');
// });

Route::get('criesgo', function () {
    return view('consultas.riesgo.RegistrarRiesgo');
})->name('consulta.revision');
//formulario de revision
Route::get('/revision', 'RevisionController@revision')->name('revision');
Route::post('/revision', 'RevisionController@registrarRevision')->name('registrarRevision');
Route::put('/revision/{id}/actualizar', 'RevisionController@ActualizarRevision')->name('actualizarRevision');
Route::delete('/revision/{id}', 'RevisionController@eliminarRevision')->name('revision.eliminar');

Route::get('/revision/{id}/editar', 'RevisionController@editarRevision')->name('revision.editar');


Route::delete('/revision/{id}/eliminar', 'RiesgoController@eliminarRiesgo')->name('riesgoInherente.eliminar');
Route::delete('/revision/{id}/eliminarc', 'RevisionController@eliminarConclusion')->name('conclusion.eliminar');
Route::delete('/revision/{id}/eliminaru', 'RevisionController@eliminarUnidad')->name('unidad.eliminar');





//formulario de registrar riesgo
Route::get('/registrariesgo/{id}', 'RiesgoController@riesgo')->name('registrar.riesgo');
Route::post('/registrariesgo', 'RiesgoController@registrarRiesgo')->name('registrarRiesgo');
Route::delete('/registrariesgo/{id}', 'RiesgoController@eliminarRiesgo')->name('riesgo.eliminar');
//actualizar formulario riesgo acepta rechaza
Route::put('/registrariesgo/{id}', 'RiesgoController@actualizar')->name('actualizar');
//para actualizar la ficha de riesgo inherente 
Route::get('/registrariesgo/{id}/editar', 'RiesgoController@verRiesgoInherente')->name('editar.riesgos');
// formulario para ver la consulta de revision
Route::get('/consultarevision', 'RevisionController@consultarRevisionVer')->name('consulta.revision');
// para actualizar riesgo inherente
Route::put('/registrariesgo/{id}/actualizar', 'RiesgoController@actualizarRiesgo')->name('actualizarRiesgo');
//pantalla de inicio 
Route::get('/inicio', 'FormController@showForm')->name('inicio');
//validacion de inicio
Route::post('/inicio', 'FormController@store')->name('inicio.store');
// actualizar aprobacion
Route::put('/registrariesgo/{id}/editar/actualizar', 'RiesgoController@actualizarAprobacion')->name('actualizar.aprobacion');


Route::put('/registrariesgo/{id}/editar/implementacion', 'RiesgoController@actualizarImplementacion')->name('actualizar.implementacion');

Auth::Routes();

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');

Route::post('cerrar', 'DeslogueoController@store')->name('deslogueo');


