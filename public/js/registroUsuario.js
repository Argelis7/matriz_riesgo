$(function(){
	// :::::::::: previene el evento de redireccion ::::::::::
	$('a[data-target="#modaRegistro"]').click(function(e){
		e.preventDefault();
	})

	// :::::::::: habilitar/desahibilitar los required de los input en login ::::::::::
	$("#registrarUsuario").click(function(event) {
		$("input#login, input#password").attr("required",false);
	});

	$("#cerrarRegistrarUsuario").click(function(event) {
		$("input#login, input#password").attr("required",true);
	});
	// :::::::::: habilitar/desahibilitar los required de los input en login ::::::::::
})