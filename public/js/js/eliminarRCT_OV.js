$(document).ready(function(){
  
        /////////////////////////////////////////////////////
        // para eliminar registros de controlado y tratado //
        /////////////////////////////////////////////////////
     
           
      var table = $('#controlado_table,#tratado_table').DataTable();

      $('#controlado_table,#tratado_table').on( 'click', 'button.borrar_fila',function () {

        $('#mensaje_con,#mensaje_tra').removeAttr('style', 'display:none'); 

         table
            .row( $(this).parents('tr') )
            .remove()
            .draw();    
         
         var idRiesgo = $('#id_inherente').val();
         var id = $(this).val(); 
         var tipo_riesgo = $(this).attr("name");
         var url = $(this).parents('form').attr('action');

         $.ajax({
          type: 'delete',
          url: url,
          headers: {'X-CSRF-TOKEN':$("meta[name='csrf-token']").attr("content") },
          data: {
            'id': id,
            'tipo_riesgo': tipo_riesgo,
            'idRiesgo': idRiesgo
          },
          dataType: "json",
          success: function(data) {   

          let variable = (data.mensaje);

          if (variable == 'Riesgo Controlado Eliminado') 
          {
             var mensaje = ("<div class='alert alert-danger'>"+data.mensaje+"</div>");
              $('#mensaje_con').html(mensaje);
              $('#mensaje_con').fadeOut(2000);    
              $('#mensaje_con').removeAttr('style', 'display:none');          
          }   

          if (variable == 'Riesgo Tratado Eliminado') 
          {
            if (localStorage.getItem('idInherenteEditar')) 
            {
              localStorage.setItem("mensaje",data.mensaje);
              location.reload();
            }else{
                   var mensaje = ("<div class='alert alert-danger'>"+data.mensaje+"</div>");
                    $('#mensaje_tra').html(mensaje);
                    $('#mensaje_tra').fadeOut(2000);  
                    $('#mensaje_tra').removeAttr('style', 'display:none'); 
                  }          
          }          


                                            /////////////
                                            // CONTROL //
                                            /////////////

          ///////////////////////////////////////////////////////////////////////////////////
          // para obtener los valores de las variables de control CORRECTIVO y PREVENTIVO  //
          ///////////////////////////////////////////////////////////////////////////////////

          var sumaControlCorrectivo = $("#sumaControlCorrectivo").val();
          var CantidadControlCorrectivo = $("#CantidadControlCorrectivo").val();

          var sumaControlPreventivo = $("#sumaControlPreventivo").val();
          var CantidadControlPreventivo = $("#CantidadControlPreventivo").val();

          var RestaSumaControlCorrectivo;
          var RestaSumaControlPreventivo;         

          //////////////////////////////////////////////////////////////
          // para asignar el value de los tipos correctivos CONTROLES //
          //////////////////////////////////////////////////////////////

          if (data.sumaControlCorrectivo > 0) {
              RestaSumaControlCorrectivo = sumaControlCorrectivo - (data.sumaControlCorrectivo);        
          }            
   
             RestaSumaControlCorrectivo = (data.sumaControlCorrectivo);                               
                                             
           $('#sumaControlCorrectivo').val(RestaSumaControlCorrectivo);
           $('#CantidadControlCorrectivo').val(data.CantidadControlCorrectivo);

          /////////////////////////////////////////////////////////////
          // para asignar el value a los tipos preventivos CONTROLES //
          /////////////////////////////////////////////////////////////

           if (data.sumaControlPreventivo > 0) {
              RestaSumaControlPreventivo = sumaControlPreventivo - (data.sumaControlPreventivo);          
          }
              RestaSumaControlPreventivo = (data.sumaControlPreventivo);                 
                       
           $('#sumaControlPreventivo').val(RestaSumaControlPreventivo);
           $('#CantidadControlPreventivo').val(data.CantidadControlPreventivo);

           // alert(data.CantidadControlCorrectivo + "control");
           // alert(data.CantidadControlPreventivo + "control");
                                    
                                      /////////////////
                                      // TRATAMIENTO //
                                      /////////////////


          ///////////////////////////////////////////////////////////////////////////////////////
          // para obtener los valores de las variables de tratamiento CORRECTIVO y PREVENTIVO  //
          ///////////////////////////////////////////////////////////////////////////////////////          

          var sumaTratamientoCorrectivo = $("#sumaTratamientoCorrectivo").val();
          var CantidadTratamientoCorrectivo = $("#CantidadTratamientoCorrectivo").val();

          var sumaTratamientoPreventivo = $("#sumaTratamientoPreventivo").val();
          var CantidadTratamientoPreventivo = $("#CantidadTratamientoPreventivo").val();

          var RestaSumaTratamientoCorrectivo;
          var RestaSumaTratamientoPreventivo;         

          /////////////////////////////////////////////////////////////////
          // para asignar el value a los tipos correctivos TRATAMIENTOS //
          ////////////////////////////////////////////////////////////////
          

          if (data.sumaTratamientoCorrectivo > 0) {
              RestasumaTratamientoCorrectivo = sumaTratamientoCorrectivo - (data.sumaTratamientoCorrectivo);        
          }            
   
             RestaSumaTratamientoCorrectivo = (data.sumaTratamientoCorrectivo);                               
                                             
           $('#sumaTratamientoCorrectivo').val(RestaSumaTratamientoCorrectivo);
           $('#CantidadTratamientoCorrectivo').val(data.CantidadTratamientoCorrectivo);

           ////////////////////////////////////////////////////////////////
           // para asignar el value a los tipos preventivos TRATAMIENTOS //
           ////////////////////////////////////////////////////////////////
          

           if (data.sumaTratamientoPreventivo > 0) {
              RestasumaTratamientoPreventivo = sumaTratamientoPreventivo - (data.sumaTratamientoPreventivo);          
          }
              RestasumaTratamientoPreventivo = (data.sumaTratamientoPreventivo);                 
                       
           $('#sumaTratamientoPreventivo').val(RestasumaTratamientoPreventivo);
           $('#CantidadTratamientoPreventivo').val(data.CantidadTratamientoPreventivo);
          
          // alert(CantidadTratamientoCorrectivo);
          // alert(CantidadTratamientoCorrectivo);
          },
            error: function(a,b,c){
              console.log(b);
            }

         }) ;
         return false;
       });
});