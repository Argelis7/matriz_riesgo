$(document).ready( function () {

  ///////////////////////////////
  // para eliminar los riesgos //
  ///////////////////////////////

     var table = $('#inherente_table').DataTable();

      $('#inherente_table').on( 'click', 'button.borrar_fila',function () {

        $('#mensaje_inhe').removeAttr('style', 'display:none'); 

         table
            .row( $(this).parents('tr') )
            .remove()
            .draw();    
         
         var id = $(this).val(); 
         var tipo_riesgo = $(this).attr("name");
         var url = $(this).parents('form').attr('action');

         $.ajax({
          type: 'delete',
          url: url,
          headers: {'X-CSRF-TOKEN':$("meta[name='csrf-token']").attr("content") },
          data: {
            'id': id,
            'tipo_riesgo': tipo_riesgo
          },
          dataType: "json",
          success: function(data) {   
          let variable = (data.mensaje);

          
             var mensaje = ("<div class='alert alert-danger'>"+variable+"</div>");
              $('#mensaje_inhe').html(mensaje);
              $('#mensaje_inhe').fadeOut(2000);  
              $('#mensaje_inhe').removeAttr('style', 'display:none');            
                   
           
          },
            error: function(a,b,c){
              console.log(b);
            }

         }) ;
         return false;
       });


});