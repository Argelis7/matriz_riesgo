// {{-- ** El Siguiente codigo calcula los valores del riesgo controlado **--}}


function implementacion(){

     var valor=document.getElementById("rcon_implementacion").value;
     document.getElementById("ocImp").value = valor;
     var combo=document.getElementById("ocImp");
     var selected = combo.options[combo.selectedIndex].text;
}
  
function documentacion(){

     var valor=document.getElementById("rcon_documentacion").value;
     document.getElementById("ocDocu").value = valor;
     var combo=document.getElementById("ocDocu");
     var selected = combo.options[combo.selectedIndex].text;
}


function ejecucion(){

     var valor=document.getElementById("rcon_ejecucion").value;
     document.getElementById("ocEjec").value = valor;
     var combo=document.getElementById("ocEjec");
     var selected = combo.options[combo.selectedIndex].text;
}


$(function(){

    $('#rcon_implementacion,#rcon_documentacion,#rcon_ejecucion').change(function(){

    var sImplementacion =  $('#ocImp option:selected').text();
    var sDocumentacion = $('#ocDocu option:selected').text();
    var sEjecucion = $('#ocEjec option:selected').text();
    var suma=parseInt(sImplementacion)+parseInt(sDocumentacion)+parseInt(sEjecucion);

    $('#sumaControl').val(suma);

        if (suma <= 39)
              $('#efectividad').val("Pobre").attr("style","background-color:#f13333e3");
        
        if (suma >= 39 && suma <= 54)
              $('#efectividad').val("Insatisfactorio").attr("style","background-color:orange");

        if (suma >= 55 && suma <= 74)    
               $('#efectividad').val("Regular").attr("style","background-color:yellow");

        if (suma >= 75 && suma <= 94)
              $('#efectividad').val("Buena").attr("style","background-color:#2ce82cde");             

        if (suma == 95 && suma <= 100)
              $('#efectividad').val("Excelente").attr("style","background-color:#2ce82cde");

    });
})
// {{-- ** ! El Siguiente codigo calcula los valores del riesgo controlado **--}}
// 

