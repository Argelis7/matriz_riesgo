$(document).ready( function () {

    		$('#controlado_table,#tratado_table,#inherente_table').DataTable({

          responsive: true,

          "order": [[ 0, "desc" ]], //ordenar ASC/DESC de manera predeterminada

            "columnDefs": [
            // mostrando columna y activando la busqueda
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],

      			"language": {
	      			// texto de los botones de paginacion ( por defecto vienen en ingles)
    				"paginate": {
      					"first": 	"Primera",
        				"last": 	"Ultima",
        				"next": 	"Siguiente",
        				"previous": "Anterior"
  					// // texto de los botones de paginacion
    				},
    				"decimal": ",",
    				"search": "Buscar:",
    				"loadingRecords": "Cargando...",
    				"processing":     "Procesando...",
    				"lengthMenu": "Mostrar _MENU_ registros por página",
		            "zeroRecords": "No hay coincidencias",
		            "info": "Mostrando página _PAGE_ de _PAGES_",
		            "emptyTable": "No hay registros en la tabla",
		            "infoEmpty": "No hay registros disponibles",
		            "infoFiltered": "(filtrado de un total de _MAX_ registros)"
  				},

    		});

        

      /////////////////////////////////////
      // DataTable de Revision CONSULTA  //
      /////////////////////////////////////
      
      // let idRevision = localStorage.getItem("idUser");

        // $('#revisiones_table').DataTable({             

        //   "serverSide": true,
        //   // "ajax": `api/revisiones/${idRevision}`,
        // "ajax":{
        //   'url':'api/revisiones',
        //   data:function(idRevision){
        //     idRevision.idRevsionUser = id_user;
        //   }
        // },
        //   "columns": [
        //         {data: 'rev_nombre'},
        //         {data: 'rev_trev'},
        //         {data: 'rev_responsable'},                
        //         {data: 'btn'},
        //   ],

        //   "language": {
        //         "sProcessing":     "Procesando...",
        //         "sLengthMenu":     "Mostrar _MENU_ registros",
        //         "sZeroRecords":    "No se encontraron resultados",
        //         "sEmptyTable":     "Ningún dato disponible en esta tabla",
        //         "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        //         "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        //         "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        //         "sInfoPostFix":    "",
        //         "sSearch":         "Buscar:",
        //         "sUrl":            "",
        //         "sInfoThousands":  ",",
        //         "sLoadingRecords": "Cargando...",
        //         "oPaginate": {
        //             "sFirst":    "Primero",
        //             "sLast":     "Último",
        //             "sNext":     "Siguiente",
        //             "sPrevious": "Anterior"
        //         },
        //         "oAria": {
        //             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        //             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        //         },
        //     }


        // });

      });

       



$(document).ready(function(){
    $('#table-unidades,#aprobacion_table,#implementacion_table,#table-conclusiones').DataTable({

          "order": [[ 0, "desc" ]], //ordenar ASC/DESC de manera predeterminada

           
            "language": {
              // texto de los botones de paginacion ( por defecto vienen en ingles)
            "paginate": {
                "first":  "Primera",
                "last":   "Ultima",
                "next":   "Siguiente",
                "previous": "Anterior"
            // // texto de los botones de paginacion
            },
            "decimal": ",",
            "search": "Buscar:",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "No hay coincidencias",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "emptyTable": "No hay registros en la tabla",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)"
          }

        });
})

      /////////////////////////////////////
      // DataTable de Revision CONSULTA  //
      /////////////////////////////////////
function DataTableConsulta(id_usuario){
   $('#revisiones_table').DataTable({             

          "serverSide": true,
          // De esta manera obtenemos el id del usuario autenticado para mostrar las consultas asociadas a el
        "ajax":{
          'url':'api/revisiones',
          data:function(idRevision){
            idRevision.idRevisionUser = id_usuario;
          }
        },
          "columns": [
                {data: 'rev_nombre'},
                {data: 'rev_trev'},
                {data: 'rev_responsable'},                
                {data: 'btn'},
          ],

          "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
            }


        });
}