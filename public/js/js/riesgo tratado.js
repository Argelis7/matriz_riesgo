// {{-- ** El Siguiente codigo calcula los valores del riesgo tratado **--}}

function complejidad(){

     var valor=document.getElementById("rtra_comptra").value;
     document.getElementById("ocCom").value = valor;
     var combo=document.getElementById("ocCom");
     var selected = combo.options[combo.selectedIndex].text;
}
  
function costoBeneficio (){

     var valor=document.getElementById("rtra_costobentra").value;
     document.getElementById("ocCB").value = valor;
     var combo=document.getElementById("ocCB");
     var selected = combo.options[combo.selectedIndex].text;
}


function tiempo(){

     var valor=document.getElementById("rtra_tiempotra").value;
     document.getElementById("ocTiempo").value = valor;
     var combo=document.getElementById("ocTiempo");
     var selected = combo.options[combo.selectedIndex].text;
}


$(function(){

  $('#rtra_comptra,#rtra_costobentra,#rtra_tiempotra').change(function(){

  var sComplejidad =  $('#ocCom option:selected').text();
  var sCostoBeneficio = $('#ocCB option:selected').text();
  var sTiempo = $('#ocTiempo option:selected').text();
  var suma=parseInt(sComplejidad)+parseInt(sCostoBeneficio)+parseInt(sTiempo);

  $('#sumaTratado').val(suma);

      if (suma <= 39)
            $('#efecTratado').val("Pobre").attr("style","background-color:#f13333e3");
      
      if (suma >= 39 && suma <= 54)
            $('#efecTratado').val("Insatisfactorio").attr("style","background-color:orange");

      if (suma >= 55 && suma <= 74)
      
             $('#efecTratado').val("Regular").attr("style","background-color:yellow");

      if (suma >= 75 && suma <= 94)
            $('#efecTratado').val("Buena").attr("style","background-color:#2ce82cde");           

      if (suma == 95 && suma <= 100)
            $('#efecTratado').val("Excelente").attr("style","background-color:#2ce82cde");

});
})
// {{-- ** ! El Siguiente codigo calcula los valores del riesgo tratado **--}}

// // {{-- ** Codigo para habilitar o deshabilitar pestaña de Riesgo TRATADO **--}}       
$(function(){

  $('#acp_s').on('click', function() {   
    localStorage.setItem("aceptacion",'true');
      var tra =document.getElementById("tratado");
      // tra.removeAttribute('data-toggle','tab');
      tra.removeAttribute('href');
      // tra.setAttribute("style","background-color:#cdd0d299");
  
  });
  $('#acp_n').on('click', function() {
    localStorage.setItem("aceptacion",'true');    
    var CCC = localStorage.getItem("CCC"); 
    var CCP = localStorage.getItem("CCP"); 

    if (CCC > 0 || CCP > 0) {
       var tra =document.getElementById("tratado");
           // tra.setAttribute("data-toggle","tab");
           tra.setAttribute("href","#messages2");
           // tra.setAttribute("style","background-color:#f4f7fa");
           localStorage.setItem("tratado",tra);
    }else{
      alert("Debe Agregar un Control");
    }

  
  });

  if ( localStorage.getItem("tratado")) 
  {
   var tra =document.getElementById("tratado");
           // tra.setAttribute("data-toggle","tab");
           tra.setAttribute("href","#messages2");
           // tra.setAttribute("style","background-color:#f4f7fa");
           localStorage.setItem("tratado",tra);
  }
  
});
// {{-- ** ! Codigo para habilitar o deshabilitar pestaña de Riesgo TRATADO **--}}