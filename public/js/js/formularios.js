/////////////////////////////////
// Formulario de revision AJAX //
/////////////////////////////////


$(document).ready(function(){
    $('#form').submit(function() {     
      $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $(this).serialize(),
        ///////////////////////////////////
        // para pasar todo en tipo json //
        //////////////////////////////////
        dataType: "json",
             //////////////////////////////////////////////////
             // Mostramos un mensaje con la respuesta de PHP //
             //////////////////////////////////////////////////
            success: function(data) {
         
                localStorage.setItem('id_revision',data.rev_id);
                localStorage.setItem('id_revision_mensaje',true);

                window.location = "registrariesgo/" + 0;
            }
      })
      return false;
    });





//////////////////////////////////////////////////////////////////////////////
// Guardar formulario de si acepta o rechaza el control vista de CONTROLADO //
//////////////////////////////////////////////////////////////////////////////

      $('#acp_s,#acp_n').click(function() {   
      var nombre_ar = $(this).attr("name");
      // dame el value que tiene el name rsg_id y luego lo enviamos en formato json
      var id = $(this).siblings('input[name="rsg_id"]').val();

      $.ajax({
        headers: {'X-CSRF-TOKEN':$("meta[name='csrf-token']").attr("content") },
        type: 'PUT',
        url: $(this).attr('action'),    
        data: { 
          "nombre": nombre_ar, //la variable nombre se manda al controlador mientras que nombre_ar es la de JS
          "rsg_id": id
        },
       
            success: function() {
              var mensaje = "<div class='alert alert-success'>Control registrado exitosamente</div>"
                 $('#mensaje_con').html(mensaje); 
                 $('#mensaje_con').removeAttr('style', 'display:none'); 
                 $('body,html').animate({scrollTop:0},500);               
                 $('#mensaje_con').fadeOut(2200);             

            },
            error: function(a){
              // alert("LA FUNCION ES ERRONEA");
              console.log(a);
            }
      })
      return false;
    });




    //////////////////////////////
    // formulario de controlado //
    //////////////////////////////
    
    let controlado = $('#controlado_table').DataTable();

    $('#form_con').submit(function() {

    $('#mensaje_con').removeAttr('style', 'display:none'); 

      $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        headers: {'X-CSRF-TOKEN':$("meta[name='csrf-token']").attr("content") },
        data: $(this).serialize(), 
        // dataType: "json",
      
             
            success: function(result) {
            // let fecha = JSON.parse(result.creacion);
            let fecha = result.creacion.date;
                fecha = moment(fecha).format('YYYY-MM-DD HH:mm:ss');
            
            const ruta = $("#eliminar_control").parent().attr('action');
            const id = (result.id);
            const papelera = `
                <form action="${ruta}" method="post">      
                  
                  <button type="submit" class="btn btn-danger btn-sm px-2 bord-rad borrar_fila" value='${id}' name="eliminarControl" id="eliminar_control">
                    <i class="fa fa-trash icon-bas" ></i>
                  </button>
                </form>
             `;

            controlado.row.add( [
              fecha,
              result.nombre,
              result.responsable,
              result.unidad,
              result.tipo,
              result.implementacion,
              result.documentacion,
              result.ejecucion,
              result.efectividad,
              papelera
            ] ).draw(false);

              $('#cancelar_con').trigger('click');
              var mensaje = ("<div class='alert alert-success'>"+result.mensaje+"</div>");
              $('#mensaje_con').html(mensaje);
              $('#mensaje_con').fadeOut(3000);
              $('#mensaje_con').removeAttr('style', 'display:none'); 
                         
              $('#sumaControlCorrectivo').val(result.sumaControlCorrectivo);
              $('#sumaControlPreventivo').val(result.sumaControlPreventivo);

              $('#CantidadControlCorrectivo').val(result.CantidadControlCorrectivo);
              $('#CantidadControlPreventivo').val(result.CantidadControlPreventivo);

              let CCC = result.CantidadControlCorrectivo;
              let CCP = result.CantidadControlPreventivo;
              localStorage.setItem("CCC",CCC);
              localStorage.setItem("CCP",CCP);
              localStorage.setItem("aceptacion",false);

                              
            }
          });            
           
      return false;
    });

    

    ///////////////////////////////
    // Formulario de Tratamiento //
    ///////////////////////////////

     let tratamiento = $('#tratado_table').DataTable();

      $('#form_tra').submit(function() {

        $('#mensaje_tra').removeAttr('style', 'display:none'); 

        $.ajax({
          type: 'POST',
          url: $(this).attr('action'),
          headers: {'X-CSRF-TOKEN':$("meta[name='csrf-token']").attr("content") },
          data: $(this).serialize(), 
          // dataType: "json",
        
               
              success: function(result) {

              let fechat = result.creacion.date;
                fechat = moment(fechat).format('YYYY-MM-DD HH:mm:ss');

              const rutat = $("#eliminar_tratamiento").parent().attr('action');
              const idt = (result.id);
              const papelera = `
              
                
                    <form action="${rutat}" method="post">      
                      
                      <button type="submit" class="btn btn-danger btn-sm px-2 bord-rad borrar_fila" value='${idt}' name="eliminarTratamiento" id="eliminar_tratamiento">
                        <i class="fa fa-trash icon-bas" ></i>
                      </button>
                    </form>            
                  

                           
               `;

               if (localStorage.getItem('idInherenteEditar')) {
                  localStorage.setItem("mensaje",result.mensaje);
                  location.reload();
               }else{




              tratamiento.row.add( [
                fechat,
                result.nombre,
                result.responsable,
                result.unidad,
                result.tipo,
                result.complejidad,
                result.costoben,
                result.tiempo,
                result.efectividad,
                papelera
              ] ).draw(false);

                $('#cancelar_tra').trigger('click');
                var mensaje = ("<div class='alert alert-success'>"+result.mensaje+"</div>");
                $('#mensaje_tra').html(mensaje);
                $('#mensaje_tra').fadeOut(3000);
                $('#mensaje_tra').removeAttr('style', 'display:none');    

                $('#sumaTratamientoCorrectivo').val(result.sumaTratamientoCorrectivo);
                $('#sumaTratamientoPreventivo').val(result.sumaTratamientoPreventivo);

                $('#CantidadTratamientoCorrectivo').val(result.CantidadTratamientoCorrectivo);
                $('#CantidadTratamientoPreventivo').val(result.CantidadTratamientoPreventivo);    

                // localStorage.setItem("mensaje",result.mensaje);
                // location.reload();
            }
                                
              }
            });            
        return false;
      });


      ///////////////////////////////////////////////
      // Formulario para guardar Riesgo Inherente  //
      ///////////////////////////////////////////////
    
      $('#form_riesgo').submit(function() {     

      $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $(this).serialize(),
                  
            success: function(data) {

              // $('input[name="rcon_rsgid"]').val(data.id);
             
             var aceptaRI= $('#elegir').val();
              $("#aceptar").attr("style", "display:none");
              $("#cancelarRiesgo").attr("style","display:none");

               if (aceptaRI == 0)
    {

       //////////////////////////////////////////////////////////////////
       // variable que me permte relacion entre inherente y controlado //
       //////////////////////////////////////////////////////////////////
           
       // var id_inherente = $("#id_inherente").val();
       var id_inherente = data.id;  

       
        localStorage.setItem('id_inherente',id_inherente);       
       
      
      $('input[name="rcon_rsgid"]').val(localStorage.getItem("id_inherente"));
      $('input[name="rtra_rsgid"]').val(localStorage.getItem("id_inherente"));    
      $('input[name="rsg_id"]').val(localStorage.getItem("id_inherente"));

    ///////////////////////////////////////////////
    // para activar links de CONTROLADO y MATRIZ //
    ///////////////////////////////////////////////

      $('#controlado').attr('href', '#profile2');

      $('#matriz').attr('href', '#messages3');


         
    }

    if (aceptaRI == 1) 
    {
       //////////////////////////////////////////////////////////////////////////
       // variable que me permte relacion entre inherente y controlado         //
       //////////////////////////////////////////////////////////////////////////

         var id_inherente = data.id;  

       
        localStorage.setItem('id_inherente',id_inherente);  
         
        $('input[name="rcon_rsgid"]').val(localStorage.getItem("id_inherente"));
        $('input[name="rtra_rsgid"]').val(localStorage.getItem("id_inherente"));  
        $('input[name="rsg_id"]').val(localStorage.getItem("id_inherente"));  

      var tra =document.getElementById("tratado");
           // tra.removeAttribute('data-toggle','tab');
           tra.removeAttribute('href');
      //      tra.setAttribute("style","background-color:#cdd0d299"); 

      var con= document.getElementById("controlado");
      //       con.removeAttribute('data-toggle','tab');
            con.removeAttribute('href');
      //       con.setAttribute("style","background-color:#cdd0d299");

      // var ini =document.getElementById("inicio");
      //     ini.removeAttribute("class","active");
       
      // var ma= document.getElementById("linma");
      //     ma.setAttribute("class","active");

      var ma= document.getElementById("matriz");
      //     ma.setAttribute("data-toggle","tab");
          ma.setAttribute("href","#messages3");
      //     ma.setAttribute("style","background-color:#f4f7fa");

        // $('#lola').trigger('click'); 

     
    }


    $('#id_inherente').val(data.id);

    var mensaje = data.mensaje;

    localStorage.setItem('mensaje',mensaje);

    window.location = data.id;   

             
             // alert(data.mensaje);

                
            }
      })
      return false;
    });

    ////////////////////////////////////////////////////////////
    // mensaje de que se envio correctamente Riesgo Inherente //
    ////////////////////////////////////////////////////////////
    
    if (localStorage.getItem("mensaje")) {

       var mensaje = ("<div class='alert alert-success'>"+localStorage.getItem("mensaje")+"</div>");
                $('#mensaje_inhe').html(mensaje);
                $('#mensaje_inhe').fadeOut(3000);
                $('#mensaje_inhe').removeAttr('style', 'display:none');

       localStorage.removeItem('mensaje');

    }
    
    
    
 });

/////////////////////////////////////////
// formulario de unidades involucradas //
/////////////////////////////////////////

$(document).ready(function(){

   let unidad = $('#table-unidades').DataTable();

    $('#form_unidad').submit(function() {

    $('#mensaje_uni').removeAttr('style', 'display:none'); 

      $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        headers: {'X-CSRF-TOKEN':$("meta[name='csrf-token']").attr("content") },
        data: $(this).serialize(), 
        // dataType: "json",
      
             
            success: function(result) {
              
              localStorage.setItem('mjs_unidades',result.mensaje)             
            
              location.reload();

              // $('#cancelar_unidad').trigger('click');
              // var mensaje = ("<div class='alert alert-success'>"+result.mensaje+"</div>");
              // $('#mensaje_uni').html(mensaje);
              // $('#mensaje_uni').fadeOut(3000);
              // $('#mensaje_uni').removeAttr('style', 'display:none');  
              // $('#form_unidad')[0].reset();     

             
            }
          });            
           
      return false;
    });

    if (localStorage.getItem('mjs_unidades')) 
    {
      var mensaje = ("<div class='alert alert-success'>"+localStorage.getItem('mjs_unidades')+"</div>");
        $('#prueba').html(mensaje);
        $('#prueba').fadeOut(4000);
        $('#prueba').removeAttr('style', 'display:none');
      localStorage.removeItem('mjs_unidades');
    }

    //////////////////////////////
    // formulario de aprobacion //
    //////////////////////////////

    $('#form_apro').submit(function() {

    $('#mensaje_apro').removeAttr('style', 'display:none'); 

      $.ajax({
        type: 'POST',

        url: $(this).attr('action'),
        headers: {'X-CSRF-TOKEN':$("meta[name='csrf-token']").attr("content") },
        data: $(this).serialize(), 
        dataType: "json",
                  
            success: function(result) {            
            
            localStorage.setItem('mjs_aprobacion',result.mensaje)             
            
            location.reload();
             
            }
            ,
            error: function(a,b,c){
              // const data = $.parseJSON(a.responseText);
              // console.log( data.errors.aptra_rtraid[0] )
             
              $('#cancelar_apro').trigger('click');
              var mensaje = ("<div class='alert alert-danger'>"+'Ya el Tratamiento se encuentra asociado a una Aprobación'+"</div>");
              $('#mensaje_apro').html(mensaje);
              $('#mensaje_apro').fadeOut(3000);
              $('#mensaje_apro').removeAttr('style', 'display:none');  
              $('#form_apro')[0].reset();
              
            }
          });            
           
      return false;
    });

    //////////////////////////////
    // formulario de conclusion //
    //////////////////////////////




})
