 //////////////////////////////////////////////////////////////////////
 // {{-- ** codigo para activar y desactivar paginas CONSULTA **--}} //
 //////////////////////////////////////////////////////////////////////

                     ///////////////////
                     // MENU REVISION //
                     ///////////////////

  function activar(){   
  
    document.getElementById("opcion").disabled = false;
    document.getElementById("agregar").disabled = false;
    // document.getElementById("agregart").disabled = false;   
    // document.getElementById("on").style.background = "#ff4f5e";
    // document.getElementById("off").style.background = "grey";       
  }

  function desactivar(){
   
    document.getElementById("opcion").disabled = true;
    document.getElementById("agregar").disabled = true;
    // document.getElementById("agregart").disabled = true;
    // document.getElementById("off").style.background = "#ff4f5e";
    // document.getElementById("on").style.background = "grey";
  }

  $(document).ready(function(){

    $('#onr').click(function() { 
        $('#offr').attr("style","background-color:grey");
        $('#onr').attr("style","background-color:#ff4f5e"); 
        $("#agregar_u,#eliminar_u").removeAttr("disabled");          
        $('#agregar_u').parent().attr("data-toggle", "modal");   
        $('#agregar').parent().attr('href',"registrariesgo"); //esta en revision
    });
     $('#offr').click(function() {         
        $('#onr').attr("style","background-color:grey"); 
        $('#offr').attr("style","background-color:#ff4f5e");  
        $("#agregar_u,#eliminar_u").prop('disabled', 'true'); 
        $('#agregar_u').parent().removeAttr('data-toggle');
    });

                     /////////////////
                     // MENU RIESGO //
                     /////////////////

      $('#on').click(function() { 
        $('#off').attr("style","background-color:grey");
        $('#on').attr("style","background-color:#ff4f5e");   
        $("#opcion_riesgo,#b_controlado,#b_tratado,#acp_s,#acp_n,#b_aprobacion").removeAttr("disabled");           
    });
     $('#off').click(function() {         
        $('#on').attr("style","background-color:grey"); 
        $('#off').attr("style","background-color:#ff4f5e"); 
        $("#opcion_riesgo,#b_controlado,#b_tratado,#acp_s,#acp_n,#b_aprobacion").prop('disabled', 'true'); 

    });

  })
  
    
