$(function(){
	// al cambiar select se habilitan las funciones
	$('#rsg_aimpact').on('change', selectAreaImpactoN1);
	$('#rsg_fuente_riesgo').on('change', selectFuenteImpactoN1);
	$('#rsg_friesgon1').on('change', selectFuenteImpactoN2);
	$('#rsg_fuente_riesgo').on('change', resetFuenteImpactoN2);
	// agregar opciones "Otros" al formulario con value = "0"
	// $('#rsg_aimpact').append('<option value="0">Otros</option>');
	// $('#rsg_fuente_riesgo').append('<option value="0">Otros</option>');
	// si el value = 0, habilita el input OtraAreaImpacto
	$('#rsg_aimpactn1').on('change ', habilitaOtraAreaImpacto);
	// si el valor se cambia deshabilita el input OtraAreaImpacto
	$('#rsg_aimpact').on('change', deshabilitaOtraAreaImpacto);
	// si el value = 0, habilita el input OtraFuenteImpacto
	$('#rsg_friesgon2').on('change ', habilitaOtraFuenteImpacto);
	// si el valor se cambia deshabilita el input OtraFuenteImpacto
	$('#rsg_fuente_riesgo').on('change', deshabilitaOtraFuenteImpacto);
	$('#rsg_friesgon1').on('change', deshabilitaOtraFuenteImpacto);
		
});

// muestra las opciones de "Area de Impacto nivel 1" segun el value(id) del select "Area de Impacto"
function selectAreaImpactoN1()
{
	var aimpacn1_aimpacid = $(this).val();		
	
	//AJAX
	$.get('../api/select/a_impacto/'+aimpacn1_aimpacid, function (data)
	{
		var html_select = '<option value="" disabled selected>Seleccione</option>'
		for(var i=0; i<data.length; i++)
		{
			html_select += '<option value="'+data[i].aimpac_id+'">'+data[i].aimpac_descripcion+'</option>';
		}
		
		$('#rsg_aimpactn1').html(html_select);
		
		// html_select = '<option value="0">Otros</option>';
		// $('#rsg_aimpactn1').append(html_select);

	});
}
// muestra las opciones de "Fuente de Impacto nivel 1" segun el value(id) del select "Fuente de Impacto"
function selectFuenteImpactoN1()
{
	var frn1_frsgid = $(this).val();
	
	//AJAX
	$.get('../api/select/f_impacton1/'+frn1_frsgid, function (data)
	{
		var html_select = '<option value="" disabled selected>Seleccione</option>';
		for(var i=0; i<data.length; i++)
		{
			html_select += '<option value="'+data[i].frn1_id+'">'+data[i].frn1_descripcion+'</option>';
		}

		$('#rsg_friesgon1').html(html_select);

		// html_select = '<option value="0">Otros</option>';
		// $('#rsg_friesgon1').append(html_select);
		
	});
}
// muestra las opciones de "Fuente de Impacto nivel 2" segun el value(id) del select "Fuente de Impacto nivel 1"
function selectFuenteImpactoN2()
{
	var rsg_friesgon2 = $(this).val();
	
	//AJAX
	$.get('../api/select/f_impacton2/'+rsg_friesgon2, function (data)
	{
		var html_select = '<option value="" disabled selected>Seleccione</option>';
		for(var i=0; i<data.length; i++)
		{
			html_select += '<option value="'+data[i].frn2_id+'">'+data[i].frn2_descripcion+'</option>';
		}
		
		$('#rsg_friesgon2').html(html_select);

		// html_select = '<option value="0">Otros</option>';
		// $('#rsg_friesgon2').append(html_select);
		
	});
}
// vuelve a colocar el '<option value="">Seleccione</option>' por defecto al cambiar el select "Fuente de Impacto"
function resetFuenteImpactoN2()
{
	var html_select = '<option value="" disabled selected>Seleccione</option>';
	$('#rsg_friesgon2').html(html_select);
}
// si el value = 0, habilita el input OtraAreaImpacto
function habilitaOtraAreaImpacto()
{
	let ValAreaImpN1 = $(this).val();

	  if (ValAreaImpN1 == 16) {
	    $('#rsg_otroarea').removeAttr('disabled');
	  }
}
// si el valor se cambia deshabilita el input OtraAreaImpacto
function deshabilitaOtraAreaImpacto()
{
	$('#rsg_otroarea').attr('disabled','true');
}
// si el value = 0, habilita el input OtraFuenteImpacto
function habilitaOtraFuenteImpacto(){
	let ValFuenteImpN2 = $(this).val();

	  if (ValFuenteImpN2 == 116) {
	    $('#rsg_otrofuente').removeAttr('disabled');
	  }
}
// si el valor se cambia deshabilita el input OtraFuenteImpacto
function deshabilitaOtraFuenteImpacto()
{
	$('#rsg_otrofuente').attr('disabled','true');
}
