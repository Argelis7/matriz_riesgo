$(document).ready(function(){

	$('.aprobacion').on('click', function(){
		let tratamiento_propuestos = $(this).parents('tr').children('td.tra_propuestos').text();
		let id_tra = $(this).val();
		$('input[name="aptra_nombretra"]').val(tratamiento_propuestos);
		$('input[name="aptra_rtraid"]').val(id_tra);
		$('input[name="aptra_rsgid"]').val(localStorage.getItem("idInherenteEditar"));
	});

	// $('#g_aprobacion').click(function(){
	// 	let estado_respuesta = $('#est_res').val();
	// 	if (estado_respuesta == 2) {
	// 		localStorage.setItem("dos",estado_respuesta);
	// 	}

	// 	// if (estado_respuesta == 1) {
	// 	// 	localStorage.setItem("uno",estado_respuesta);
	// 	// }
		
	// })

	$('#m_aprobacion').click(function(){

		$('td.estadoR').each(function(index){
			let estado_respuesta = $(this).text();
			let padre = $(this).parent().children('td.apro').children('form').children('div');

			if (estado_respuesta == 'Cerrado') {				
			padre.children('button.implementacion').attr("style","background:blue");
			}else{
				padre.children('button.edit_aprobacion').attr("style","background:blue");				
			}
		})		
	});

	if (localStorage.getItem("mjs_aprobacion")) {
	  let mensaje_apro = localStorage.getItem("mjs_aprobacion");
	  var mensaje = ("<div class='alert alert-success'>"+mensaje_apro+"</div>");
      $('#mensaje_inhe').html(mensaje);
      $('#mensaje_inhe').fadeOut(5000);
      $('#mensaje_inhe').removeAttr('style', 'display:none'); 
      localStorage.removeItem('mjs_aprobacion'); 
	}

	$('#mensaje_apro').fadeOut(3000);


})