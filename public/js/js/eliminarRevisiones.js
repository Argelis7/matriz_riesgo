$(document).ready(function(){

        //////////////////////////////
        // Para eliminar Revisiones //
        //////////////////////////////
        
        var table_revision = $('#revisiones_table').DataTable();

      $('#revisiones_table').on( 'click', 'button.borrar_fila',function () {

        $('#mensaje_rev').removeAttr('style', 'display:none'); 
  
         
         var id = $(this).val(); 
         var url = $(this).parents('form').attr('action');

         table_revision
            .row( $(this).parents('tr') )
            .remove()
            .draw();  

         $.ajax({
          type: 'delete',
          url: url,
          headers: {'X-CSRF-TOKEN':$("meta[name='csrf-token']").attr("content") },
          data: {
            'id': id,
          },
          dataType: "json",
          success: function(data) {   
          let variable = (data.mensaje);

           var mensaje = ("<div class='alert alert-success'>"+data.mensaje+"</div>");
            $('#mensaje_rev').html(mensaje);
            $('#mensaje_rev').fadeOut(2000);    
            $('#mensaje_rev').removeAttr('style', 'display:none');
           
          },
            error: function(a,b,c){
              console.log(b);
            }

         }) ;
         return false;
       });

  });