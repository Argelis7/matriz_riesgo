$(function(){

    $("#matriz").click(function() {	 
	

    ///////////////////////////////////////////////////////////
	// variables guardadas para dibujar el riesgo controlado //
    ///////////////////////////////////////////////////////////
    
	var ConsProbControl_aux = localStorage.getItem("ConsProbControl")
	localStorage.setItem("ConsProbControl_aux",ConsProbControl_aux);
	var ConsProbControl_cAux = localStorage.getItem("ConsProbControl_c");
	localStorage.setItem("ConsProbControl_cAux",ConsProbControl_cAux);

    ////////////////////////////////////////////////////////
	// variables guardadas para dibujar el riesgo tratado //
    ////////////////////////////////////////////////////////
    
    var ConsProbTrata_aux = localStorage.getItem("ConsProbTrata")
	localStorage.setItem("ConsProbTrata_aux",ConsProbTrata_aux);
	var ConsProbTrata_cAux = localStorage.getItem("ConsProbTrata_t");
	localStorage.setItem("ConsProbTrata_cAux",ConsProbTrata_cAux);

			    ///////////////////////////////////////////////
				// comenzamos a dibujar el RIESGO INHERENTE  //
			    ///////////////////////////////////////////////

	var conse_x = $('#consecuencia').val();
	var proba_y = $('#probabilidad').val();
	var num1 =  'p' + conse_x + proba_y;
	var num1i =  'pi' + conse_x + proba_y;
    	
    	     
  	$("#" + num1).addClass('inherente');
  	$("#" + num1i).addClass('inherente_pe');  

  	var CantidadControlCorrectivo = $('#CantidadControlCorrectivo').val();
  	var CantidadControlPreventivo = $('#CantidadControlPreventivo').val();
	var status = localStorage.getItem("aceptacion");

  	
			      ////////////////////////////////////////////////
			      // ecuacion para dibujar un riesgo controlado //
			      ////////////////////////////////////////////////
      

	   ///////////////////////////////////////////////////////////////////////////////////////
  	  // calculamos el promedio del control que es la suma entre la cantidad de controles  //
      // la consecuencia se calcula con el tipo CORRECTIVO                                 //
      // la probabilidad con el tipo PREVENTIVO                        		               //
	  ///////////////////////////////////////////////////////////////////////////////////////
	  
	  /////////////////////////////////////
	  // promedio del control correctivo //
	  /////////////////////////////////////
	  
	  // variables
	  
	  var promedio_ConCorrec;
	  var PorcPromedio_ConCorrec;
	  var PorcTrata_implemen;


	  var sumaControlCorrectivo = $('#sumaControlCorrectivo').val();
	  var CantidadControlCorrectivo = $('#CantidadControlCorrectivo').val();

	  if (sumaControlCorrectivo && CantidadControlCorrectivo != 0) {

	  	 promedio_ConCorrec = Math.ceil (sumaControlCorrectivo / CantidadControlCorrectivo);
	  	 PorcPromedio_ConCorrec = promedio_ConCorrec / 100 ;
	  	 PorcTrata_implemen = 0;  
	  
	  }
	  else {
	  	 PorcPromedio_ConCorrec = 0 ;
	  	 PorcTrata_implemen = 0; 	  	
	  }


	  ////////////////////////////////////////////////////
	  // calcular consecuencia del control (CORRECTIVO) //
	  ////////////////////////////////////////////////////
	  
	  var ConsCorrectivo = [conse_x - (conse_x * PorcPromedio_ConCorrec) - (conse_x * PorcTrata_implemen)];		 

	  if (ConsCorrectivo < 1) {
	  	 var ConseContCorrectivo = 1;
	  }else{
	  	 var ConseContCorrectivo = Math.ceil(ConsCorrectivo);
	  }


	  /////////////////////////////////////
	  // promedio del control preventivo //
	  /////////////////////////////////////
	 
	 // variables
	  var promedio_ConPrevent;
	  var PorcPromedio_ConPrevent;
	  var PorcTrata_implemenP; 


	  var sumaControlPreventivo = $('#sumaControlPreventivo').val();
	  var CantidadControlPreventivo = $('#CantidadControlPreventivo').val();

	 if (sumaControlPreventivo && CantidadControlPreventivo != 0) {

	    promedio_ConPrevent = Math.ceil (sumaControlPreventivo / CantidadControlPreventivo);
	    PorcPromedio_ConPrevent = promedio_ConPrevent / 100 ;
	    PorcTrata_implemenP = 0; 
	 }
	 else{
		PorcPromedio_ConPrevent = 0;
		PorcTrata_implemenP = 0; 
	 }	 

	  ///////////////////////////////////////////////////////
	  // calcular la probabilidad del control (PREVENTIVO) //
	  ///////////////////////////////////////////////////////

	  var ProbPreventivo = [proba_y - (proba_y * PorcPromedio_ConPrevent) - (proba_y * PorcTrata_implemenP)];	

	  if (ProbPreventivo < 1) {
	  	 var ProbContPreventivo = 1;
	  }else{
	  	 var ProbContPreventivo = Math.ceil(ProbPreventivo);
	  }


	  ///////////////////////////////////////////////////////////////////////////////////
	  // concatenamos el punto de consecuencia y probabilidad para dibujar el control  //
	  ///////////////////////////////////////////////////////////////////////////////////	
	  
	  var ConsProbControl = 'p' +  ConseContCorrectivo + ProbContPreventivo;
	  var ConsProbControl_c = 'pc' +  ConseContCorrectivo + ProbContPreventivo;



    localStorage.setItem("ConsProbControl",ConsProbControl);  
    localStorage.setItem("ConsProbControl_c",ConsProbControl_c); 

    
     
	if (CantidadControlCorrectivo > 0 || CantidadControlPreventivo > 0) {
	    if (ConsProbControl_aux != localStorage.getItem("ConsProbControl")) {
	    	$('#' + ConsProbControl_aux).removeClass('controlado');
	    	$('#' + ConsProbControl_cAux).removeClass('controlado_pe');
	      	$('#' + ConsProbControl).addClass('controlado').addClass('ml-3');  
	      	$('#' + ConsProbControl_c).addClass('controlado_pe');  

	    }else{
	      	$('#' + ConsProbControl).addClass('controlado').addClass('ml-3');      	
	      	$('#' + ConsProbControl_c).addClass('controlado_pe');      	
	    }
    }else{    	
    		$('#' + ConsProbControl_aux).removeClass('controlado');
	    	$('#' + ConsProbControl_cAux).removeClass('controlado_pe');
    }

    
     
					      /////////////////////////////////////////////
					      // ecuacion para dibujar el riesgo tratado //
					      /////////////////////////////////////////////
    
      
      /////////////////////////////////////////////////////////////////////////////////////////////
      // calculamos el promedio del tratamiento que es la suma entre la cantidad de tratamientos //
      // la consecuencia se calcula con el tipo CORRECTIVO                                       //
      // la probabilidad con el tipo PREVENTIVO                                                  //
      /////////////////////////////////////////////////////////////////////////////////////////////

	   /////////////////////////////////////////
	   // promedio del tratamiento correctivo //
	   /////////////////////////////////////////
	   
	    // variables
	  var promedio_TraCorrec;
	  var PorcPromedio_TraCorrec;
	  var PorcTrata_implemen;


	  var sumaTratamientoCorrectivo = $('#sumaTratamientoCorrectivo').val();
	  var CantidadTratamientoCorrectivo = $('#CantidadTratamientoCorrectivo').val();
	

	  if (sumaTratamientoCorrectivo && CantidadTratamientoCorrectivo != 0) {

	    promedio_TraCorrec = Math.ceil (sumaTratamientoCorrectivo / CantidadTratamientoCorrectivo);
	    PorcPromedio_TraCorrec = promedio_TraCorrec / 100 ;
	    PorcTrata_implemen = 0; 
	  }
	  else{
		PorcPromedio_TraCorrec = 0;
		PorcTrata_implemenP = 0; 
	  }		  


	  ////////////////////////////////////////////////////////
	  // calcular consecuencia del tratamiento (CORRECTIVO) //
	  ////////////////////////////////////////////////////////

	  
	  var ConsTraCorrectivo = [conse_x - (conse_x * PorcPromedio_ConCorrec) - (conse_x * PorcPromedio_TraCorrec) - (conse_x * PorcTrata_implemen)];		 

	  if (ConsTraCorrectivo < 1) {
	  	 var ConseTraCorrectivo = 1;
	  }else{
	  	 var ConseTraCorrectivo = Math.ceil(ConsTraCorrectivo);
	  }

	  /////////////////////////////////////////
	  // promedio del tratamiento preventivo //
	  /////////////////////////////////////////
	
	 
	 // variables
	  var promedio_TraPrevent;
	  var PorcPromedio_TraPrevent;
	  var PorcTrata_implemenP; 


	  var sumaTratamientoPreventivo = $('#sumaTratamientoPreventivo').val();
	  var CantidadTratamientoPreventivo = $('#CantidadTratamientoPreventivo').val();

	 if (sumaTratamientoPreventivo && CantidadTratamientoPreventivo != 0) {

	    promedio_TraPrevent = Math.ceil (sumaTratamientoPreventivo / CantidadTratamientoPreventivo);
	    PorcPromedio_TraPrevent = promedio_TraPrevent / 100 ;
	    PorcTrata_implemenP = 0; 
	 }
	 else{
		PorcPromedio_TraPrevent = 0;
		PorcTrata_implemenP = 0; 
	 }	 

	  ///////////////////////////////////////////////////////
	  // calcular la probabilidad del control (PREVENTIVO) //
	  ///////////////////////////////////////////////////////
	 
	  var ProbTraPreventivo = [proba_y - (proba_y * PorcPromedio_ConPrevent) - (proba_y * PorcPromedio_TraPrevent) - (proba_y * PorcTrata_implemenP)];	  

	  if (ProbTraPreventivo < 1) {
	  	 var ProbaTraPreventivo = 1;
	  }else{
	  	 var ProbaTraPreventivo = Math.ceil(ProbTraPreventivo);
	  }
		
	  
	  /////////////////////////////////////////////////////////////////////////////////////
	  // concatenamos el punto de consecuencia y probabilidadpara dibujar el tratamiento //
	  /////////////////////////////////////////////////////////////////////////////////////
	  
	  var ConsProbTrata = 'p' +  ConseTraCorrectivo + ProbaTraPreventivo;
	  var ConsProbTrata_t = 'pt' +  ConseTraCorrectivo + ProbaTraPreventivo;



    localStorage.setItem("ConsProbTrata",ConsProbTrata);  
    localStorage.setItem("ConsProbTrata_t",ConsProbTrata_t); 

    

	if (CantidadTratamientoCorrectivo > 0 || CantidadTratamientoPreventivo > 0) {
	    if (ConsProbTrata_aux != localStorage.getItem("ConsProbTrata")) {
	    	$('#' + ConsProbTrata_aux).removeClass('tratado');
	    	$('#' + ConsProbTrata_cAux).removeClass('tratado_pe');
	      	$('#' + ConsProbTrata).addClass('tratado').addClass('ml-5');  
	      	$('#' + ConsProbTrata_t).addClass('tratado_pe');  

	    }else{
	      	$('#' + ConsProbTrata).addClass('tratado').addClass('ml-5');      	
	      	$('#' + ConsProbTrata_t).addClass('tratado_pe');      	
	    }
    }else{    	
    		$('#' + ConsProbTrata_aux).removeClass('tratado');
	    	$('#' + ConsProbTrata_cAux).removeClass('tratado_pe');
    }
		

    });
  });