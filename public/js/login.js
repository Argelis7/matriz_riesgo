$(function(){
	// alerta si el usuario ingresa datos invalidos

	$('#usuario_incorrecto').click(function(){
       toastr.error(' Usuario/E-mail o Contraseña invalida');
	});

	// ******************************************
	// 
	// alerta si el usuario ingreso menos de 4 digitos

	$('#mensajeModal').click(function(){
       toastr.error('Contraseña debe contener al menos 4 caracteres.');
	});


	//**********************************************
	//
	// alerta para decir que el usuario ingreso la clave erronea
	var mensajeClaveAdministradorE = $('#mensajeLogin').text();

	$('#mensajeLogin').click(function(){
       toastr.error(mensajeClaveAdministradorE);
	});

	//**********************************************
	//
	// alerta para decir que el usuario ingreso la clave erronea
	var mensajeResCon = $('#mensajeResCon').children().val();

	$('#mensajeResCon').click(function(){
       toastr.success(mensajeResCon);
	});

	//**********************************************
	//
	// Activador de eventos anteriores


	$('#usuario_incorrecto,#mensajeModal,#mensajeLogin,#mensajeResCon').trigger('click');

})

	


