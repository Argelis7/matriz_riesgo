/////////////////////////////////
// Formulario de revision AJAX //
/////////////////////////////////


$(document).ready(function(){
    $('#form').submit(function() {     
      $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $(this).serialize(),
        ///////////////////////////////////
        // para pasar todo en tipo json //
        //////////////////////////////////
        dataType: "json",
             //////////////////////////////////////////////////
             // Mostramos un mensaje con la respuesta de PHP //
             //////////////////////////////////////////////////
            success: function(data) {

              var mensaje = "<div class='alert alert-success'>Revisión registrada exitosamente</div>"
                 $('#mensaje').html(mensaje); 
                 $('#mensaje').removeAttr('style', 'display:none'); 
                 $('body,html').animate({scrollTop:0},500);               
                 $('#mensaje').fadeOut(2200);     
                 $('#form')[0].reset();     

                localStorage.setItem('id_revision',data.rev_id);              

            }
      })
      return false;
    });

//////////////////////////////////////////////////////////////////////////////
// Guardar formulario de si acepta o rechaza el control vista de CONTROLADO //
//////////////////////////////////////////////////////////////////////////////

      $('#acp_s,#acp_n').click(function() {   
      var nombre_ar = $(this).attr("name");
      // dame el value que tiene el name rsg_id y luego lo enviamos en formato json
      var id = $(this).siblings('input[name="rsg_id"]').val();

      $.ajax({
        headers: {'X-CSRF-TOKEN':$("meta[name='csrf-token']").attr("content") },
        type: 'PUT',
        url: $(this).attr('action'),    
        data: { 
          "nombre": nombre_ar, //la variable nombre se manda al controlador mientras que nombre_ar es la de JS
          "rsg_id": id
        },
       
            success: function() {

              var mensaje = "<div class='alert alert-success'>Control registrado exitosamente</div>"
                 $('#mensaje_con').html(mensaje); 
                 $('#mensaje_con').removeAttr('style', 'display:none'); 
                 $('body,html').animate({scrollTop:0},500);               
                 $('#mensaje_con').fadeOut(2200);             

            },
            error: function(){
              alert("LA FUNCION ES ERRONEA");
            }
      })
      return false;
    });

 });
