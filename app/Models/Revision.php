<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\tipoRevision;
use App\User;
use App\Models\Riesgo;
use App\Models\Unidad;
class Revision extends Model
{
    // Asociando a tabla "revision"
    protected $table = 'revision';

    //Diciendola a Laravel que la PK es "rev_id" , por defecto asume que existe la PK "id"
    protected $primaryKey = 'rev_id';

    // Formato de los timestamps
    // protected $dateFormat = 'D-m-y H:i:s';

    // Si se usa el metodo para insertar datos "Modelo::create($request->all());"
    protected $fillable = [
        
            'rev_trev',
            'rev_fechainicio',
            'rev_fechafinal',
            'rev_fecharealfinal',
            'rev_nombre',
            'rev_ggeneral',
            'rev_gerencia',
            'rev_coordinacion',
            'rev_responsable',
            'rev_objetivos',
            'rev_metas',
            'rev_alcances',
            'rev_relacion',
            'rev_fortalezas',
            'rev_oportunidades',
            'rev_debilidades',
            'rev_amenazas',
            'rev_usrcreacion',
            'rev_status',
            'rev_revid',

    ];

    public function tipo(){
        return $this->belongsTo(tipoRevision::class, 'rev_trev');
    }

    public function user(){
        return $this->belongsTo(User::class, 'rev_usrcreacion');
    }

    public function riesgos(){
        return $this->hasMany(Riesgo::class, 'rsg_revid', 'rev_id');
    }

    public function unidad(){
        return $this->hasMany(Unidad::class, 'uni_revid', 'rev_id');
    }


    //////////////////////////////////////
    // metodo para eliminar en cascada  //
    //////////////////////////////////////

    // protected static function boot(){

    //     parent::boot();
        
    //     static::deleting(function($revision){

    //         $revision->riesgos()->delete()
    //     });
    // }
    
    ///////////////////////////////////////////////////////
    // Para Mostrar el ripo de revision en el datatables //
    ///////////////////////////////////////////////////////

    public function getRevTrevAttribute($tipo){

       if ($tipo == 1) {
           return 'Proceso';
       }
       if ($tipo == 2) {
           return 'Proyecto';
       }
       if ($tipo == 3) {
           return 'Producto';
       }
       if ($tipo == 4) {
           return 'Servicio';
       }
       if ($tipo == 5) {
           return 'Plataforma';
       }        
        
    }

}
