<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Revision;

class Unidad extends Model
{
    protected $table = 'unidades';

    protected $primaryKey = 'uni_id';

    public function revision(){

    	return $this->belongsTo(Revision::class, 'uni_revid');

    }
}
