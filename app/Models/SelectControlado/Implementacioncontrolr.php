<?php

namespace App\Models\SelectControlado;

use Illuminate\Database\Eloquent\Model;
use App\Models\RiesgoControlado;

class Implementacioncontrolr extends Model
{
    protected $table = "implementacioncontrolr";

    protected $primaryKey = "icr_id";

     public function riesgosControlados(){
    	return $this->hasMany(RiesgosControlado::class, 'rcon_implementacion', 'icr_id');
    }
}
