<?php

namespace App\Models\SelectControlado;

use Illuminate\Database\Eloquent\Model;

class Ejecucioncontrolr extends Model
{
    protected $table = "ejecucioncontrolr";

    protected $primaryKey = "ecr_id";

    public function riesgosControlados(){
    	return $this->hasMany(RiesgosControlado::class, 'rcon_ejecucion', 'ecr_id');
    }
}
