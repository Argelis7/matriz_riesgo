<?php

namespace App\Models\SelectControlado;

use Illuminate\Database\Eloquent\Model;
use App\Models\RiesgoControlado;


class Documentacioncontrolr extends Model
{
    protected $table = "documentacioncontrolr";

    protected $primaryKey = "dcr_id";

    public function riesgosControlados(){
    	return $this->hasMany(RiesgosControlado::class, 'rcon_documentacion', 'dcr_id');
    }
}
