<?php

namespace App\Models\SelectControlado;

use Illuminate\Database\Eloquent\Model;
use App\Models\RiesgoControlado;
use App\Models\RiesgoTratado;

class TipoControlTratamiento extends Model
{
    protected $table = "tipocontrol_tratamiento";

    protected $primaryKey = "tcr_id";

    public function riesgosControlados(){
    	return $this->hasMany(RiesgoControlado::class, 'rcon_tipocontrol', 'tcr_id');
    }

    public function riesgosTratados(){
    	return $this->hasMany(RiesgoTratado::class, 'rtra_tipotra', 'tcr_id');
    }
}
