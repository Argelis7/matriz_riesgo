<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\SelectControlado\TipoControlTratamiento;
use App\Models\SelectTratado\Complejidadtrar;
use App\Models\SelectTratado\Costobeneficiotrar;
use App\Models\SelectTratado\Tiempotrar;
use App\Models\Riesgo;

class RiesgoTratado extends Model
{
    protected $table = "riesgotratado";

    protected $primaryKey = "rtra_id";

    public function riesgo(){
        return $this->belongsTo(Riesgo::class, 'rtra_rsgid');
    }

    public function tipoControlTratado(){
        return $this->belongsTo(TipoControlTratamiento::class, 'rtra_tipotra');
    }

    public function complejidad(){
        return $this->belongsTo(Complejidadtrar::class, 'rtra_comptra');
    }

    public function costo_beneficio(){
        return $this->belongsTo(Costobeneficiotrar::class, 'rtra_costobentra');
    }

    public function tiempo(){
        return $this->belongsTo(Tiempotrar::class, 'rtra_tiempotra');
    }

    public function getRtraNombreAttribute($nombre){
        return ucfirst($nombre);
    }    

    public function getRtraResponsableAttribute($responsable){
        return ucwords($responsable);
    }

    public function getRtraunidadAttribute($unidad){
        return ucwords($unidad);
    }
}
