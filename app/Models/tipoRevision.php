<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tipoRevision extends Model
{
    protected $table = 'tipo_revision';

    protected $primaryKey = 'trev_id';

    public function revisiones(){
    	return $this->hasMany(Revision::class, 'rev_trev', 'trev_id');
    }
    
}
