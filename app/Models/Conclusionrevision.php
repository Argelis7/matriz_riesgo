<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conclusionrevision extends Model
{
    protected $table = 'conclusionrevision';

    protected $primaryKey = 'cr_id';

     protected $fillable = [        
            'cr_id',
            'cr_revid',
            'cr_conclusion',
            'cr_usrcreacion',
            'cr_status',
            'cr_nombre',
            'cr_logro',
            'cr_desviaciones',
            'cr_puntosantencion',
            'cr_unidadresponsable'
    ];

}
