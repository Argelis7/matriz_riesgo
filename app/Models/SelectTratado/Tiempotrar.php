<?php

namespace App\Models\SelectTratado;

use Illuminate\Database\Eloquent\Model;
use App\Models\RiesgoTratado;

class Tiempotrar extends Model
{
    protected $table = "tiempotrar";

    protected $primaryKey = "ttr_id";

     public function riesgosTratados(){
    	return $this->hasMany(RiesgoTratado::class, 'rtra_comptra', 'ttr_id');
    }

}
