<?php

namespace App\Models\SelectTratado;

use Illuminate\Database\Eloquent\Model;
use App\Models\RiesgoTratado;

class Complejidadtrar extends Model
{
    protected $table = "complejidadtrar";

    protected $primaryKey = "ctr_id";

    public function riesgosTratados(){
    	return $this->hasMany(RiesgoTratado::class, 'rtra_comptra', 'ctr_id');
    }
}
