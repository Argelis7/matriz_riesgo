<?php

namespace App\Models\SelectTratado;

use Illuminate\Database\Eloquent\Model;
use App\Models\RiesgoTratado;

class Costobeneficiotrar extends Model
{
    protected $table ="costobeneficiotrar";

    protected $primaryKey ="cbtr_id";

     public function riesgosTratados(){
    	return $this->hasMany(RiesgoTratado::class, 'rtra_costobentra', 'cbtr_id');
    }
}
