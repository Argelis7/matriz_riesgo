<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Implementaciontrata extends Model
{
    protected $table = 'implementaciontrata';

    protected $primaryKey = 'imt_id';

     protected $fillable = [
        
            'imt_aptraid',
            'imt_responsable',
            'imt_fechaseguimiento',
            'imt_fechaobje',
            'imt_culminacion',
            'imt_porcavance',
            'imt_observaciones',        
            'imt_status'
           

    ];

}
