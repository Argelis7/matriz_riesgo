<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Aprobacion;


class EstadoRiesgo extends Model
{
	protected $table = 'estado_riesgo';

    protected $primaryKey = 'estrsg_id';

    public function aprobacion(){
    	return $this->hasMany(Aprobacion::class, 'aptra_ert', 'estrsg_id');
    }
}
