<?php

namespace App\Models\SelectInherente;

use Illuminate\Database\Eloquent\Model;
use App\Models\Riesgo;

class Friesgon2 extends Model
{
    protected $table = "friesgon2";

    protected $primaryKey = "frn2_id";

    public function riesgo(){
        return $this->hasMany(Riesgo::class, 'frn2_id', 'rsg_friesgon2');
    }
}
