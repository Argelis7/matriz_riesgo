<?php

namespace App\Models\SelectInherente;

use Illuminate\Database\Eloquent\Model;
use App\Models\Riesgo;

class Probabilidadri extends Model
{
    protected $table = "probabilidadri";

    protected $primaryKey ="proba_id";

    public function riesgo(){
        return $this->hasMany(Riesgo::class, 'proba_id', 'rsg_riprob');
    }
}
