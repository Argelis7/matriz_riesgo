<?php

namespace App\Models\SelectInherente;

use Illuminate\Database\Eloquent\Model;
use App\Models\Riesgo;

class Fuentesriesgos extends Model
{
    protected $table = "fuentes_riesgos";

    protected $primaryKey ="frsg_id";

    public function riesgo(){
        return $this->hasMany(Riesgo::class, 'frsg_id', 'rsg_fuente_riesgo');
    }
}
