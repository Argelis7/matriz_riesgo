<?php

namespace App\Models\SelectInherente;

use Illuminate\Database\Eloquent\Model;
use App\Models\Riesgo;

class Consecuenciari extends Model
{
    protected $table = "consecuenciari";

    protected $primaryKey = "cons_id";

    public function riesgo(){
        return $this->hasMany(Riesgo::class, 'cons_id', 'rsg_ricons');
    }
}
