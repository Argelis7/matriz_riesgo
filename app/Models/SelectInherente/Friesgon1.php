<?php

namespace App\Models\SelectInherente;

use Illuminate\Database\Eloquent\Model;
use App\Models\Riesgo;

class Friesgon1 extends Model
{
    protected $table = "friesgon1";

    protected $primaryKey = "frn1_id";

    public function riesgo(){
        return $this->hasMany(Riesgo::class, 'frn1_id', 'rsg_friesgon1');
    }
}
