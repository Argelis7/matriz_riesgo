<?php

namespace App\Models\SelectInherente;

use Illuminate\Database\Eloquent\Model;
use App\Models\Riesgo;


class Clasificacionriesgo extends Model
{
    protected $table = "clasificacion_riesgo";

    protected $primaryKey = "clasfrsg_id";

    public function riesgo(){
        return $this->hasMany(Riesgo::class, 'clasfrsg_id', 'rsg_clasf');
    }
}
