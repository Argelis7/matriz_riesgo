<?php

namespace App\Models\SelectInherente;

use Illuminate\Database\Eloquent\Model;
use App\Models\Riesgo;

class Estadoriesgo extends Model
{
    protected $table = "estado_riesgo";

    protected $primaryKey = "estrsg_id";

    public function riesgo(){
        return $this->hasMany(Riesgo::class, 'estrsg_id', 'rsg_estrsg');
    }
}
