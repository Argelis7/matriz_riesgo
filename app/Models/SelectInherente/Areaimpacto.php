<?php

namespace App\Models\SelectInherente;

use Illuminate\Database\Eloquent\Model;
use App\Models\Riesgo;

class Areaimpacto extends Model
{
    protected $table = "areaimpacto";

    protected $primaryKey = "aimpac_id";

    public function riesgo(){
        return $this->hasMany(Riesgo::class, 'aimpac_id', 'rsg_aimpact');
    }
}
