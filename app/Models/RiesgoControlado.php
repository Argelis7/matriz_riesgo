<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\SelectControlado\TipoControlTratamiento;
use App\Models\SelectControlado\Implementacioncontrolr;
use App\Models\SelectControlado\Documentacioncontrolr;
use App\Models\SelectControlado\Ejecucioncontrolr;
use App\Models\Riesgo;

class RiesgoControlado extends Model
{
    protected $table = "riesgocontrolado";

    protected $primaryKey = "rcon_id";

    public function riesgo(){
        return $this->belongsTo(Riesgo::class, 'rcon_rsgid');
    }

    public function tipoControlTratamiento(){
    	return $this->belongsTo(TipoControlTratamiento::class, 'rcon_tipocontrol');
    }   

    public function implementacion(){
    	return $this->belongsTo(Implementacioncontrolr::class, 'rcon_implementacion');
    }

    public function documentacion(){
    	return $this->belongsTo(Documentacioncontrolr::class, 'rcon_documentacion');
    }

    public function ejecucion(){
    	return $this->belongsTo(Ejecucioncontrolr::class, 'rcon_ejecucion');
    }   

    public function getRconResponsableAttribute(){
        return ucwords($this->attributes['rcon_responsable']);
    }

    public function getRconNombreAttribute($nombre){
        return ucfirst($nombre);
    }

    public function getRconUnidadAttribute($unidad){
        return ucfirst($unidad);
    }
}
