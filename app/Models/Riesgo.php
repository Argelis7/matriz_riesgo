<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Revision;
use App\Models\RiesgoControlado;
use App\Models\RiesgoTratado;
use App\Models\SelectInherente\Clasificacionriesgo;
use App\Models\SelectInherente\Estadoriesgo;
use App\Models\SelectInherente\Areaimpacto;
use App\Models\SelectInherente\Areaimpacton1;
use App\Models\SelectInherente\Fuentesriesgos;
use App\Models\SelectInherente\Friesgon1;
use App\Models\SelectInherente\Friesgon2;
use App\Models\SelectInherente\Consecuenciari;
use App\Models\SelectInherente\Probabilidadri;

class Riesgo extends Model
{
	protected $table = "riesgos";

    protected $primaryKey = "rsg_id";

    // protected $touched = ['revision'];

    ////////////////////////////////////////////////////////////////
    // estas son las variables que se actzalizaran del formulario //
    ////////////////////////////////////////////////////////////////

    protected $fillable = [
        
            'rsg_nombre',
            'rsg_clasf',
            'rsg_estrsg',
            'rsg_responsable',
            'rsg_undresp',
            'rsg_descripcion',
            'rsg_aimpact',
            'rsg_fuente_riesgo',
            'rsg_otrofuente',
            'rsg_aimpactn1',
            'rsg_friesgon1',
            'rsg_otroarea',
            'rsg_friesgon2',
            'rsg_ricons',
            'rsg_riprob',
            'rsg_acepri',
            'rsg_revid',
            'rsg_status',
            'rsg_severidad',

    ];

    public function revision(){
        return $this->belongsTo(Revision::class, 'rsg_revid');
    }
 
    public function riesgosControlados(){
        return $this->hasMany(RiesgoControlado::class, 'rcon_rsgid', 'rsg_id');
    }

    public function riesgosTratados(){
        return $this->hasMany(RiesgoTratado::class, 'rtra_rsgid', 'rsg_id');
    }

    public function clasificacionRiesgo(){
        return $this->belongsTo(Clasificacionriesgo::class, 'rsg_clasf');
    }

    public function estadoRiesgo(){
        return $this->belongsTo(Estadoriesgo::class, 'rsg_estrsg');
    }

    public function areaImpacto(){
        return $this->belongsTo(Areaimpacto::class, 'rsg_aimpact');
    }
    
    public function areaImpactoN1(){
        return $this->belongsTo(Areaimpacton1::class, 'rsg_aimpactn1');
    }  

    public function fuenteImpacto(){
        return $this->belongsTo(Fuentesriesgos::class, 'rsg_fuente_riesgo');
    }

    public function fuenteImpactoN1(){
        return $this->belongsTo(Friesgon1::class, 'rsg_friesgon1');
    } 

    public function fuenteImpactoN2(){
        return $this->belongsTo(Friesgon2::class, 'rsg_friesgon2');
    } 

    public function consecuenciaRiesgo(){
        return $this->belongsTo(Consecuenciari::class, 'rsg_ricons');
    }  

    public function probabilidadRiesgo(){
        return $this->belongsTo(Probabilidadri::class, 'rsg_riprob');
    }  

}
