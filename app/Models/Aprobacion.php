<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\EstadoRiesgo;


class Aprobacion extends Model
{
    protected $table = 'aprobaciontratamiento';

    protected $primaryKey = 'aptra_id';

     protected $fillable = [        
            'aptra_id',
            'aptra_responsableapro',
            'aptra_fecharespuesta',
            'aptra_ert',
            'aptra_observaciones',
            'aptra_nombretra',
            'aptra_rtraid',
            'aptra_rsgid'
    ];

     public function estadoRiesgo(){
    	return $this->belongsTo(EstadoRiesgo::class, 'aptra_ert');
    }  
}
