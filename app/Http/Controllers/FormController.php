<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    
    public function showForm()
    {
        return view('formularios.inicio');
    }

   
    public function store(Request $request)
    {
        return $request->validate([

            'usuario' => 'required',
            'contraseña' => 'required',
            
        ]);
    }

    
}
