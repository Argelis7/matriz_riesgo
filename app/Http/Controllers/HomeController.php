<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Revision;
use App\Models\Riesgo;
use App\Models\RiesgoControlado;
use App\Models\RiesgoTratado;
use App\Models\Implementaciontrata;
use App\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('formularios.inicio');
    }

     public function vistaInicio()
    {   
        // $cantidadRiesgo = Riesgo::($request->all());
         $usuario = Auth::user()->id;

        $cantidadRiesgo = Riesgo::where('rsg_usrcreacion', $usuario)->count();
        $cantidadRevision = Revision::where('rev_usrcreacion', $usuario)->count();
        $cantidadControlado = RiesgoControlado::where('rcon_usrcreacion', $usuario)->count();
        $cantidadTratado = RiesgoTratado::where('rtra_usrcreacion', $usuario)->count();              
        $cantidadImplementaTra = Implementaciontrata::where('imt_usrcreacion', $usuario)->count();
        $fecha =  Carbon::now()->format('d-m-Y');            
        $hora =  Carbon::now()->format('h:m a');            
        
        return view('formularios.index', compact('cantidadRiesgo', 'cantidadRevision', 'cantidadControlado', 'cantidadTratado', 'cantidadImplementaTra','fecha','hora'));        
    }
}
