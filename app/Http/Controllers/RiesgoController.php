<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Tablas Formulario
use App\Models\RiesgoControlado;
use App\Models\RiesgoTratado;
use App\Models\Riesgo;
// Selects Riesgo Controlado
use App\Models\SelectControlado\TipoControlTratamiento;
use App\Models\SelectControlado\Implementacioncontrolr;
use App\Models\SelectControlado\Documentacioncontrolr;
use App\Models\SelectControlado\Ejecucioncontrolr;
// Selects Riesgo Tratado
use App\Models\SelectTratado\Complejidadtrar;
use App\Models\SelectTratado\Costobeneficiotrar;
use App\Models\SelectTratado\Tiempotrar;
// Selects Riesgo Inherente (tabla: Riesgo)
use App\Models\SelectInherente\Areaimpacto;
use App\Models\SelectInherente\Areaimpacton1;
use App\Models\SelectInherente\Fuentesriesgos;
use App\Models\SelectInherente\Friesgon1;
use App\Models\SelectInherente\Friesgon2;
use App\Models\SelectInherente\Consecuenciari;
use App\Models\SelectInherente\Probabilidadri;
use App\Models\SelectInherente\Clasificacionriesgo;
use App\Models\SelectInherente\Estadoriesgo;

use App\Models\Revision;

use App\Models\Aprobacion;
use App\Models\Implementaciontrata;

use Validator;

class RiesgoController extends Controller
{
     public function selectAreaImpactoN1($id){
        return Areaimpacton1::where('aimpacn1_aimpacid', $id)->get();
    }

    public function selectFuenteImpactoN1($id){
        return Friesgon1::where('frn1_frsgid', $id)->get();
    }

    public function selectFuenteImpactoN2($id){
        return Friesgon2::where('frn2_frn1id', $id)->get();
    }
    
    // Retorna la vista de registrar riesgo y manda las variables de los selects
    public function riesgo($idRiesgo)
    {
      // Select formulario "Riesgo Controlado"
        $tipoTratamiento = TipoControlTratamiento::all();//tambien para triesgo tratado
        $implementacionControlado = Implementacioncontrolr::all();
        $documentacionControlado = Documentacioncontrolr::all();
        $ejecucionControlado = Ejecucioncontrolr::all();
        // Select formulario "Riesgo Tratado"
        $complejidadTratado = Complejidadtrar::all();
        $costobeneficioTratado = Costobeneficiotrar::all();
        $tiempoTratado = Tiempotrar::all();
        // Select formulario "Riesgo Inherente"
        $areaImpacto = Areaimpacto::all();

        // $areaImpactoN1 = Areaimpacton1::all();
        // $areaImpacto_id = Input::get()
        // $areaImpactoN1 = Areaimpacton1::where('aimpacn1_aimpacid', '=', 1)->get();
        
        $fuenteImpacto = Fuentesriesgos::all();
        $fuenteImpactoN1 = Friesgon1::all();
        $fuenteImpactoN2 = Friesgon2::all();
        $consecuencia = Consecuenciari::orderBy('cons_id', 'ASC')->get();
        $probabilidad = Probabilidadri::orderBy('proba_id', 'ASC')->get();
        $clasificacionRiesgo = Clasificacionriesgo::all();
        $estadoRiesgo = Estadoriesgo::all();

        // $idRiesgo = Riesgo::where('rsg_revid', $idRevision)->latest()->get();

        // if ( isset($idRiesgo->rsg_id)) {
        //     return 'si';   

        // }else{
        //     return 'no';
        // }

    
        
        $riesgo = Riesgo::find($idRiesgo);  
        // dd($riesgo);    

        $riesgosControlados = $this->riesgosControlados($idRiesgo);
        // $riesgosControlados = RiesgoControlado::all();
        $riesgosTratados = $this->riesgosTratados($idRiesgo);   
        // $riesgosTratados = RiesgoTratado::all();         
        // 
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // esto es para obtener LA SUMA TOTAL de los tipo de control tanto preventivo como correctivo de la COLUMNA SUMA CONTROLADO //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

         $sumaControlCorrectivo = $this->riesgosControladosTipos($idRiesgo, "correctivo");
         $sumaControlPreventivo = $this->riesgosControladosTipos($idRiesgo, "preventivo");

         ////////////////////////////////////////////////////////////////////
         // para saber la cantidad de controles CORRECTIVOS y PREVENTIVOS  //
         ////////////////////////////////////////////////////////////////////
         
         $CantidadControlCorrectivo = $this->riesgosControladosCantidad($idRiesgo, "correctivo");
         $CantidadControlPreventivo = $this->riesgosControladosCantidad($idRiesgo, "preventivo");

         ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         // esto es para obtener LA SUMA TOTAL de los tipo de tratamiento tanto preventivo como correctivo de la COLUMNA SUMA TRATAMIENTO //
         ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

         $sumaTratamientoCorrectivo = $this->riesgosTratadosTipos($idRiesgo, "correctivo");
         $sumaTratamientoPreventivo = $this->riesgosTratadosTipos($idRiesgo, "preventivo");

         //////////////////////////////////////////////////////////////////////
         // para saber la cantidad de tratamientos CORRECTIVOS y PREVENTIVOS //
         //////////////////////////////////////////////////////////////////////

         $CantidadTratamientoCorrectivo = $this->riesgosTratadosCantidad($idRiesgo, "correctivo");
         $CantidadTratamientoPreventivo = $this->riesgosTratadosCantidad($idRiesgo, "preventivo");

        $estado = true;

        $aprobacion = Aprobacion::all();
        $implementacion = Implementaciontrata::all();



        return view('formularios.riesgo.RegistrarRiesgo',  
            compact('tipoTratamiento', 'implementacionControlado', 'documentacionControlado', 'ejecucionControlado', 'complejidadTratado', 'costobeneficioTratado', 'tiempoTratado', 'areaImpacto', 'areaImpactoN1', 'fuenteImpacto', 'fuenteImpactoN1', 'fuenteImpactoN2', 'consecuencia', 'probabilidad', 'clasificacionRiesgo', 'estadoRiesgo', 'riesgosControlados', 'riesgosTratados', 'estado', 'riesgo', 'sumaControlCorrectivo', 'sumaControlPreventivo', 'CantidadControlCorrectivo', 'CantidadControlPreventivo', 'sumaTratamientoCorrectivo', 'sumaTratamientoPreventivo', 'CantidadTratamientoCorrectivo', 'CantidadTratamientoPreventivo', 'aprobacion', 'implementacion'));
     
    }
    
    // Guarda formulario segun el "submit" que se pulse
    public function registrarRiesgo(Request $request)
    {                    
       $tipo_riesgo = $request->tipo_riesgo;

       
        // Si se guarda formulario de "Riesgo Inherente"
        if ($tipo_riesgo == "RiesgoInherente") 
        {
        
             $id_inherente = $this->guardarInherente($request);             

              return response()->json([
            'id' =>  $id_inherente,           
            'mensaje' => "Riesgo Inherente registrado exitosamente. !!!"

         ]);
            // return back()
            //    ->with('mensaje', 'Riesgo inherente registrado exitosamente.')->with('id_inherente', $id_inherente); 
        }

        // Si se guarda formulario de "Riesgo controlado"
        if ($tipo_riesgo == "RiesgoControlado") 
        {             
            return $this->guardarControlado($request);                    
        }

        // Si se guarda formulario de "Riesgo tratado"
        if ($tipo_riesgo == "RiesgoTratado") 
        {
            return $this->guardarTratado($request);        
        }

        // Si se guarda formulrio de "Aprobacion";
        if ($tipo_riesgo == "Aprobacion") 
        {
            return $this->guardarAprobacion($request);        
        }
        
        // Si se guarda formulrio de "Implementacion";
        if ($tipo_riesgo == "Implementacion") 
        {
            return $this->guardarImplementacion($request);        
        }
    }

    public function actualizar(Request $request, $id){

        if ($request->nombre == "aceptaControlado") {
            $this->aceptaControlado($request);
        }
        if ($request->nombre == "rechazaControlado") {
            $this->rechazaControlado($request);
        }
    }

     protected function aceptaControlado(Request $request){
        $actualizar = Riesgo::findorfail($request->rsg_id);

        $actualizar->rsg_acepcon = 1;

        $actualizar->save();
    }

    protected function rechazaControlado(Request $request){
        $actualizar = Riesgo::findorfail($request->rsg_id);

        $actualizar->rsg_acepcon = 0;

        $actualizar->save();
    }

    protected function guardarInherente(Request $request)
    {

        // validas los INPUT del formulario con sus respectivos name
        // $request->validate([

        //     'rsg_nombre' => 'string|max:200|required',
        //     'rsg_clasf' => 'integer|required',
        //     'rsg_estrsg' => 'integer|required',
        //     'rsg_responsable' => 'string|max:200|required',
        //     'rsg_undresp' => 'string|max:60|required',
        //     'rsg_descripcion' => 'string|max:200|required',
        //     'rsg_aimpact' => 'integer|required',
        //     'rsg_fuente_riesgo' => 'integer|required',
        //     'rsg_aimpactn1' => 'integer|required',
        //     'rsg_friesgon1' => 'integer|required',
        //     'rsg_friesgon2' => 'integer|required',
        //     'rsg_ricons' => 'integer|required',
        //     'rsg_riprob' => 'integer|required',
        //     'rsg_acepri' => 'integer|required',
        //     
        //     'rsg_otrofuente' => 'string|max:200|required',
        //     'rsg_otroarea' => 'string|max:200|required',
        // ]);
            
        // si los campos existen y tienen valor los valida
        // if($request->filled('rsg_otrofuente')){
        //     $request->validate(['rsg_otrofuente' => 'string|max:200|required']);
        // }
        // if($request->filled('rsg_otroarea')){
        //     $request->validate(['rsg_otroarea' => 'string|max:200|required']);
        // }
            $riesgoInherente = new Riesgo();


            $riesgoInherente->rsg_nombre = $request->rsg_nombre;
            $riesgoInherente->rsg_clasf = $request->rsg_clasf;
            $riesgoInherente->rsg_estrsg = $request->rsg_estrsg;
            $riesgoInherente->rsg_responsable = $request->rsg_responsable;
            $riesgoInherente->rsg_undresp = $request->rsg_undresp;
            $riesgoInherente->rsg_descripcion = $request->rsg_descripcion;
            $riesgoInherente->rsg_aimpact = $request->rsg_aimpact;
            $riesgoInherente->rsg_fuente_riesgo = $request->rsg_fuente_riesgo;
            $riesgoInherente->rsg_otrofuente = $request->rsg_otrofuente;
            $riesgoInherente->rsg_aimpactn1 = $request->rsg_aimpactn1;
            $riesgoInherente->rsg_friesgon1 = $request->rsg_friesgon1;
            $riesgoInherente->rsg_otroarea = $request->rsg_otroarea;
            $riesgoInherente->rsg_friesgon2 = $request->rsg_friesgon2;
            $riesgoInherente->rsg_ricons = $request->rsg_ricons;
            $riesgoInherente->rsg_riprob = $request->rsg_riprob;
            $riesgoInherente->rsg_acepri = $request->elegir;
            $riesgoInherente->rsg_severidad = $request->rsg_severidad;
            $riesgoInherente->rsg_revid = $request->rsg_revid;


                // $riesgoInherente->rsg_revid = 10;
        
            $riesgoInherente->rsg_status = 1;
            $riesgoInherente->rsg_usrcreacion = $request->user()->id;

             // == Prueba AMBOS FUNCIONAN IGUAL ==
                // $riesgoInherente->rsg_revid = Revision::find(15)->rev_id;
                    
                // $revision = Revision::find(18);

                // $riesgoInherente->revision()->associate($revision);
               
            // == FIN PRUEBA ==

            $riesgoInherente->save();  

            return $riesgoInherente->rsg_id;
        }

    protected function guardarControlado(Request $request)
    {        
        // return $request->all();
        // validas los INPUT del formulario con sus respectivos name
        $request->validate([

                // 'rcon_rsgid' => 'integer|required',
                'rcon_nombre' => 'string|max:300|required',
                'rcon_responsable' => 'string|max:50|required',
                'rcon_unidad' => 'string|max:100|required',
                // 'rcon_aceprc' => 'integer|required',
                'rcon_tipocontrol' => 'integer|required',
                'rcon_implementacion' => 'integer|required',
                'rcon_documentacion' => 'integer|required',
                'rcon_ejecucion' => 'integer|required',
                'rcon_efectividad' => 'string|required',                
                'rcon_suma' => 'integer|required'
        ]);

        
        $riesgoControlado = new RiesgoControlado(); 

        $riesgoControlado->rcon_nombre = $request->rcon_nombre;
        $riesgoControlado->rcon_responsable = $request->rcon_responsable;
        $riesgoControlado->rcon_unidad = $request->rcon_unidad;
        $riesgoControlado->rcon_tipocontrol = $request->rcon_tipocontrol;
        $riesgoControlado->rcon_implementacion = $request->rcon_implementacion;
        $riesgoControlado->rcon_documentacion = $request->rcon_documentacion;
        $riesgoControlado->rcon_ejecucion = $request->rcon_ejecucion;
        $riesgoControlado->rcon_efectividad = $request->rcon_efectividad;        
        $riesgoControlado->rcon_suma = $request->rcon_suma;
        $riesgoControlado->rcon_rsgid = $request->rcon_rsgid;    
            
        $riesgoControlado->rcon_status = 1;
        $riesgoControlado->rcon_usrcreacion = $request->user()->id;

        $riesgoControlado->save();

         //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // esto es para obtener LA SUMA TOTAL de los tipo de control tanto preventivo como correctivo de la COLUMNA SUMA CONTROLADO //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

         $sumaControlCorrectivo = $this->riesgosControladosTipos($request->rcon_rsgid, "correctivo");
         $sumaControlPreventivo = $this->riesgosControladosTipos($request->rcon_rsgid, "preventivo");

         ////////////////////////////////////////////////////////////////////
         // para saber la cantidad de controles CORRECTIVOS y PREVENTIVOS  //
         ////////////////////////////////////////////////////////////////////
         
         $CantidadControlCorrectivo = $this->riesgosControladosCantidad($request->rcon_rsgid, "correctivo");
         $CantidadControlPreventivo = $this->riesgosControladosCantidad($request->rcon_rsgid, "preventivo");


        // dd($request->all());
        // return $riesgoControlado->toJson();
           return response()->json([
            'id' => $riesgoControlado->rcon_id,
            'creacion' => $riesgoControlado->created_at,
            'nombre' => $riesgoControlado->rcon_nombre,
            'responsable' => $riesgoControlado->rcon_responsable,
            'unidad' => $riesgoControlado->rcon_unidad,
            'tipo' => $riesgoControlado->tipoControlTratamiento->tcr_descripcion,
            'implementacion' => $riesgoControlado->implementacion->icr_descripcion,
            'documentacion' => $riesgoControlado->documentacion->dcr_descripcion,
            'ejecucion' => $riesgoControlado->ejecucion->ecr_descripcion,
            'efectividad' => $riesgoControlado->rcon_efectividad,
            'sumaControlCorrectivo' => $sumaControlCorrectivo,
            'sumaControlPreventivo' => $sumaControlPreventivo,
            'CantidadControlCorrectivo' => $CantidadControlCorrectivo,
            'CantidadControlPreventivo' => $CantidadControlPreventivo,
            'mensaje' => "Riesgo controlado registrado exitosamente. !!!"

         ]);

    }

    protected function guardarTratado(Request $request)
    {
      // validas los INPUT del formulario con sus respectivos name
        $request->validate([

        // 'rtra_rsgid' => 'integer|required',
            'rtra_nombre' => 'string|max:100|required',
            'rtra_responsable' => 'string|max:50|required',
            'rtra_unidad' => 'string|max:100|required',
            'rtra_tipotra' => 'integer|required',
            'rtra_comptra' => 'integer|required',
            'rtra_costobentra' => 'integer|required',
            'rtra_tiempotra' => 'integer|required',
            'rtra_efectividad' => 'string|required',

        ]);


        $riesgoTratado = new RiesgoTratado();
            
        $riesgoTratado->rtra_nombre = $request->rtra_nombre;
        $riesgoTratado->rtra_responsable = $request->rtra_responsable;
        $riesgoTratado->rtra_unidad = $request->rtra_unidad;
        $riesgoTratado->rtra_tipotra = $request->rtra_tipotra;
        $riesgoTratado->rtra_comptra = $request->rtra_comptra;//mal escrito ¬¬!impementacion
        $riesgoTratado->rtra_costobentra = $request->rtra_costobentra;
        $riesgoTratado->rtra_tiempotra = $request->rtra_tiempotra;
        $riesgoTratado->rtra_suma = $request->sumaTratado;        
        $riesgoTratado->rtra_rsgid = $request->rtra_rsgid;       
        $riesgoTratado->rtra_efectividad = $request->rtra_efectividad;       
        $riesgoTratado->rtra_status = 1;
        $riesgoTratado->rtra_usrcreacion = $request->user()->id;

        // $riesgoTratado->rtra_rsgid = $request->rsg_id; para asociar el tratamiento a el riesgo inherente 
        $riesgoTratado->save();

         ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         // esto es para obtener LA SUMA TOTAL de los tipo de tratamiento tanto preventivo como correctivo de la COLUMNA SUMA TRATAMIENTO //
         ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

         $sumaTratamientoCorrectivo = $this->riesgosTratadosTipos($request->rtra_rsgid, "correctivo");
         $sumaTratamientoPreventivo = $this->riesgosTratadosTipos($request->rtra_rsgid, "preventivo");

         //////////////////////////////////////////////////////////////////////
         // para saber la cantidad de tratamientos CORRECTIVOS y PREVENTIVOS //
         //////////////////////////////////////////////////////////////////////

         $CantidadTratamientoCorrectivo = $this->riesgosTratadosCantidad($request->rtra_rsgid, "correctivo");
         $CantidadTratamientoPreventivo = $this->riesgosTratadosCantidad($request->rtra_rsgid, "preventivo");

         return response()->json([
            'id' => $riesgoTratado->rtra_id,
            'creacion' => $riesgoTratado->created_at,            
            'nombre' => $riesgoTratado->rtra_nombre,
            'responsable' => $riesgoTratado->rtra_responsable,
            'unidad' => $riesgoTratado->rtra_unidad,
            'tipo' => $riesgoTratado->tipoControlTratado->tcr_descripcion,
            'complejidad' => $riesgoTratado->complejidad->ctr_descripcion,
            'costoben' => $riesgoTratado->costo_beneficio->cbtr_descripcion,
            'tiempo' => $riesgoTratado->tiempo->ttr_descripcion,
            'efectividad' => $riesgoTratado->rtra_efectividad,
            'sumaTratamientoCorrectivo' => $sumaTratamientoCorrectivo,
            'sumaTratamientoPreventivo' => $sumaTratamientoPreventivo,
            'CantidadTratamientoCorrectivo' => $CantidadTratamientoCorrectivo,
            'CantidadTratamientoPreventivo' => $CantidadTratamientoPreventivo,
            'mensaje' => "Riesgo Tratado registrado exitosamente. !!!"

         ]);
    }

     protected function guardarAprobacion(Request $request)
    {

      // validas los INPUT del formulario con sus respectivos name

        $request->validate([   

            'aptra_rtraid' => 'required|unique:aprobaciontratamiento'

            // 'aptra_nombretra' => 'string|max:100|required',
            // 'aptra_responsableapro' => 'string|max:60|required',
            // 'aptra_fecharespuesta' => 'date|required',
            // 'aptra_ert' => 'integer|required',
            // 'aptra_observaciones' => 'string|max:200|required',           

        ]);




        $aprobacion = new Aprobacion();
            
        $aprobacion->aptra_rtraid = $request->aptra_rtraid;
        $aprobacion->aptra_rsgid = $request->aptra_rsgid;
        $aprobacion->aptra_nombretra = $request->aptra_nombretra;
        $aprobacion->aptra_responsableapro = $request->aptra_responsableapro;
        $aprobacion->aptra_fecharespuesta = $request->aptra_fecharespuesta;
        $aprobacion->aptra_ert = $request->aptra_ert;
        $aprobacion->aptra_observaciones = $request->aptra_observaciones;
        
        $aprobacion->save();        

         return response()->json([                    
            'mensaje' => "Aprobación registrada exitosamente. !!!"
         ]);
    }


    public function eliminarRiesgo(Request $request)
    {        
         if ($request->tipo_riesgo == "eliminarControl") 
        {          
           return $this->eliminarRiesgoControlado($request);  
        }

          if ($request->tipo_riesgo == "eliminarTratamiento") 
        {
             return $this->eliminarRiesgoTratado($request);
        }
        

          if ($request->tipo_riesgo == "eliminarRiesgo") 
        {
             return $this->eliminarRiesgoInherente($request);
        }

        if ($request->tipo_riesgo == "eliminarAprobacion") 
        {
             return $this->eliminarAprobacion($request);
        }

        if ($request->tipo_riesgo == "eliminarImplementacion") 
        {
             return $this->eliminarImplementacion($request);
        }
    }

    public function eliminarRiesgoControlado(Request $request){

        RiesgoControlado::findorfail($request->id)->delete();
        

          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // esto es para obtener LA SUMA TOTAL de los tipo de control tanto preventivo como correctivo de la COLUMNA SUMA CONTROLADO //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

         $sumaControlCorrectivo = $this->riesgosControladosTipos($request->idRiesgo, "correctivo");
         $sumaControlPreventivo = $this->riesgosControladosTipos($request->idRiesgo, "preventivo");

         ////////////////////////////////////////////////////////////////////
         // para saber la cantidad de controles CORRECTIVOS y PREVENTIVOS  //
         ////////////////////////////////////////////////////////////////////
         
         $CantidadControlCorrectivo = $this->riesgosControladosCantidad($request->idRiesgo, "correctivo");
         $CantidadControlPreventivo = $this->riesgosControladosCantidad($request->idRiesgo, "preventivo");

         ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         // esto es para obtener LA SUMA TOTAL de los tipo de tratamiento tanto preventivo como correctivo de la COLUMNA SUMA TRATAMIENTO //
         ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

         $sumaTratamientoCorrectivo = $this->riesgosTratadosTipos($request->idRiesgo, "correctivo");
         $sumaTratamientoPreventivo = $this->riesgosTratadosTipos($request->idRiesgo, "preventivo");

         //////////////////////////////////////////////////////////////////////
         // para saber la cantidad de tratamientos CORRECTIVOS y PREVENTIVOS //
         //////////////////////////////////////////////////////////////////////

         $CantidadTratamientoCorrectivo = $this->riesgosTratadosCantidad($request->idRiesgo, "correctivo");
         $CantidadTratamientoPreventivo = $this->riesgosTratadosCantidad($request->idRiesgo, "preventivo");

       

         return response()->json([
            'sumaTratamientoCorrectivo' => $sumaTratamientoCorrectivo,
            'sumaTratamientoPreventivo' => $sumaTratamientoPreventivo,
            'CantidadTratamientoCorrectivo' => $CantidadTratamientoCorrectivo,
            'CantidadTratamientoPreventivo' => $CantidadTratamientoPreventivo,
            'sumaControlCorrectivo' => $sumaControlCorrectivo,
            'sumaControlPreventivo' => $sumaControlPreventivo,            
            'CantidadControlCorrectivo' => $CantidadControlCorrectivo,
            'CantidadControlPreventivo' => $CantidadControlPreventivo,
            'mensaje' => 'Riesgo Controlado Eliminado'
         ]);
     }
    
    public function eliminarRiesgoTratado(Request $request){

        RiesgoTratado::findorfail($request->id)->delete();



          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // esto es para obtener LA SUMA TOTAL de los tipo de control tanto preventivo como correctivo de la COLUMNA SUMA CONTROLADO //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

         $sumaControlCorrectivo = $this->riesgosControladosTipos($request->idRiesgo, "correctivo");
         $sumaControlPreventivo = $this->riesgosControladosTipos($request->idRiesgo, "preventivo");

         ////////////////////////////////////////////////////////////////////
         // para saber la cantidad de controles CORRECTIVOS y PREVENTIVOS  //
         ////////////////////////////////////////////////////////////////////
         
         $CantidadControlCorrectivo = $this->riesgosControladosCantidad($request->idRiesgo, "correctivo");
         $CantidadControlPreventivo = $this->riesgosControladosCantidad($request->idRiesgo, "preventivo");

         ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         // esto es para obtener LA SUMA TOTAL de los tipo de tratamiento tanto preventivo como correctivo de la COLUMNA SUMA TRATAMIENTO //
         ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

         $sumaTratamientoCorrectivo = $this->riesgosTratadosTipos($request->idRiesgo, "correctivo");
         $sumaTratamientoPreventivo = $this->riesgosTratadosTipos($request->idRiesgo, "preventivo");

         //////////////////////////////////////////////////////////////////////
         // para saber la cantidad de tratamientos CORRECTIVOS y PREVENTIVOS //
         //////////////////////////////////////////////////////////////////////

         $CantidadTratamientoCorrectivo = $this->riesgosTratadosCantidad($request->idRiesgo, "correctivo");
         $CantidadTratamientoPreventivo = $this->riesgosTratadosCantidad($request->idRiesgo, "preventivo");

         return response()->json([
            'sumaControlCorrectivo' => $sumaControlCorrectivo,
            'sumaControlPreventivo' => $sumaControlPreventivo,
            'CantidadControlCorrectivo' => $CantidadControlCorrectivo,
            'CantidadControlPreventivo' => $CantidadControlPreventivo,
            'sumaTratamientoCorrectivo' => $sumaTratamientoCorrectivo,
            'sumaTratamientoPreventivo' => $sumaTratamientoPreventivo,
            'CantidadTratamientoCorrectivo' => $CantidadTratamientoCorrectivo,
            'CantidadTratamientoPreventivo' => $CantidadTratamientoPreventivo,
            'mensaje' => 'Riesgo Tratado Eliminado'
         ]);

    }

    public function eliminarRiesgoInherente(Request $request){

        Riesgo::findorfail($request->id)->delete();

         return response()->json([
            'mensaje' => 'Riesgos Eliminado Correctamente..!!'
         ]);

    }

    private function riesgosControlados($idRiesgo){
        // return $riesgosControlados = RiesgoControlado::all();
        return $riesgosControlados = RiesgoControlado::where("rcon_rsgid", $idRiesgo)->get();
    }

    private function riesgosTratados($idRiesgo){
        return $riesgosTratados = RiesgoTratado::where("rtra_rsgid", $idRiesgo)->get();
    }

    public function verRiesgoInherente($idRiesgo){

         // Select formulario "Riesgo Controlado"
        $tipoTratamiento = TipoControlTratamiento::all();//tambien para triesgo tratado
        $implementacionControlado = Implementacioncontrolr::all();
        $documentacionControlado = Documentacioncontrolr::all();
        $ejecucionControlado = Ejecucioncontrolr::all();
        // Select formulario "Riesgo Tratado"
        $complejidadTratado = Complejidadtrar::all();
        $costobeneficioTratado = Costobeneficiotrar::all();
        $tiempoTratado = Tiempotrar::all();
        // Select formulario "Riesgo Inherente"
        $areaImpacto = Areaimpacto::all();


        // $areaImpactoN1 = Areaimpacton1::all();
        // $areaImpacto_id = Input::get()
        // $areaImpactoN1 = Areaimpacton1::where('aimpacn1_aimpacid', '=', 1)->get();
        
        $fuenteImpacto = Fuentesriesgos::all();
        $fuenteImpactoN1 = Friesgon1::all();
        $fuenteImpactoN2 = Friesgon2::all();
        $consecuencia = Consecuenciari::orderBy('cons_id', 'ASC')->get();
        $probabilidad = Probabilidadri::orderBy('proba_id', 'ASC')->get();
        $clasificacionRiesgo = Clasificacionriesgo::all();
        $estadoRiesgo = Estadoriesgo::all();

        // $idRiesgo = Riesgo::where('rsg_revid', $idRevision)->latest()->get();

        // if ( isset($idRiesgo->rsg_id)) {
        //     return 'si';   

        // }else{
        //     return 'no';
        // }

        $aprobacion = Aprobacion::where("aptra_rsgid", $idRiesgo)->get();
        $implementacion = Implementaciontrata::where("imt_rsgid", $idRiesgo)->get();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // esto es para obtener LA SUMA TOTAL de los tipo de control tanto preventivo como correctivo de la COLUMNA SUMA CONTROLADO //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

         $sumaControlCorrectivo = $this->riesgosControladosTipos($idRiesgo, "correctivo");
         $sumaControlPreventivo = $this->riesgosControladosTipos($idRiesgo, "preventivo");

         ////////////////////////////////////////////////////////////////////
         // para saber la cantidad de controles CORRECTIVOS y PREVENTIVOS  //
         ////////////////////////////////////////////////////////////////////
         
         $CantidadControlCorrectivo = $this->riesgosControladosCantidad($idRiesgo, "correctivo");
         $CantidadControlPreventivo = $this->riesgosControladosCantidad($idRiesgo, "preventivo");

         ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         // esto es para obtener LA SUMA TOTAL de los tipo de tratamiento tanto preventivo como correctivo de la COLUMNA SUMA TRATAMIENTO //
         ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

         $sumaTratamientoCorrectivo = $this->riesgosTratadosTipos($idRiesgo, "correctivo");
         $sumaTratamientoPreventivo = $this->riesgosTratadosTipos($idRiesgo, "preventivo");

         //////////////////////////////////////////////////////////////////////
         // para saber la cantidad de tratamientos CORRECTIVOS y PREVENTIVOS //
         //////////////////////////////////////////////////////////////////////

         $CantidadTratamientoCorrectivo = $this->riesgosTratadosCantidad($idRiesgo, "correctivo");
         $CantidadTratamientoPreventivo = $this->riesgosTratadosCantidad($idRiesgo, "preventivo");



        $riesgosControlados = $this->riesgosControlados($idRiesgo);
        // $riesgosControlados = RiesgoControlado::all();
        $riesgosTratados = $this->riesgosTratados($idRiesgo);   
        // $riesgosTratados = RiesgoTratado::all();   
        
         $riesgo = Riesgo::findOrFail($idRiesgo);



        return view('formularios.riesgo.RegistrarRiesgo',  
            compact('tipoTratamiento', 'implementacionControlado', 'documentacionControlado', 'ejecucionControlado', 'complejidadTratado', 'costobeneficioTratado', 'tiempoTratado', 'areaImpacto', 'areaImpactoN1', 'fuenteImpacto', 'fuenteImpactoN1', 'fuenteImpactoN2', 'consecuencia', 'probabilidad', 'clasificacionRiesgo', 'estadoRiesgo', 'riesgosControlados', 'riesgosTratados', 'riesgo', 'sumaControlCorrectivo', 'sumaControlPreventivo', 'CantidadControlCorrectivo', 'CantidadControlPreventivo', 'sumaTratamientoCorrectivo', 'sumaTratamientoPreventivo', 'CantidadTratamientoCorrectivo', 'CantidadTratamientoPreventivo','aprobacion', 'implementacion'));
       
    }

    public function actualizarRiesgo(Request $request, $id){

        $riesgo = Riesgo::findOrFail($id)->update($request->all());
        // $riesgo->touch();

        return back()->with('mensaje', 'Su riesgo fue actualizado Exitosamente..!!');
    }

    /////////////////////////////////////
    // para los calculos de controles  //
    /////////////////////////////////////

    public function riesgosControladosTipos($idRiesgo, $tipo){
        $suma = 0;
        if ($tipo == "correctivo") {
            $tipoControl = RiesgoControlado::where("rcon_rsgid", $idRiesgo)->where("rcon_tipocontrol", 2)->get();
        }
        if ($tipo == "preventivo") {
            $tipoControl = RiesgoControlado::where("rcon_rsgid", $idRiesgo)->where("rcon_tipocontrol", 1)->get();
        }

        foreach ($tipoControl as $tp) {
            $suma = $suma + $tp->rcon_suma;
        }

         return $suma;

    }

     public function riesgosControladosCantidad($idRiesgo, $tipo){
    
    ////////////////////////////////////////////////////////////////////////////////////////
    // se coloca count para saber la cantidad de registros asociadas al riesgo controlado //
    ////////////////////////////////////////////////////////////////////////////////////////
        if ($tipo == "correctivo") {
            $cantidad = RiesgoControlado::where("rcon_rsgid", $idRiesgo)->where("rcon_tipocontrol", 2)->count();
        }
        if ($tipo == "preventivo") {
            $cantidad = RiesgoControlado::where("rcon_rsgid", $idRiesgo)->where("rcon_tipocontrol", 1)->count();
        }       

         return $cantidad;

    }

    ///////////////////////////////////////
    // para los calculos del tratamiento //
    ///////////////////////////////////////

    public function riesgosTratadosTipos($idRiesgo, $tipo){
        $suma = 0;
        if ($tipo == "correctivo") {
            $tipoTratamiento = RiesgoTratado::where("rtra_rsgid", $idRiesgo)->where("rtra_tipotra", 2)->get();
        }
        if ($tipo == "preventivo") {
            $tipoTratamiento = RiesgoTratado::where("rtra_rsgid", $idRiesgo)->where("rtra_tipotra", 1)->get();
        }

        foreach ($tipoTratamiento as $tp) {
            $suma = $suma + $tp->rtra_suma;
        }

         return $suma;

    }

     public function riesgosTratadosCantidad($idRiesgo, $tipo){
    
    //////////////////////////////////////////////////////////////////////////////////////
    // se coloca count para saber la cantidad de registros asociadas al riesgo tratado //
    ////////////////////////////////////////////////////////////////////////////////////
        if ($tipo == "correctivo") {
            $cantidad = RiesgoTratado::where("rtra_rsgid", $idRiesgo)->where("rtra_tipotra", 2)->count();
        }
        if ($tipo == "preventivo") {
            $cantidad = RiesgoTratado::where("rtra_rsgid", $idRiesgo)->where("rtra_tipotra", 1)->count();
        }       

         return $cantidad;

    }
    
    public function actualizarAprobacion(Request $request, $id){

        // dd($id);
        $aprobacion = Aprobacion::findOrFail($id)->update($request->all());
        // $riesgo->touch();

        return back()->with('mensaje1', 'Su Aprobación fue actualizado Exitosamente..!!');
    }

    public function actualizarImplementacion(Request $request, $id){

        // dd($id);
        $implementacion = Implementaciontrata::findOrFail($id)->update($request->all());
        // $riesgo->touch();

        return back()->with('mensaje1', 'Su Implementación fue actualizado Exitosamente..!!');
    }

    public function guardarImplementacion(Request $request){

        // dd($request->all());
        // 
        
        $validator = Validator::make($request->all(), [
            'imt_aptraid' => 'required|unique:implementaciontrata'
        ]);

        if ($validator->fails()) {
             return back()->with('mensaje1', 'Ya existe una implementación asociada al tratamiento');
        }else{


        // $request->validate([   

            

            // 'imt_responsable' => 'string|max:100|required',
            // 'aptra_responsableapro' => 'string|max:60|required',
            // 'aptra_fecharespuesta' => 'date|required',
            // 'aptra_ert' => 'integer|required',
            // 'aptra_observaciones' => 'string|max:200|required',           

        // ]);
            
       
            $implementacion = new Implementaciontrata();


            $implementacion->imt_aptraid = $request->imt_aptraid;
            $implementacion->imt_responsable = $request->imt_responsable;
            $implementacion->imt_fechaseguimiento = $request->imt_fechaseguimiento;
            $implementacion->imt_fechaobje = $request->imt_fechaobje;
            $implementacion->imt_culminacion = $request->imt_culminacion;
            $implementacion->imt_porcavance = $request->imt_porcavance;
            $implementacion->imt_observaciones = $request->imt_observaciones;        
            $implementacion->imt_rsgid = $request->imt_rsgid;        
            $implementacion->imt_status = 1;
            $implementacion->imt_usrcreacion = $request->user()->id;
          

            $implementacion->save();  

            return back()->with('mensaje1', 'Su implementación fue agregada Exitosamente..!!');
        }

    }

    public function eliminarAprobacion(Request $request){

        Aprobacion::findorfail($request->id)->delete();

         return back()->with('mensaje1', 'Aprobación Eliminada');

    }

    public function eliminarImplementacion(Request $request){

        Implementaciontrata::findorfail($request->id)->delete();

         return back()->with('mensaje1', 'Implementación Eliminada');

    }
    

    
   
}
