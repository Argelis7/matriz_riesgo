<?php

namespace App\Http\Controllers;

use App\Models\Revision;
use App\Models\tipoRevision;
use App\Models\RiesgoControlado;
use App\Models\RiesgoTratado;
use App\Models\Riesgo;
use App\Models\Unidad;
use App\Models\Conclusionrevision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class RevisionController extends Controller
{
   
    //public $idAuthUser = Auth::user()->id;
    public function consultaRevision(Request $request)
    {
        // return dd($request->all());
         $model = tipoRevision::with('revisiones');
         // $revisiones = Revision::where( "rev_usrcreacion", Auth()->user()->id )->get();

         // return view('consultas.FormConsultar' , compact('revisiones'));
         return datatables()
                ->eloquent(Revision::query()->where("rev_usrcreacion", $request->idRevisionUser) )
                ->addColumn('btn', 'formularios.revision.accionesRevision')
                ->rawColumns(['btn'])
                
                ->toJson();
    }

    public function consultarRevisionVer()
    {
         // $idRevision = Auth()->user()->id;

         return view('consultas.FormConsultar');         

    }
   
    public function revision()
    {

         

        // select formulario "Registrar Revision"
        $tipoRevision = tipoRevision::all();    

        $riesgosInherentes = Riesgo::all();

        $unidad = Unidad::all();

        $conclusion = Conclusionrevision::all();
            

        return view('formularios.revision.revision', compact('tipoRevision', 'riesgosInherentes', 'unidad','conclusion'));   
  
    }

 // Envio de Formulario de revision
    public function registrarRevision(Request $request){


      $formulario_revision = $request->formulario_revision;

        if ($formulario_revision == "Revision") 
        {        
            return $this->guardarRevision($request);
        }

      
        if ($formulario_revision == "unidades") 
        {        
             $unidad = $this->guardarUnidad($request);             

              return response()->json([  

              'nombre' => $unidad->uni_nomape,
              'ggeneral' => $unidad->uni_ggeneral,
              'gcorporativa' => $unidad->uni_gcorporativa,
              'coordinacion' => $unidad->uni_coordinacion,
              'tipo' => $unidad->uni_tunidad_evaluacion,        
              'mensaje' => "Unidad registrada exitosamente. !!!"

              ]);
        }

         if ($formulario_revision == "Conclusion") 
        {        
            return $this->guardarConclusion($request);
        }
        

        
        
        // Metodo del dominicano
        // Revision::create($request->all());

        // prueba del id
            
            // return view('formularios.riesgo.RegistrarRiesgo', compact('idRevision'));
        // return back()->with('status','Revisión registrada exitosamente.');
        // 
        // Metodo del dominicano
        // $contenido = $request->all();
        // $prueba = array(['
        //     rev_usrcreacion' => '1' ,
        //     'rev_status' => '1'
        // ]);

        // Revision::create($request->all());
    }

    public function eliminarRevision(Request $request){

        // dd($request->all());
   

        Revision::findOrFail($request->id)->delete();

         return response()->json([
            'mensaje' => 'Revision Eliminada Exitosamente ..!!'
         ]);

    }

    public function editarRevision($rev_id){

        // $revision = Revision::where('rev_id', $rev_id)->first();
        $revision = Revision::findOrFail($rev_id);

        // dd($revision);

        // return $revision-
        

        $riesgosInhe = Riesgo::where('rsg_revid', $rev_id)->get();

        foreach ($riesgosInhe as $r) {
   

          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // esto es para obtener LA SUMA TOTAL de los tipo de control tanto preventivo como correctivo de la COLUMNA SUMA CONTROLADO //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

         $sumaControlCorrectivo_r[] = $this->riesgosControladosTipos($r->rsg_id, "correctivo");
         $sumaControlPreventivo_r[] = $this->riesgosControladosTipos($r->rsg_id, "preventivo");

         ////////////////////////////////////////////////////////////////////
         // para saber la cantidad de controles CORRECTIVOS y PREVENTIVOS  //
         ////////////////////////////////////////////////////////////////////
         
         $CantidadControlCorrectivo_r[] = $this->riesgosControladosCantidad($r->rsg_id, "correctivo");
         $CantidadControlPreventivo_r[] = $this->riesgosControladosCantidad($r->rsg_id, "preventivo");

         ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         // esto es para obtener LA SUMA TOTAL de los tipo de tratamiento tanto preventivo como correctivo de la COLUMNA SUMA TRATAMIENTO //
         ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

         $sumaTratamientoCorrectivo_r[] = $this->riesgosTratadosTipos($r->rsg_id, "correctivo");
         $sumaTratamientoPreventivo_r[] = $this->riesgosTratadosTipos($r->rsg_id, "preventivo");

         //////////////////////////////////////////////////////////////////////
         // para saber la cantidad de tratamientos CORRECTIVOS y PREVENTIVOS //
         //////////////////////////////////////////////////////////////////////

         $CantidadTratamientoCorrectivo_r[] = $this->riesgosTratadosCantidad($r->rsg_id, "correctivo");
         $CantidadTratamientoPreventivo_r[] = $this->riesgosTratadosCantidad($r->rsg_id, "preventivo");

          }

        $riesgosInherentes = Riesgo::where('rsg_revid', $rev_id)->get();

        $tipoRevision = tipoRevision::all();

        $estado = true;

        $unidad = Unidad::where('uni_revid', $rev_id)->get();

        $conclusion = Conclusionrevision::where('cr_revid',$rev_id)->get();
        
        return view( 'formularios.revision.revisionEdita', compact('revision', 'tipoRevision', 'estado', 'riesgosInherentes', 'sumaControlCorrectivo_r', 'sumaControlPreventivo_r', 'CantidadControlCorrectivo_r', 'CantidadControlPreventivo_r', 'sumaTratamientoCorrectivo_r', 'sumaTratamientoPreventivo_r', 'CantidadTratamientoCorrectivo_r', 'CantidadTratamientoPreventivo_r', 'unidad','conclusion') );
            

    }

    // Envio de Formulario de revision
    public function ActualizarRevision(Request $request, $id){
        

         $revision = Revision::findOrFail($id)->update($request->all());
         // $revision->touch();


        return back()->with('mensaje', 'Su Revision fue actualizado Exitosamente..!!');
        
       
    }    

    /////////////////////////////////////
    // para los calculos de controles  //
    /////////////////////////////////////

    public function riesgosControladosTipos($idRiesgo, $tipo){
        $suma = 0;
        if ($tipo == "correctivo") {
            $tipoControl = RiesgoControlado::where("rcon_rsgid", $idRiesgo)->where("rcon_tipocontrol", 2)->get();
        }
        if ($tipo == "preventivo") {
            $tipoControl = RiesgoControlado::where("rcon_rsgid", $idRiesgo)->where("rcon_tipocontrol", 1)->get();
        }

        foreach ($tipoControl as $tp) {
            $suma = $suma + $tp->rcon_suma;
        }

         return $suma;

    }

     public function riesgosControladosCantidad($idRiesgo, $tipo){
    
    ////////////////////////////////////////////////////////////////////////////////////////
    // se coloca count para saber la cantidad de registros asociadas al riesgo controlado //
    ////////////////////////////////////////////////////////////////////////////////////////
        if ($tipo == "correctivo") {
            $cantidad = RiesgoControlado::where("rcon_rsgid", $idRiesgo)->where("rcon_tipocontrol", 2)->count();
        }
        if ($tipo == "preventivo") {
            $cantidad = RiesgoControlado::where("rcon_rsgid", $idRiesgo)->where("rcon_tipocontrol", 1)->count();
        }       

         return $cantidad;

    }

    ///////////////////////////////////////
    // para los calculos del tratamiento //
    ///////////////////////////////////////

    public function riesgosTratadosTipos($idRiesgo, $tipo){
        $suma = 0;
        if ($tipo == "correctivo") {
            $tipoTratamiento = RiesgoTratado::where("rtra_rsgid", $idRiesgo)->where("rtra_tipotra", 2)->get();
        }
        if ($tipo == "preventivo") {
            $tipoTratamiento = RiesgoTratado::where("rtra_rsgid", $idRiesgo)->where("rtra_tipotra", 1)->get();
        }

        foreach ($tipoTratamiento as $tp) {
            $suma = $suma + $tp->rtra_suma;
        }

         return $suma;

    }

     public function riesgosTratadosCantidad($idRiesgo, $tipo){
    
    //////////////////////////////////////////////////////////////////////////////////////
    // se coloca count para saber la cantidad de registros asociadas al riesgo tratado //
    ////////////////////////////////////////////////////////////////////////////////////
        if ($tipo == "correctivo") {
            $cantidad = RiesgoTratado::where("rtra_rsgid", $idRiesgo)->where("rtra_tipotra", 2)->count();
        }
        if ($tipo == "preventivo") {
            $cantidad = RiesgoTratado::where("rtra_rsgid", $idRiesgo)->where("rtra_tipotra", 1)->count();
        }       

         return $cantidad;

    }

    public function guardarUnidad(Request $request){

        // validar los campos del formulario
        
        // $request->validate([
        //     'uni_ggeneral' => 'required|integer',
        //     'uni_gcorporativa' => 'required|string|max:70',
        //     'uni_coordinacion' => 'required|string|max:70',
        //     'uni_email' => 'required|email',
        //     'uni_telf' => 'required|integer|max:11',
        //     'uni_tunidad_evaluacion' => 'required|string|max:60',
        //     'uni_nomape' => 'required|string|max:50',
        // ]);        
        // 
        // 
        
        ///////////////////////////////
        // guardar datos en la tabla //
        ///////////////////////////////

        $unidad = new Unidad();

        $unidad->uni_revid = $request->uni_revid;
        $unidad->uni_ggeneral = $request->uni_ggeneral;
        $unidad->uni_gcorporativa = $request->uni_gcorporativa;
        $unidad->uni_coordinacion = $request->uni_coordinacion;
        $unidad->uni_email = $request->uni_email;
        $unidad->uni_telf = $request->uni_telf;
        $unidad->uni_tunidad_evaluacion = $request->uni_tunidad_evaluacion;
        $unidad->uni_nomape = $request->uni_nomape;

        $unidad->save();

        return $unidad;

        

    }

    public function guardarRevision(Request $request){
        // validar los campos introducidos en el formulario
        // $request->validate([
        //     'rev_trev' =>           'string',
        //     'rev_fechainicio' =>    'date',
        //     'rev_fechafinal' =>     'date',
        //     'rev_fecharealfinal' => 'date',
        //     'rev_nombre' =>         'alpha|max:200',
        //     'rev_ggneral' =>        'integer',
        //     'rev_gerencia' =>       'alpha|max:60',
        //     'rev_coordinacion' =>   'alpha|max:60',
        //     'rev_responsable' =>    'alpha|max:60',
        //     'rev_objetivos' =>      'string|max:200',
        //     'rev_metas' =>          'string|max:200',
        //     'rev_alcances' =>       'string|max:200',
        //     'rev_relacion' =>       'string|max:200',
        //     'rev_fortalezas' =>     'string|max:200',
        //     'rev_oportunidades' =>  'string|max:200',
        //     'rev_debilidades' =>    'string|max:200',
        //     'rev_amenazas' =>       'string|max:200',
        //     'rev_usrcreacion' =>    'integer|nullabl',
        //     'rev_status' =>         'integer|nullable',
        //     'rev_revid' =>          'integer|nullable',
        // ]);
        
        // insertar datos en la tabla 
        $revision = new Revision();
        $revision->rev_fechainicio = $request->rev_fechainicio;
        $revision->rev_trev = $request->rev_trev;
        $revision->rev_fechafinal = $request->rev_fechafinal;
        $revision->rev_fecharealfinal = $request->rev_fecharealfinal;
        $revision->rev_nombre = $request->rev_nombre;
        $revision->rev_ggeneral = $request->rev_ggeneral;
        $revision->rev_coordinacion = $request->rev_coordinacion;
        $revision->rev_gerencia = $request->rev_gerencia;
        $revision->rev_responsable = $request->rev_responsable;
        $revision->rev_objetivos = $request->rev_objetivos;
        $revision->rev_alcances = $request->rev_alcances;
        $revision->rev_metas = $request->rev_metas;
        $revision->rev_relacion = $request->rev_relacion;
        $revision->rev_fortalezas = $request->rev_fortalezas;
        $revision->rev_debilidades = $request->rev_debilidades;
        $revision->rev_oportunidades = $request->rev_oportunidades;
        $revision->rev_amenazas = $request->rev_amenazas;

        $revision->rev_usrcreacion = $request->user()->id; //guarda el "id" del usuario activo en la sesion
        $revision->rev_status = 1; //Deberia ser siempre 1 porque siempre se crea en status "activo".
        $idRevision = $revision->save();

        // echo $revision->rev_id;
        // echo $revision->created_at;

        return $revision->toJson();
    }

    public function guardarConclusion(Request $request){

        $request->validate([   

            'cr_revid' => 'required'

            // 'aptra_nombretra' => 'string|max:100|required',
            // 'aptra_responsableapro' => 'string|max:60|required',
            // 'aptra_fecharespuesta' => 'date|required',
            // 'aptra_ert' => 'integer|required',
            // 'aptra_observaciones' => 'string|max:200|required',           

        ]);

        $conclusion = new Conclusionrevision();
        $conclusion->cr_revid = $request->cr_revid;
        $conclusion->cr_conclusion = $request->cr_conclusion;
        $conclusion->cr_nombre = $request->cr_nombre;
        $conclusion->cr_logro = $request->cr_logro;
        $conclusion->cr_desviaciones = $request->cr_desviaciones;
        $conclusion->cr_puntosatencion = $request->cr_puntosatencion;
        $conclusion->cr_unidadresponsable = $request->cr_unidadresponsable;

        $conclusion->cr_usrcreacion = $request->user()->id;
        $conclusion->cr_status = 1;

        $conclusion->save();
        
        // return response()->json([                    
        //     'mensaje' => "Conclusion registrada Exitosamente..!!"            
        //  ]);
        
        return back()->with('conclusion','Conclusion registrada Exitosamente..!!');

    }

    public function eliminarConclusion(Request $request, $id)
    {
        Conclusionrevision::findOrFail($request->id)->delete();        
         
        return back()->with('mensaje', 'Su Conclusion fue Eliminada Exitosamente..!!');       

    }

    public function eliminarUnidad(Request $request, $id)
    {
        Unidad::findOrFail($request->id)->delete();        
         
        return back()->with('mensaje', 'Su Unidad fue Eliminada Exitosamente..!!');       

    }
   

 }
