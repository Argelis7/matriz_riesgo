<?php 
 
 ////////////////////// funcion hora /////////////////////////
    {{ $fecha=getdate();
        $dia=$fecha['mday'].' / '.$fecha['month'].' / '.$fecha['year'];
        $hora=$fecha['hours'] - 4;

      // establecar el formato de 12 horas no 24  

if ($hora>=12){
if ($hora==12){
    $hor=$hora;}
else{
    $hor=$hora-12;}
    $hora=$hor.':'.$fecha['minutes'].' pm ';}
else{ 
if ($hora==0) {
    $hor=$hora + 12; }
else{
    $hor=$hora;}
    $hora=$hor.':'.$fecha['minutes'].' am ';}
    }}
      
  ////////////////termina funcion hora///////////////////////// 



function severidad($conse, $proba) {

    switch ($conse) {
    
   case 1:
        if ($proba == 1 || $proba == 2 || $proba == 3) {
            $sever = "Baja";
        }
        if ($proba == 4) {
            $sever = "Moderada";
        }
        if ($proba == 5) {
            $sever = "Alta";
        }
    break;

   case 2;
        if ($proba == 1 || $proba == 2) {
            $sever = "Baja";
        }
        if ($proba == 3) {
            $sever = "Moderada";
        }
        if ($proba == 4 || $proba == 5) {
            $sever = "Alta";
        }
    

   case 3;
        if ($proba == 1 || $proba == 2) {
            $sever = "Moderada";
        }
        if ($proba == 4 || $proba == 3) {
            $sever = "Alta";
        }
        if ($proba == 5) {
            $sever = "Extrema";
        }
    
   case 4;
        if ($proba == 1 || $proba == 2) {
            $sever = "Alta";
        }
        if ($proba == 4 || $proba == 3 || $proba == 5) {
            $sever = "Extrema";
        }
    
   case 5;
        if ($proba == 1) {
            $sever = "Alta";
        }
        if ($proba == 4 || $proba == 3 || $proba == 2 || $proba == 5) {
            $sever = "Extrema";
        }
    
    }
    return $sever;
}

function calculo_controlado($rinherente, $promedio1, $promedio2) {
    $total = $rinherente - ($rinherente * $promedio1 / 100) - ($rinherente * $promedio2 / 100);
    if ($total < 1) {
        $total = 1;
    }
    return $total;
}

function calculo_tratado($rinherente, $promedio1, $promedio2, $promedio3) {
    $total = $rinherente - ($rinherente * $promedio1 / 100) - ($rinherente * $promedio2 / 100) - ($rinherente * $promedio3 / 100);
    if ($total < 1) {
        $total = 1;
    }
    return $total;
}

function suma_control($id_cont1, $id_cont2, $id_cont3) {

    $cadena_consulta = "SELECT * FROM implementacioncontrolr WHERE icr_id=$id_cont1 ";
    $resultado_cunsulta = nueva_consulta($cadena_consulta);
    $row_im = pg_fetch_array($resultado_cunsulta);

    $cadena_consulta = "SELECT * FROM documentacioncontrolr WHERE dcr_id=$id_cont2 ";
    $resultado_cunsulta = nueva_consulta($cadena_consulta);
    $row_dc = pg_fetch_array($resultado_cunsulta);

    $cadena_consulta = "SELECT * FROM ejecucioncontrolr WHERE ecr_id=$id_cont3 ";
    $resultado_cunsulta = nueva_consulta($cadena_consulta);
    $row_ej = pg_fetch_array($resultado_cunsulta);

    $suma = $row_im["icr_valor"] + $row_dc["dcr_valor"] + $row_ej["ecr_valor"];

    return $suma;
}

function suma_tratamiento($id_cont1, $id_cont2, $id_cont3) {
    $cadena_consulta = "SELECT * FROM complejidadtrar WHERE ctr_id=$id_cont1 ";
    $resultado_cunsulta = nueva_consulta($cadena_consulta);
    $row_com = pg_fetch_array($resultado_cunsulta);

    $cadena_consulta = "SELECT * FROM costobeneficiotrar WHERE cbtr_id=$id_cont2 ";
    $resultado_cunsulta = nueva_consulta($cadena_consulta);
    $row_cos = pg_fetch_array($resultado_cunsulta);

    $cadena_consulta = "SELECT * FROM tiempotrar WHERE ttr_id=$id_cont3 ";
    $resultado_cunsulta = nueva_consulta($cadena_consulta);
    $row_ti = pg_fetch_array($resultado_cunsulta);

    $suma = $row_com["ctr_valor"] + $row_cos["cbtr_valor"] + $row_ti["ttr_valor"];

    return $suma;
}

function efectividad_control($id_cont1) {

    if ($id_cont1 <= 39)
        $efect = "Pobre";

    if ($id_cont1 >= 39 && $id_cont1 <= 54)
        $efect = "Insatisfactorio";

    if ($id_cont1 >= 55 && $id_cont1 <= 74)
        $efect = "Regular";

    if ($id_cont1 >= 75 && $id_cont1 <= 94)
        $efect = "Buena";

    if ($id_cont1 == 95 && $id_cont1 <= 100)
        $efect = "Excelente";
    return $efect;
}
function efectividad_tratamiento($id_cont1) {

    if ($id_cont1 <= 40)
        $efect = "Casi nunca";

    if ($id_cont1 >= 41 && $id_cont1 <= 55)
        $efect = "Ocasionalmente";

    if ($id_cont1 >= 56 && $id_cont1 <= 70)
        $efect = "Algunas veces";

    if ($id_cont1 >= 71 && $id_cont1 <= 85)
        $efect = "Casi siempre";

    if ($id_cont1 == 86 && $id_cont1 <= 100)
        $efect = "Siempre";
    return $efect;
}




   ?>
  